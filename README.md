To install the ProBiS plugin:
- download correct file on your computer
- unzip it
- follow instructions in InstallationGuide.pdf

[PyMOL Plugin] (https://gitlab.com/tanjastular/probis-plugin/raw/master/pymol-install/probis.py)

[UCSF Chimera Plugin] (https://gitlab.com/tanjastular/probis-plugin/raw/master/chimera-install/probis.zip)
