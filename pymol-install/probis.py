'''
Described at PyMOL wiki: http://www.pymolwiki.org/index.php/probislite
 
'''

import os
import stat
import urllib2
from urllib2 import urlopen,URLError,HTTPError
import zipfile
import tempfile
import shutil
import platform
from distutils.dir_util import copy_tree
from Tkinter import *
from ttk import *
import webbrowser
import time
import tkMessageBox
from threading import Thread
import contextlib

HOME_DIRECTORY=os.path.expanduser('~')
PROBIS_DIRECTORY=os.path.join(HOME_DIRECTORY,".probis")
versionFile = os.path.join(PROBIS_DIRECTORY,"version.txt")


class Configuration:
    def __init__(self):
        
        self.system=platform.system()
        self.machine=platform.machine()
        self.architecture=platform.architecture()
        import struct
        self.python_bit=8 * struct.calcsize("P")
        self.python_version=platform.python_version_tuple()

    def is_os_64bit(self):
        if self.system == 'Windows':
            return platform.architecture()[0] == "64bit"
        else:
            return platform.machine().endswith('64')
        
    def exe_File(self):
        if self.system=='Windows':
            if self.is_os_64bit():
                exe_filename="64/probislite.exe"
            else:
                exe_filename="32/probislite.exe"
            
        elif self.system=='Linux':
            exe_filename="probislite"
        else:
            print "The plugin might not be compatible with your machine"
            exe_filename="probislite"
        return exe_filename
        
    
    
class UpdateCheck:

    def __init__(self):
        self.firstVersionURL="http://insilab.org/files/probis-plugin/version.txt"
        self.firstArchiveURL="http://insilab.org/files/probis-plugin/archive.zip"
        self.currentVersion=""
        self.latestVersion = ""


    def deleteTmpDir(self):
        try:
            shutil.rmtree(self.tmpDir)
            print "deleted temporary ", self.tmpDir
        except:
            print "error : could not remove temporary directory"


    def createTmpDir(self):
        try:
            self.tmpDir= tempfile.mkdtemp()
            print "created temporary ", self.tmpDir
        except:
            print "error : could not create temporary directory"
        self.zipFileName=os.path.join(self.tmpDir, "archive.zip")


    def downloadInstall(self):

        try:
            urlcontent=urlopen(self.firstArchiveURL, timeout=7)
            zipcontent=urlcontent.read()
            probisZipFile=open(self.zipFileName,'wb')
            probisZipFile.write(zipcontent)
        except:
            print "Error: download failed, check if http://insilab.org website is up..."
            self.installationFailed()
        finally:
            try:
                probisZipFile.close()
            except:
                pass
            try:
                urlcontent.close()
            except:
                pass

    def extractInstall(self):
        #this must be called before import Plugin_GUI, otherwise
        #rmtree will fail on NFS systems due to open log file handle
        try:
            with contextlib.closing(zipfile.ZipFile(self.zipFileName,"r")) as probisZip:
                for member in probisZip.namelist():
                    masterDir = os.path.dirname(member)
                    break
                probisZip.extractall(self.tmpDir)
            #copy new
            #if os.path.isdir(PROBIS_DIRECTORY):
            #    shutil.rmtree(PROBIS_DIRECTORY)

            #os.mkdir(PROBIS_DIRECTORY)
            copy_tree(os.path.join(self.tmpDir, masterDir, ".probis"), PROBIS_DIRECTORY)
        except Exception, e:
            print e
            self.installationFailed()
            print "installation of probis failed"

    def firstUpgrade(self):
        return not os.path.isdir(PROBIS_DIRECTORY)

    def installationFailed(self):
        try:
            shutil.rmtree(PROBIS_DIRECTORY)
        except:
            pass
        global running, status
        status = "failed"
        running = False


    def upgrade(self):

        self.createTmpDir()
        self.downloadInstall()
        self.extractInstall()
        self.deleteTmpDir()

        # add executable properties
        configure=Configuration()
        exe_filename=configure.exe_File()
        exe_path=os.path.normpath(os.path.join(PROBIS_DIRECTORY,"bin",exe_filename))
        st = os.stat(exe_path)
        os.chmod(exe_path, st.st_mode | stat.S_IEXEC)

        self.writeToVersionTxt(self.latestVersion)

        print "Upgrade finished successfully!"
        global running, status
        status = "start"
        running = False

    #
    # read current version of probis from version.txt file
    #
    def findCurrentVersion(self):

        if os.path.isfile(versionFile):
            with open(versionFile, 'r') as myFile:
                lines = myFile.readlines()
            if len(lines) == 1:
                self.currentVersion = lines[0].strip()
            else:
                date = lines[len(lines)-1].split('\t')[2]
                t = time.strftime("%d/%m/%Y")

                tmpTime1 = date.split('/')
                tmpTime2 = t.split('/')
                if int(tmpTime2[2]) > int(tmpTime1[2]) or int(tmpTime2[1]) > int(tmpTime1[1]) or int(tmpTime2[0]) > int(tmpTime1[0])+15:
                    self.currentVersion = lines[0].strip()
                else:
                    self.currentVersion = (lines[len(lines)-1].split('\t')[0]).strip()


    #
    # get info about latest version
    #
    def findLatestVersion(self):
        try:
            #from secondary git server
            versionFile = urllib2.urlopen(self.firstVersionURL, timeout=5)
            self.latestVersion = versionFile.read().strip()
            if len(self.latestVersion) > 6:
                self.latestVersion = ""
            print "ProBiS version: ", self.latestVersion
        except HTTPError, e1:
            print "HTTP Error:", e1.code, e1.reason
        except URLError, e2:
            print "URL Error:", e2.reason
        except Exception, e:
            print "Error: Gitlab server down?"


    #
    # yes or no update window
    #
    def updateWindow(self):

        self.window=Tk()
        self.window.title("Software Update")

        frame = Frame(self.window)
        frame.pack()

        Label(frame, text="A new ProBiS version is available. Do you want to update?", anchor=CENTER).pack(pady=(20,10), padx=30, fill=BOTH, expand=1)


        versionFrame = Frame(self.window)
        versionFrame.pack(fill=BOTH, expand=1, padx=15, pady=(0,10))

        Label(versionFrame, text="Current version: ").grid(column=0, row=0, sticky=W, padx=(30,0), pady=(10,0))
        Label(versionFrame, text="Latest version: ").grid(column=0, row=1, sticky=W, padx=(30,0), pady=(0,10))

        Label(versionFrame, text=self.currentVersion).grid(column=1, row=0, sticky=W, padx=10, pady=(10,0))
        Label(versionFrame, text=self.latestVersion).grid(column=1, row=1, sticky=W, padx=10, pady=(0,10))
        Label(versionFrame, text="For more information about software update, go to:", font=("Times",10)).grid(column=0, columnspan=2, row=3, padx=30)
        urlLabel = Label(versionFrame, text="http://insilab.org/probis-plugin", font=("Times",10),foreground="blue",underline=True)
        urlLabel.grid(row=4, column=0, columnspan=2, pady=(0, 10), padx=30, sticky=W)
        urlLabel.bind("<Button-1>", self.open_url)

        self.checkRemindMe = 0
        Checkbutton(self.window, command=self.setCheckRemindMe, text="Do not ask me again").pack(fill=BOTH, expand=1, padx=60)

        buttonsFrame = Frame(self.window)
        buttonsFrame.pack(fill=BOTH, expand=1, padx=30, pady=(10,20))
        Button(buttonsFrame, text='Cancel', command=self.cancelButton).grid(row=0, column=0, padx=(200, 20), pady=(0,10))
        Button(buttonsFrame, text='Update', command=self.updateButton).grid(row=0, column=1, pady=(0,10))

        self.window.bind('<Return>', self.updateButton)
        self.window.protocol('WM_DELETE_WINDOW', self.cancelButton)

        self.window.mainloop()



    def open_url(self, e):
        try:
            webbrowser.open_new(r"http://insilab.org/probis-plugin")
        except:
            print "Error: Could not open the website."

    def setCheckRemindMe(self):
        if self.checkRemindMe == 0:
            self.checkRemindMe = 1
        else:
            self.checkRemindMe = 0

    def cancelButton(self):
        if self.checkRemindMe  == 1:
            self.writeToVersionTxt(self.latestVersion, True)
        self.window.quit()


    def updateButton(self, event=None):
        #shutil.rmtree(PROBIS_DIRECTORY)
        global status
        status = "update"
        self.window.quit()


    #
    # write to version.txt file
    #
    def writeToVersionTxt(self, version, reminder=False):

        if reminder:
            with open(versionFile, 'a') as myfile:
                myfile.write(version+"\t later \t"+time.strftime("%d/%m/%Y")+"\n")

        else:
            with open(versionFile, 'w') as myfile:
                myfile.write(version+"\n")


def internetOn():
        try:
            urllib2.urlopen('http://www.google.com',timeout=1)
            return True
        except:
            pass
        return False

def run():

    print("Initialising ProBiS ...")

    global running, updateObj, status
    status = "start"

    try:
        sys.path.remove('')
    except:
        pass

    updateObj = UpdateCheck()

    if updateObj.firstUpgrade():
        if not internetOn():
            status = "no-internet"
        else:
            updateObj.findLatestVersion()
            updateObj.upgrade()
    else:
        updateObj.findCurrentVersion()

        if internetOn():
            updateObj.findLatestVersion()

            if updateObj.currentVersion == "UPDATE NOW":
                updateObj.upgrade()

            elif updateObj.latestVersion != updateObj.currentVersion:
                status = "check-update"

    running = False


def showManualDlInfo():
    tkMessageBox.showinfo("Installation failed.", "To download and manually install ProBiS please visit website http://insilab.org/probis-plugin/ .")


class FuncThread(Thread):
    def __init__(self, target, *args):
        self._target = target
        self._args = args
        Thread.__init__(self)

    def run(self):
        self._target(*self._args)


class Logo(Frame):

    def __init__(self, parent, txt):
        Frame.__init__(self, parent)

        self.parent = parent

        # move window to center
        self.update_idletasks()
        w = self.winfo_screenwidth()
        h = self.winfo_screenheight()
        size = tuple(int(_) for _ in self.parent.geometry().split('+')[0].split('x'))
        x = w/2 - size[0]/2
        y = h/2 - size[1]/2
        self.parent.geometry("%dx%d+%d+%d" % (size + (x, y)))

        s = Style()
        s.configure("My.TFrame", background='#BFBFBF')

        frame=Frame(self.parent, style="My.TFrame")
        frame.pack(fill=BOTH, expand=1)

        raw_data = """R0lGODlh2gJcAPYAAAAAAAwAAAMFDBQAABwAAAMEEQYJFAABGwcLGQgNHSMAACsAADMAADsAAAEBIwoPIAEBLAsRJQ0ULAABMwAAOw4XMQ8YNBAZNhEcPEMAAEwAAFMAAFsAAGMAA
                    GsAAHIAAHsAAQEAQwAASxMfQgAAVAEAXBQgRRYjSxclURgnUhkpVhorXAEAZAAAbQAAcwAAfBwuYx4wZx8xaiAzbiE2cyI4dyQ6fIQAAYsAAJUAAJwAAKMAAK0AALMAALoAAMUAAMwA
                    ANQAANsAAOQAAOoAAPMAAP4AAAAAhAAAiwAAlAAAmiY9gyc/iAAApQAAqwAAtAAAuydAiSlBjCtFlCxHmC1JnS9LojBNpjFPqjJRrTRUtDZXujdYvThZvwAAwwEAywAA1QAA3AAA4wA
                    A7AAA9AAA/jdZwDlbxDpeyjxgzj1j1D9l2UBn3UBo30Fp4URu7EVw70Zw8Uh1+wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH5BAEAAHMALAAAAADaAlwAAA
                    f+gHOCg4SFhoeIiYqLjI2Oj5CRkpOUlZaXmJmam5ydnp+goaKjpKWmp6ipqqusra6vsLGys7S1trexA4gFj7qHB7jBwsPExcbHyMnKlgQdOiANhA4sSkcTiwweOh4LhBAuSS4Oy+Tl5
                    ufo6errswQ/RvBEHwBzDlBl+GMuiRpC8EZBMgiiIAZfmTAk2ClcyLChw4cQaQXA8S+eAgBJypAxSIbCIQI9KhrpMeDAPYNlvACLyLKly5cwYyoc8E5khgNeUOJjcSiDv4pCGjjQqXGc
                    zKNIkypdypTTAB8ijdw8ibLEoQY//wVh4GAjSjFGm4odS7as2YcBOogMMgCAC53+YA8B0CEyB4ADSnQ64XW2r9+/gAPDIrCjCDwgHOgdSOL1i4hEDKDC88FAEAQvG8lA8Si4s+fPoEM
                    7GpChgwYChARQYEEirCAAsOnNId0hgy9BBUKwCMFXtO/fwIODjk1cdiHYwpMrX84cqeziyBNFN968uvXr2I1Bp44o9mvu2cOLH08e1HZH3udEL8++vfv3cs8/Sp8evv1i8u/rtwV9EH
                    hF9a3XCnLEqbcdgd4VqOCB/RW4H4AMFmcgghEeOGGEn8QmwQkjRBCAgJV8WOGIIo54YIkPljXAioYU4KJ/DhLyn3TU1ccKAAKckIAAAhhgggQGGCDABRgEaYAEIxj+mcAJRhqgo5EjA
                    BkkBkUKKYEJQQqwJI88YmDhfrAZacEFYl6AQJAVYBBBkBFgIGUCGFRg5AUW9GhABWYGKcEFawrQppQGRJBAbD3GSAlsEdhwxhtvsMEGFSaAyEhxCRgAwJEcSiCBBSusgAGRK8DwKQYo
                    zGAClSfMcMIFF4wwgwqsYtDpqKGOmgIMJrBqaYpiBcCBD0HskEEAgghQghdgOEEBPTYeh56Azaq3wAYaKFAIBCW0tgkAK8jhBhptpAHHGmiwgYYb36ahhhtvoKEGGmu8sUYa7srrrhq
                    NqpFGGm+4oe66b6ghcLxonHHGuVtcMcUSVpiAQLTtAYBBFmr+sIGuwI5mrHG5bLShsRts6GuxGwKrsUbI71qMssXhnpExF1pkocUUpkawJsSLXApDF/224fMbcGiBAY+N8AgAAm3OME
                    UXBqvRBqNQQw1H1OwCHfXUUjPqRhxc97s11FUzesYJM/IKUwAfVEQEB+q54FUZY/A0adnHcQfiAB8QAc8QiQnywhj4hNGCJgDIEAe6iCPuNbuJL57404mj63jkjTNeddfyakGDDDiLB
                    4AJXETuaOQ+P+2xx6NTrjjikLtRehuit4E10Ic7WoUJAkgKIQpowAG766W/kcUFdH9nQApadAzH7L+v4TPwr0fvMcgdn146utdD/zvssHcRqdn+Sy0whEg/BDBBQW9D0RuAz9ZNHQci
                    DVFZCTqNAUEmAlQRxxpu8K/6879zHfZI9ztHBZB76PKf/7QXPXbF4Q1WqIDuPDcCLTxOdQIEGfayF0DEjcyDlOug9AbYLy7MoAK5Q48BmIBB4FVBAnTrkQmywK7XQW+ALSRdCJ+3w/+
                    9wXvFA19EMhAVIgSgBIBDSRju14ggNstGOYgKCDBClBdkAgAzOFwOt4hDLnrxfz9zQwwM8J7PbWFyHdQh677IxhbC7g1xuIIEg6geE2zhght8wxlQUDYARGAKXUxjG23YRgzqkWxCTA
                    oDDFORIAQgBAXBx0a+4Br25ew/6QnADqL+cgMA5OVtZTjCFVcgLzcW0pRsFKQA4zhB7CRgBizzoipPScBUuuEKMGwEAmrwwRD6SwW6wxEsgTfIWdLSjWygQQISiZQA3EAk8wAAEnRix
                    flcMj7G8UBUNAAAFuiEDI/BBLcml0NjHvOcFqAjcwBwgkWd851b5CHl2LCEXS0CA1KIJxtikMK6YWALcMCjL+HJxTR2D5HMPMoCdjC+IIDAWvVQghjIAIYXCAASxZvgehbAg4qAADUH
                    eAJKkKDOnMFAi/+LJ0ExKM/IrSEOVmildQBQwQyicqXwVBkNHnBJGPSSdI1agaQuZQNzPm6WLR3oKZ92hu8l9CgBWAADIEr+iAlYdRJ00+h6BsABHeBgA7c5AAmSkAQRrM8SBqhCQH2
                    5PZx+0ajoikBJk0PTMw4Sh3BNqRqTSkzs2cCe0mknGNHFhRT8RwApCF0t3SpLVT5tC0596llkaog+tvJutyFEAVZCOBmMi7EtXKBLy6m6N1BhrsKhqQV7CNo1nhN2VHhARiNAhRy+oQ
                    pylQvoWqvGvhZ0gECU7FjWY6icdeeyxD0Ft1C6UtHytn9xkAJqg/PKkfG1i78V4TGN+QahZdQANsCuB2kAWBmtAF95vetbxfu7Rs0AAcJtio0kNLf4XFM9qAAADCYn2vSqV6855B8EK
                    bvOFLjzuUp97bcMy4j+Hg2zgWyYQT/rNkzXyrK3AuWhdrd4BmDGV77gKW537AshAneCW58tpM/4Jz0W949yzoOd80iLx6dlwcTKMaNAC+qz6oHQdSrTIAOve1TVscEGFWhijrQAxwe+
                    AbIaBa9/F7tGBPbVegC0qZUTB+UPMyVa9KVRZTOKY04IgApvIDJLo/YGOQBtanCe2gPn7GSuPVCQcH0DF6YDgEHFRq6x8TNsEJA7BxGKOA+LjZAQTRxBzwc6lgo0cSINm0LFJtGwEXS
                    fGRQJM6b5lMuz2gOxFmep2ZnO5LQphtMg1GdVYAY1oMEMVjBHRazgCqqmMeKmtjUSfvppdwY2uthgtZ/+TS14PTvkdL3MEK12Dr91u+SyLyGAGsTBnAZ9wxRGMAIa2IDbI4ABE8CNAi
                    mYwAQjMEEUVIBuEyxBClb4NC1H4J8EmGEK+K7CGaiA7ymcwQr9NkMW8E2FJTR6BFaIwhL4fYaB49sMM8N3Fu6NbyugAbdZnTQGbIACGdBgCVuIAhPwjQaA4xtm/dYCF/p9hTP02+L8n
                    gIVzFDwJdjACjQYgQUeMALZEljH5dQuG6SAgnPDgAbsHsEKaIArE6SA6edO9wykYIMlZCGWU2ZDqx8d5u4IoKgsdSzszjCDT60gBRfAkwpSUIG2pwBWmzrBCkbQdhOs4Ed4WsEJ2m4B
                    FcD+nUi5ZbZSOidi/4y5xGUm3Ar251w3yo5zJhoRFdZaSDT081JugLOpD0c7qHFNDmbA0QmoEC9GzY5RTm5y6rvmBjkY/LgAkIAVZi5gOGItDnDeGuZQn3o4mh5rmV8eqUMdaqYyQQV
                    BwpkBYLCuQgY18hHKkV1T2SgPhwJHRcU25fQcKeh7H0OCVwgAFJDZYjngrM/uuoycJR0YpUK/8havb2VXg2nLSAK2lFzonxOBK19PeivmBlogMbzWQBskY/3jP23AYk8TA5gkMTS0PK
                    xjQwvIQ2swY8+jQC+2gQDYgQI0NeRiAj53CAKgAmiQarm2QVngJZfgR1UQf74VSCD+AwMXdX3gVUtGdQXEE36i4Ss78AM84AG+AAAl0ARe8AQuUIPfQWYaBSMzUiP2dyiLN2U2poSVg
                    CPnokqN1z9vsH+DAAO+s15PgwUS0Hzw9DQseBz/xDhqhlMLJHY3BDRmgAIT5h8jcEfydEBV9jRyFIWCYACw9FMJ5ihSgAHYVCEl0h+vYQBRkIJbpAYzQEY8GBoZwEjw4AH0QAFJhA+D
                    44RzA2aeWFnugwoCMAUwqDr99Qav14ICsChtZVR6NB0JgGFrZjAoqGJvEFmvIQBawFwIBk/xogY2EAFWWEdckGZ55TFvgAU72IK7lV6wgwZbdxwJEAM2sHapYgNndwL+1jgDKCB3NqA
                    qKDA0xhEDuHZKagADfjiJMgESIiEEuiBSbyMG1xCK0mY3zxFiD5hfS+CLBfUGM7COOLIueLZFhriIjqiHNbZdNvUGafgdU7A/jveLVNZCkCcjNdWGj7OMFkAslgAbKZAGMehGzycXK8
                    AGuMc1y3NqLGlnclAFg7KIS1CQOZSOiceO4TM+FVEEDHAAkcQRCWGP91gj3xFt2KRcpFRMcEAD6zgHKoBGtIguaCCJ6oEAtJRXzrVh/bOU1JEjvjhl76SQbGQFuTQIy3eCcMU93UOHr
                    BiIhcQG6ngIFWADsTQ6WCY9FjMFsrWIYHdKS1CHOPkXCgAEIkH+BApwAF/AEXATAkb5LNBiIDKScYewAAtQfnMAAfWIVfulUo8TB5wjThEgllvUhbJIUGCpOnHAlISQAFt4U1zUmlcZ
                    OQGjjsYhACsgkhf2M0D0fYl4NCvAM7mJOtOYGj6FShqGLlMQk+ohZRpJOVuQApQGG735fRQSmOwAANpUETsgCC2gF2M2V/QVI5RlIwqAA0IgBDoQDZaBBGAQBkqQmRj1lLDpQ1dQjFj
                    1AFTIVOuRlCy1XucULw95KfPpn1skiABGk+hCBYGnHndITmomPCOQADaDbglQoecmKHByAhh6AXsnoSPwgjdUkUDmKJEoFyaYalrJh2WJfQDWQgH+QwMVoCkmoKEVagEdmgAbcgEVGg
                    GnsqPn9gAVWgEWEJ03aZ23MAAgMAREQAQ5gBpzIAB/MwZkoAScJZSOWZ2QKW2D8BQVIT/1IFIGIQaMGQkCEAX+GE9wEAX2KQk0FYbU54Xq8QDyZ5pslIvGITGuM5/QuFRR2UJooAJKC
                    HT9+T8ZAzkZ40Ed0yijo6jE1ihw6ENsYH3HoQK4WWQ79FgPCQNXcIt4dUE98zSMiqjEhjrERj0WE6pt0HBSsAQr4GhGag4KkAEM4JGv4QAhAAFnZaVXOh2PNgjwIxJTRD+glASoJQBS
                    IJGsRTpx4IDUWRy584KvmGAJZAH+YQBIRZH+jTMCdzqFCiatsRla3DcddnRhWpZUHXNUADQ92lNMbrAFCDWp58KZA6SXz8GIpxlCH4RUPTZA//czTeYGZ1CIAvAhr+oQDzht+dFE0fF
                    MIrEDAfACROEEqLVcbOQ/aaopI2ADEhCjFmADbqIpNHACGysBMAADFjCjNKAFFnSvarBMr5EABtWi6NpApWMx/LNALCNjbxCXxRKRAwWNHggyMZanB9hYlIMGDFYsBsapE0mLarlBSk
                    VkNoRxhxABvISLyfkcAkADBvpWHJSvBnWcW7auvQaXhVawzaaPH1l4NCIb2VkRneRNwzqxmzm2XmR6bBY1vcdmTpaWVfZk02H+AH2KQQJ2e3l7eqfHZq5nHAYwfYOKioyDenrLebbnQ
                    FbDKHlYp3/KuDFghvm3pyv1M8lUXmYJS0yrQ/T6HSfpiLwFlj2WZlMAQ02JtrUAIs82lCVVIAQgGXtTGfbwFeGEUaQUs1xUPQFItgp4PQpIS/EygITAn94qm7cUBSo7clJwBRQnBVTg
                    clMgBVNgBlbgvVLABVmgvWWpHlPgO6cZMGaABvjmvWgQc1JwBgozBUwAcdarBWZwBZQXWoQrA4E6rtjqmvEENGewAkLSHSZIrq5zWwsKACmAliM5wOvVXSqwI7S7EMRVpE7IthfCLNH
                    RAAwlBD6wAYMwAU4QBmL+8ARmxaY2kGIETKD5x7qSE1PPcQFbJpqUwwWyazSXRhw+l2mTFmYAgGaOZVtTQGiapmnRiWmVRlN2+0VSQK2vYUe/dq9YTK4+0wUz4GfSQamnOJpd9hoXsF
                    puda/ZBTJCNbsZPAsbfCh8liDqV5SvsSK0WiwusqZFQwXXRsE4VYXV2pyUEwfqqMctmL7EmziiJTwcnBoAQAN93EYBs8ZVfEZobLSWmqwXFAWRBiCUulhJ9TxZCyOgc4rX1VKCTMOZT
                    EBtQAXn28brwGdsuiBz/J2jME7adcmnlJV7hT1wYAOBip+qLF58ZB5ZEMZbBAfFLE5yumOsa5P+IVinvD3+jzrMOeXKrZQANECFxJa6MLLA5+rH2kdA7gWYsIwOirirBLKETHjLdSuv
                    8PyLyrmcUUySA8jGhfAA8apr2BMHq9iC0NtGajCNgIheraXLLrqCE6TNAxpC2jbPrxEAPrU4ifytKnbEkEWV5xzLt/vBhpKP49nI6BHQfhx04GrDhIACyNxbT3PP5mGKd/UGy9yCGKB
                    i7DLQ4oqHJi2z/LzKQdddDzmp/NLTsLWg1YpPbBBQPlbSvwU8NuCyG32dT2Rod/qYHQwhovDIyGph/stGXevMtqWtMoIBpwtULp0haBbTzdiCCNDQ8+QGU3CQDMpkFHma+io8ay0jnz
                    xY5Dz+xu4jAVKQ1FpDSHmIZ0dsYQjKUjVgzlGNC3ehsBKCj5FpN5NtWfhcCGl1i7x8TG6NQSMjYFrwMDJirTScbVsg0jkD03ytOkF9hQnwU/1FuNOoYyLktzMLMhzkrc05du+q1/vcW
                    KZl1HVjAOE2v5dzppmMymBU0W4UBRDd2K4AABqAAznADYMgACJwBEogDrBXy68xiutX2Qd7fSfF1XNa1y3kzzhCjcqtagZ1AddnBVvdp0+TTldkAFtoTE+D0/5RyuuLLscWNlIDfHCU
                    OMsDtImTBpJKCAV9i7XNLqMsbQaAAUsgc1qQBqgD4I5DeVojORzuNYwSzoUkBc8N3az+4AF6Aw884Ast8DZPwEQezc5q25jhXdkkliGL19lGpsis20tZqZaEtDxowEchht/n7dBcwNi
                    ZkACVmkpceUUmUNb8SjLTiAAz4LlstAUqUAEmAANoRyQwoAJjUgEqsAKsQuYwMAKxsgI01NMDxN+GkChskN/96c2f2GhqEqQ9+gA8agIVynMmgKEY8CMVyqEWsCGB/bjkPDyXbeKS0A
                    BRYRcQ8JMbYVHOlqV0TOPu5z62i9oAQtJR+4sg7nuMsgZUYAPphEkjwLS13V2eLh18fEpvMJxXGOU6LjmsJq7HmJDi1YfNShyA/dX9qQYAbKL8skANrW17iVUg/OvF8SH+CGCKzH1UT
                    9bajp4KGxAVQECEoFQGTlBJh/fdmD7ZI3aIVd3orxED881Gh3OBAubupuPu/SLvCRgvJPMusCNwVDB7RfJsJ8CpYevq6K4ek0dL9i1ORr6QoSUF8F3FoTNlQO2HRHWvhWjsUg5cYj0L
                    ErAEwh7KcL3s174K2S4S204C3f7tWprptiwXJVbjnpBWyK1U3RUDfl6hGKACQIqjMJDzDwADGJoAK2ABCJAAD3ACWGIlXnJo40na/ZltZ+0JRVzWi2ztWMWaig5jxV7JufagkWrIjsA
                    jKVCqE7x9cC7UtKSgA38oVuxfRX3HIY8KDBAEItEBAOAAiVkVCmv+eMF0Wcb1hNDWgjJQ1ge0rA0ixxTS7Igv7hgFsza9BlSM1qcrSAffgq9tnLKJ08+x9k3tM1aQ14dyAjp9YWWfGp
                    1LTm+4fVRQ4tE9MfKmZs+j7Gn/9ougAXIPD3YhCCIQBgahBLl6lESp6cBfCEi6AzuwAbXpAk2gBCQg8aTUmrP05LJwKVpJkmdAupsQAb/N4y7Ksy34ADYryWX/SgR55NhDbMwokCfQB
                    dasOLl+CBt/gU0tQGhPCxBYp47yBn0o+6ywAB3gAbZRVYBQ0iIiMGd4iJgIsJg4B6D42IgYKTm38GOUSQRiCPFUBkr2UlBZqgjz5qa66rbGytq29kb+Q2lqe4trCGDR9ur7+6bFmEus
                    axW36vr72vZWVVusKOHbu6zam6YyDGBylmrdVu3WxvZ2dQEd7RhBFf4rfj3upgaT7qiS9qZsLS9PlWBPncBKAFKw6ccvXLg3XEwEHAgxosSJFCtavIiR4DBIujaWeggik0giCgAgAYW
                    SjIiIAGAg4/crjo2HGXGteAMP4S9ZwmiynPJyVU5qb6ws8njrqFIaQcH9YiLhEIARW2DB7PUGi4UASrt6PcpV6YlyQ69KwSAJAL5vzJb12oIh0te5dB3RvatUwIks7tq+Ozjlgc+ahA
                    sbPow4sS2lH+0ODrjAh8hMRTgA8IIS5ZGIBqr+sG0FU9WbLIUUJzXQS1zZV8GQWgRARV/oX1NMRJBwZvabMxISSIgAQ0qUq9bW0DAg1cSWz07HkaOC4gEC3xgkGDAQYYSFBAYSYMDAH
                    cEIExIqzNiicLVON2/o2VObb99sN1ckyAXwwIQFAwgejLjAnXfgdYeBCQFeQN51EuiHQH8jYIBAdw9yZ8AFGESAnQrLEfdKOVREMJhpIo5IYokkevXRUUnZowAPk2WyAQBQlEFGZi+w
                    RANzbiUTBy0mpvVANauVtUZUhAEwBU7zsabjfKm8EUeT86WxwjZUKUlNWc3AMQ4bB6XiZTluhAmml288eeZQfb3TnjZpraAGaPP+NTMFQLoIUIMqZI7p5TV98slGL2FWE6aefxb6hpd
                    YCbkmM/C8UWeIP05KaaWWmrKNa5No2khAG7z4AwFzsJBZGWBAwNJN8s3Wo6SJAYDakqygkUBhCGiB5XpCvTObeuGsuut6WKHhpiEGwBCnagld0ygrB/UjaKDpBRpaTtgUm4gEUQA724
                    dyJSBFX9Huau0yz65yrp7xqDefFHZeCm+88l4KDacd2TuJJAPkUEQmQmxgiAAv1GgqCRK11FSvcMzgKmIATNNcPOO01nAxAEgBBzzcRjyfMuxW68YZKQwjwAq5fdxssKkpxKxzLb/cq
                    zxpoPDeCvl8zJq7clVwRaL+a1rLKELiDi0ktKkt2WgbSxhQ8bxOPw11NOmoqFFHiwWUAQcbLJAIBSWQQMFEAKzApaxttBovKhu7RfGRKsSBc8zBymr2biMnt1y6CSlLd9xXwZFGDMh1
                    ula5Wo6Gzp02sEy3xI2Dg3MvSyDQdNSWX355QPZuhK8jHCUmABVwALt2M6TBCwACjTfT05EVrE6u3NbozW4vDZXmCFXv+P34sleZA2Ja+Ljs+0FZJO6IATbovXvvzu+9xhIWYE599dY
                    rkqLm9T5ECWOv2vDS2r7IVPmRqvM69ypqPFAYAALE+Tw18euqbBs2CNC9cmzx7vj8RRPvqPu9Zy3Smk05IqX+CwPUQG/PStn8+gfBCIpmCyMo3/UuiEGHYUpTVNtUY7z3qhUkzC+wWJ
                    gFMwIr2clDDUYiDAK8Ib6YRQ5p/EDDyChxrDQ88HFa+l0XTqApCSzvf74LjFwQwAS/8U1+fdMVTCD1rgxKcYqGGUAlcLfBtHBKex0MYRw8BhNXtAEOPqJXAjgGi7YdCWI74dCO6MY8Y
                    b3hH7UomQ6Z2Lsezq8cVwieJAQQAzXkqlqQEowuJNCz+CVth+oRWeeoCMlI5qIBINgBDjSACAocoQlIWMkGp0YTpIDQNGMbZBuFYsK0yYaERGndkQDAlBj2b4l6pKGjeqEGGXiEGxtC
                    3w4B2Kv+lU3sDCuIwEeGxz9zVEAuyjsI/5yIRmDOiT1XsMAJJYnNKWZgCJPpgCFEIIbMsIB7rnkkI1A0IgFEAW51uwIWKQWACLhRKGp85RLkYLYHyhIWzeDCCQzwTl7OaUjPNBs52AC
                    HLjiEezbTETl2pIW4JHBxv1RhlkIzhQi8M5sc7WgjXEQZIwiBAAJwAo1OGoYJfPJzV6PaNQWizoyZDQ5ReOlrDLBP1pxhcIYBQAJswIUoySmneaydO6CkBirYZ2pX+plQ4PHMpC1Rfg
                    WEQ1HQEcoUpIEcDpSYOxCYvAUWNH1NTNn+VvEGNMyAaTb1qFuhpgAhvKgIGihAGEpFhhL+rFQq5gThIzFSyoOswRX76EVhwxGHGLSVIqljF2GTQdgKKMZ9MMAVlEbIT5eJkXHOcUd6p
                    HkuNLkhC1SoAEDsIQAVoKGzCpHWZ9MDW3ExKxyuje1nW/YGOFj1DWmgAgxA5JMIDNG2XG1tl6DIzCUQ96CKom1sa6ssarF2uVY5E3tCVpsuvnW72FRAEObagAKAoVRjMFgW+eoTxtRi
                    sbkAQAzweSY4RGm3uz1TlFIhh5mgLgFn6q9//3vfM8nBDH9l7CJgAAMbZMEMqhisG4T63wjH178BhhKFv9GMeaBhC2jIggqWai8DBDIO95VvHKy62/lO2L66XXF8Kyzf3F7+NsUvUwM
                    atLCF2rA1RAaYAUJ1yyX50hfIub0vHQ8BgyzUF8VLbnF8W4ziFwe4vpftb4mtKlRnqeEMZ8jCDCqAvwJzd8zWC8lkdmDFF5SqCQdoKV8/uZEAbIADHBCVVETAghK0uSbcWMF3MEADGP
                    wZBjT4Mwps8GcTSBReAvizDVTw5xjI4DsjWEGhv2MC6Ym5Il0xkG8kQAMTVOA7gR70pTFw6EQv4UHfscEJ/kyDFVSgAhJIQQoA6r4dJ0UAFljBDF6NgRTAIAXfOcEMIF2gGMTgzytY9
                    ndWIANFB3sGxMaAsSGNoBn4+dl+voAETmCCHQsgLMRInQm0bSETKJv+2TFgtQp+XR383UkCGIBBu7/zbmC/u9rn3vYItM1sGLhbBsBOAbWLDfDvwGAFF5g1uE8rgDCzl8wUtxQBdCBX
                    IvSAAYZwgBLEQAYxPEGlbraaRjyiABwQwQhF4AHH5+CAI4yBRk5AFQrxgvN44TznJbrPzn8O9IoFfeheCcvPHVM5on/lXkovetN3XvGodxQAC9hAAwLQNRKEQFIufc8oB3CDF/FAVEk
                    o1RNIIfW0q33tbG+7218jtft0apRzYMB3J1OEDDhgvAQrgxg8+fbAC37whC984DMFCU01gJsv0gAEwpmZvBp+8pSvvOUvT731zr0UBACpSILAgAJ8ou/+YHAA5k+P+tSrfvViY0ADrD
                    gJCohgz7bIgFwpwwFDUADyNCLBxFkP/OALf/gc3YAPiFCEIHggEhSAghjGAIYWoN0UDLjBDnKAyUNMYJNJADzxvw/+8Ivfowu4PWUyAPMZZUavuNDe+N8P//jLP2oceJERdgAAUqWkD
                    E6Y/vz/D4ABKIDz4gH25wMAcAR9BwpQQHsD6IAPCIERiBEaYH83AAAh0Hc1kgQSyIEd6IEfWAkDsAOT8QMKMAcHcASZAQYhAIIt6IIvGIAD0AE94AMgwDWGUAAs4ARQcAQkB4M/CIRB
                    KHwDYGeJUAANKIRJqIRLyIRN6IRPCIVRKIVT2IICgQAAOw=="""

        photo=PhotoImage(master=self, data=raw_data)
        display=Label(frame, image=photo, background="#BFBFBF")
        display.image=photo
        display.pack(ipady=10, ipadx=50, fill=BOTH)

        Label(frame, text=txt, anchor=E, font = "Fixedsys 10 bold").pack(ipady=0, ipadx=0, fill=BOTH)

        global running
        running = True

        #t = FuncThread(run)

        if txt == "Starting Software ":
            t = FuncThread(run)
        else:
            global updateObj
            t = FuncThread(updateObj.upgrade)
        t.daemon = True
        t.start()

        while running:
            self.parent.update_idletasks()
            self.parent.update()

        self.parent.destroy()


def main():
    root = Tk()
    root.overrideredirect(1)
    root.geometry("730x135")
    Logo(root, "Starting Software ")

    global updateObj, status

    if status == "no-internet":
        tkMessageBox.showinfo("Communication Error", "No Internet connection available. Please make sure that your device is connected to the Internet.")

    if status == "check-update":
        status = "start"
        updateObj.updateWindow()
        updateObj.window.destroy()
        if status == "update":
            root = Tk()
            root.overrideredirect(1)
            root.geometry("730x135")
            Logo(root, "Updating Software ")

    if status == "start":
        print "Python version: "+platform.architecture()[0]
        sys.path.append(os.path.normpath(os.path.join(PROBIS_DIRECTORY, "pymol")))
        import probis_pymol
        del status, updateObj # remove global variables
        probis_pymol.main()

    elif status == "failed":
        showManualDlInfo()


def __init__(self):
    self.menuBar.addmenuitem('Plugin', 'command', 'ProBiS', label = 'ProBiS', command=lambda s=self: main())
