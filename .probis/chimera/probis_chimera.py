from itertools import chain
import tkMessageBox
import chimera
import urllib2
import json, os
from os.path import join
from Tkinter import *
from ttk import *
import tkFont
import zipfile
from tkFileDialog import askopenfilename, askdirectory, asksaveasfilename
import subprocess
import multiprocessing
from time import sleep
from threading import Thread
import Queue
import platform
import webbrowser
import sys
import re
import shutil
import pickle
import collections
import cPickle as cpickle


probisliteDir = join(os.environ["probis-home"],".probis")
databaseFile = join(os.environ["probis-home"],".probis", "database.txt")
colors = ["red", "green", "blue", "yellow", "orange", "violet", "cyan", "pink", "gray", "magenta"]
ligandcolors = ['lightblue', 'lightorange', 'lightteal', 'limegreen', 'lithium', 'lightmagenta', 'lightpink', 'lime', 'limon']

sys.path.append(probisliteDir)

### Custom imports for PLIP
from kendrew.toolchain.webservice import query as restquery
from plip.modules.chimeraplip import ChimeraVisualizer
###
###

### Custom imports for LigandRMSD
from kendrew.REST.Molecules import LigandRMSDjson
###


#
# objects for saving data
#

class LigandRMSD(LigandRMSDjson):
    """Computes LigandRMSD within the ProBiS plugin."""
    def __init__(self, **args):
        super(LigandRMSD,self).__init__(**args)
        self.results = None
        self.url = 'https://projects.biotec.tu-dresden.de/ligandrmsd'

    def setQueryMol(self, pdbMolStr):
        """Provide the molecule as PDB File string and set it as query"""
        self.setQueryMols([pdbMolStr])

    def setQueryMols(self, listofpdbMolStr):
        """Provide a set of molecules as PDB File string in any iterable type and set these as queries"""
        print "calculating LigandRMSD for "+str(len(listofpdbMolStr))+" ligands"
        super(LigandRMSD,self).setQueryMols(listofpdbMolStr, molformat='pdb')

    def setTargetMol(self, pdbMolStr):
        """Provide the molecule as PDB File string and set it as target aka reference"""
        super(LigandRMSD,self).setTargetMol(pdbMolStr, molformat='pdb')

    def getMatchMol(self):
        """Gets the matching part of the two molecules with min LigandRMSD as PDB string."""
        return self.results['qmol_match']

    def compute(self, alwaysReturnAlist=True):
        return super(LigandRMSD,self).compute(alwaysReturnAlist=alwaysReturnAlist)

    def killjob(self):
        """Kills the current LigandRMSD jobs."""
        pass


### PLIP-specific code

class PLIPVisualization:
    """Provides access to detection of interactions with PLIP and their
    visualization in Chimera."""
    def __init__(self, protname, ligname, tid, uid):
        # Select protein model by proteinname + chain
        self.protname = protname
        self.protchain = self.protname[-1]
        self.ligname = ligname
        self.tid = tid
        self.plipname = 'PLIP-%i' % self.tid
        self.uid = uid
        self.hetid, self.chain, self.pos = self.uid.split(':')
        self.pos = int(self.pos)
        self.update_model_dict()
        self.tmpfile = '/'.join([probisliteDir, "data/plip.pdb"])
        #self.tmpfile = '/tmp/classtest/automatic.pdb'
        self.generate_pdbfile()

        with open(self.tmpfile, 'r') as f:
            self.pdbfile = f.read()

        self.renumber_atoms()

        PLIPURL = 'https://projects.biotec.tu-dresden.de/plip-rest'
        json_string = json.dumps({'pdbfile':self.pdbfile, 'mapping':None})
        rest_result = restquery(url=PLIPURL, data=json_string)
        self.complexes = pickle.loads(rest_result)

        cplx_dict = {c.uid: c for c in self.complexes}
        for cplx in cplx_dict:
            hetid, chain, pos = cplx.split(':')
            hetidr, chainr, posr = self.uid.split(':')
            if hetid == hetidr and pos == posr:
                chosen_complex = cplx_dict[cplx]
        self.chosen_complex = chosen_complex

        self.vis = ChimeraVisualizer(chosen_complex, chimera, self.tid)
        chimera.runCommand('~modeldisplay #0')

    def update_model_dict(self):
        """Updates the model dictionary"""
        dct = {}
        models = chimera.openModels
        for md in models.list():
            dct[md.name] = md.id
        self.model_dict = dct



    def generate_pdbfile(self):
        """Generates all PDB strings for selections: protein, ligand, complex"""
        # Make new model with reference protein and aligned ligand
        chimera.runCommand("delete #%i & :/isHet & ~:HOH" % self.model_dict[self.protname])
        chimera.runCommand('combine "#%i | #%i" name PLIP-%i' % (self.model_dict[self.protname], self.model_dict[self.ligname], self.tid))
        self.update_model_dict()
        chimera.runCommand("delete #%i & ~:.%s & ~:/isHet" % (self.model_dict[self.plipname], self.protchain))
        chimera.runCommand('write relative %i #%i %s' % (self.model_dict[self.plipname], self.model_dict[self.plipname], self.tmpfile))

    def renumber_atoms(self):
        """Renumber the atoms in the new model consecutively.
        Considering TER records."""

        d = {}
        j, i = 0, 0
        previous_ter = False
        for line in self.pdbfile.splitlines():
                if line.startswith(("ATOM", "HETATM")):
                    if not previous_ter:
                        i += 1
                        j += 1
                    else:
                        i += 1
                        j += 2
                    d[i] = j
                    previous_ter = False
                # Numbering Changes at TER records
                if line.startswith("TER"):
                    previous_ter = True

        # remap!
        model_to_remap = None
        for md in chimera.openModels.list():
            if md.name == self.plipname:
                model_to_remap = md
        print "Selected Model", model_to_remap
        for i, atom in enumerate(model_to_remap.atoms):
            atom.serialNumber = d[i+1]

    def show_interactions(self):
        """Visualizes all interactions."""
        self.vis.set_initial_representations()
        self.vis.show_hydrophobic()
        self.vis.show_hbonds()
        self.vis.show_halogen()
        self.vis.show_stacking()
        self.vis.show_cationpi()
        self.vis.show_sbridges()
        self.vis.show_wbridges()
        self.vis.show_metal()
        self.vis.zoom_to_ligand()
        self.vis.cleanup()
        self.vis.refinements()

    def update_class(self):
        """Updates information for stored class instances upon reloading."""
        self.update_model_dict()
        self.generate_pdbfile()
        self.update_model_dict()
        self.renumber_atoms()
        self.vis = ChimeraVisualizer(self.chosen_complex, chimera, self.tid)



#def color_carbons(color, selection):
#    """Colors only carbons in the given selection."""
#    cmd.color(color, '%s and symbol c' % (selection))

class bsiteObject:
    def __init__(self, id, name, protein, chain, zscore, selection, selectionA, ligandsArray):
        self.id = id
        self.name = name
        self.protein = protein
        self.chain = chain
        self.score = zscore
        self.sel = selection
        self.selA = selectionA
        self.ligs = ligandsArray

class ligsObject:
    def __init__(self, id, name, bindingSite, selection, selectionA, alignment):
        self.id = id
        self.name = name
        self.bsite = bindingSite
        self.sel = selection
        self.selA = selectionA
        self.align = alignment
        self.rmsd = ""


class alignObject:
    def __init__(self, id, name, selection, selectionA, ligandsArray, zscore):
        self.id = id
        self.name = name
        self.sel = selection
        self.selA = selectionA
        self.ligs = ligandsArray
        self.zscore = zscore


class RmsdThread(Thread):

    def __init__(self, probisFrame, target, indexes):

        Thread.__init__(self)
        self.target = target
        self.indexes = indexes
        self.ProbisFrame = probisFrame
        self.queue = self.ProbisFrame.queueRMSD

    def run(self):
        try:
            ligData = self.ProbisFrame.loadMoleculesForLigandRMSD(self.indexes)
            self.ProbisFrame.LigandRMSD.setQueryMols(ligData)
            rmsds = self.ProbisFrame.LigandRMSD.compute()
            print "returned LigandRMSD for "+str(len(rmsds)) + " ligands"

        except:
            rmsds = []
            self.indexes = []
            print "Error while computing LigandRMSD. Server down?"

        self.queue.put(self.target)
        self.queue.put(self.indexes)
        self.queue.put(rmsds)

        self.ProbisFrame.runningRmsd = 2



class FuncThread(Thread):

    def __init__(self, queue, probisFrame, target, indexes):
        self.queue = queue
        self.target = target
        self.indexes = indexes
        self.probisFrame = probisFrame
        Thread.__init__(self)

    def run(self):
        self.calculateLigandRMSD(self.target, self.indexes)




class RunInThread(Thread):

    def __init__(self, ProbisFrame, command, what, pdbID, chainID, queue):

        Thread.__init__(self)
        self.command = command
        self.what = what
        self.pdbID = pdbID
        self.chainID = chainID
        self.ProbisFrame = ProbisFrame
        self.queue = queue


    def run(self):

        # download ProBiS database
        if self.what == "download":
            self.ProbisFrame.dlInProgress = 1
            print "Downloading ProBiS database..."
            sys = platform.system()

            url = 'http://insilab.org/files/probis-plugin/livedb.zip'
            file_name = join(self.ProbisFrame.databaseLocation, "livedb.zip")
            u = urllib2.urlopen(url)
            f = open(file_name, 'wb')
            file_size = int(u.info().getheaders("Content-Length")[0])

            file_size_dl = 0
            block_sz = 4 * 1024
            while True:
                bufferFile = u.read(block_sz)
                if not bufferFile:
                    break
                file_size_dl += len(bufferFile)
                f.write(bufferFile)
                p = float(file_size_dl) / file_size
                self.queue.queue.clear()
                self.queue.put(round(p, 2))
            f.close()
            u.close()

            print "Setting up ProBiS database..."
            self.ProbisFrame.dlInProgress = 2
            self.queue.queue.clear()
            self.queue.put("Done")
            with zipfile.ZipFile(file_name, "r") as livedbzip:
                livedbzip.extractall(self.ProbisFrame.databaseLocation)

            os.remove(join(self.ProbisFrame.databaseLocation, "livedb.zip"))

            self.ProbisFrame.dlInProgress = 0
            self.ProbisFrame.running = 0
            print "ProBis done"

            with open(databaseFile, 'w+') as myfile:
                myfile.write(self.ProbisFrame.databaseLocation)
            if sys != "Windows":
                os.chmod(join(probisliteDir, "bin/probislite"), 0777)

        # run custom
        elif self.what == "probis":
            print "Started ProBiS for " + self.pdbID + ", chain " + self.chainID

            sys = platform.system()
            if sys == "Windows":
                info = subprocess.STARTUPINFO()
                info.dwFlags |= subprocess.STARTF_USESHOWWINDOW
                info.wShowWindow = subprocess.SW_HIDE
                self.ProbisFrame.probisProcess = subprocess.Popen(self.command, shell=False, startupinfo=info)
            else:
                self.ProbisFrame.probisProcess = subprocess.Popen(self.command, shell=False)

            while True:
                if self.ProbisFrame.probisProcess.poll() is not None:
                    self.queue.put("done")
                    break
                sleep(10)


#
# gui
#

class CreateToolTip(object):
    '''
    create a tooltip for a given widget
    '''
    def __init__(self, widget, text='widget info'):
        self.widget = widget
        self.text = text
        self.widget.bind("<Enter>", self.enter)
        self.widget.bind("<Leave>", self.close)
    def enter(self, event=None):
        x = y = 0
        x, y, cx, cy = self.widget.bbox("insert")
        x += self.widget.winfo_rootx() + 25
        y += self.widget.winfo_rooty() + 20
        # creates a toplevel window
        self.tw = Toplevel(self.widget)
        # Leaves only the label and removes the app window
        self.tw.wm_overrideredirect(True)
        self.tw.wm_geometry("+%d+%d" % (x, y))
        label = Label(self.tw, text=" "+self.text+" ", justify='left',
                       background='yellow', relief='solid', borderwidth=1,
                       font=("times", "10", "normal"))
        label.pack(ipadx=1)
    def close(self, event=None):
        if self.tw:
            self.tw.destroy()


class ProbisFrame(Frame):

    def __init__(self, parent):
        Frame.__init__(self, parent)
        self.queue = Queue.Queue()
        self.queueRMSD = Queue.Queue()
        self.rmsdJobs = ()
        self.runningRmsd = 0

        self.parent = parent
        self.parent.title("ProBiS - Protein Binding Sites")

        self.initUI()

        ### LigandRMSD-specific code
        self.tmol = None
        self.referenceLigandID = 0 # the ID of the ligand currently set as reference for LigandRMSD or PLIP
        self.LigandRMSD = LigandRMSD()
        #set PyMOL selection names
        self.ligrmsdmatchname = 'LigandRMSD_Match'
        self.ligrmsdreferencename = 'LigandRMSD_Reference'


    def updateReferenceLigand(self, referenceLigandID=None):
        """The reference Ligand is the ligand to which LigandRMSD is computed relative to. This function should be called as soon as the reference ligand ID is altered. A new ID can be provided as optional argument."""
        if referenceLigandID is not None:
            self.referenceLigandID = referenceLigandID
            self.refLigands[self.ligsObjects[referenceLigandID].bsite] = referenceLigandID

        print "reference ligand: "+str(self.referenceLigandID)
        ### LigandRMSD-specific code
        self.removeMoleculeFromPymol(self.ligrmsdreferencename)
        self.tmol = self.loadMoleculeToPymol("ligs-"+self.bsiteObjects[self.ligsObjects[referenceLigandID].bsite].name+".p", self.ligsObjects[referenceLigandID].name, "export")
        #color_carbons('grey', self.ligrmsdreferencename)
        with open(join(probisliteDir, "data/temp.pdb"), 'w') as myfile:
        	myfile.write(self.tmol)
        chimera.openModels.open(join(probisliteDir, "data/temp.pdb"), identifyAs=self.ligrmsdreferencename)
        self.LigandRMSD.setTargetMol(self.tmol)

    def initUI(self):

        #
        # logo
        #

        s = Style()
        s.configure("My.TFrame", background='#BFBFBF')

        frame=Frame(self, style="My.TFrame")
        frame.pack(fill=BOTH, expand=1)

        photo=PhotoImage(master=self, file=join(probisliteDir, "Icons/probis6.gif"))
        #photo = photoOriginal.subsample(3, 3)
        display=Label(frame, image=photo, background="#BFBFBF")
        display.image=photo
        display.pack(ipady=20, ipadx=50, fill=BOTH)
        #Label(frame, text="ProBiS Lite", foreground="#2B50D4", background="#BFBFBF", font="Times 25 bold", anchor=CENTER).pack(ipady=5, pady=2, fill=BOTH, expand=1)

        #
        # main tabs
        #

        self.note = Notebook(self)
        self.inputTab = Frame(self.note)
        self.outputTab = Frame(self.note)
        self.aboutTab = Frame(self.note)
        self.fileTab = Frame(self.note)

        self.note.add(self.inputTab, text="Input")
        self.note.add(self.outputTab, text="Output")
        self.note.add(self.fileTab, text="File")
        self.note.add(self.aboutTab, text="About")
        self.note.pack(padx=20, pady=(20,5), fill=BOTH,expand=1)


        #
        # input frame
        #

        noteInput = Notebook(self.inputTab)
        self.inputTabPDB = Frame(noteInput)
        self.inputTabCustom = Frame(noteInput)

        noteInput.add(self.inputTabPDB, text="PDB Protein")
        noteInput.add(self.inputTabCustom, text="Custom Protein")
        noteInput.pack(fill=BOTH,expand=1)

        # pdb protein frame
        Label(self.inputTabPDB, text="Get binding site and ligand prediction for a given protein structure:").grid(row=0, columnspan=3, padx=(50,0), pady=(50,20), sticky=W)
        Label(self.inputTabPDB, text="PDB ID:").grid(row=1, padx=(50, 0), pady=(10, 10), sticky=W)
        Label(self.inputTabPDB, text="Chain ID:").grid(row=2, padx=(50, 0), pady=(10,10), sticky=W)

        self.entryPdbID = Entry(self.inputTabPDB, width=10)
        self.entryChainID = Entry(self.inputTabPDB, width=10)
        self.entryPdbID.grid(row=1, column=1, pady=(10, 10), sticky=W)
        self.entryChainID.grid(row=2, column=1, pady=5, sticky=W)
        Button(self.inputTabPDB, text='GO', command=self.pdbProtein).grid(row=3, column=2, pady=(20,10), ipadx=10, ipady=5)

        # Custom Protein frame
        self.customProteinFrameStatus = StringVar()
        self.databaseAlreadyInstalled()
        self.createCustomFrame()


        #
        # output frame
        #

        self.noteOutput = Notebook(self.outputTab)
        self.outputTabSite = Frame(self.noteOutput)
        self.outputTabAlign = Frame(self.noteOutput)
        self.helpTab = Frame(self.noteOutput)

        self.noteOutput.add(self.outputTabSite, text="BSite View")
        self.noteOutput.add(self.outputTabAlign, text="Alignment View")
        self.noteOutput.add(self.helpTab, text="Help")
        self.noteOutput.pack(fill=BOTH,expand=1)

        self.outputTab.bind_all('<Key>', self.keyBoardEvent)

        #
        # bsite view tab
        #
        frameWrapper = Frame(self.outputTabSite)
        frameWrapper.pack(fill=BOTH, expand=1)
        self.frameOut1=Frame(frameWrapper)
        self.frameOut1.pack(side=LEFT, fill=BOTH, expand=1)
        self.frameOut2=Frame(frameWrapper)
        self.frameOut2.pack(side=LEFT, fill=BOTH, expand=1)
        self.frameOut3=Frame(frameWrapper)
        self.frameOut3.pack(side=LEFT,fill=BOTH, expand=1)

        frameOut4=Frame(self.outputTabSite, style='My.TFrame')
        frameOut4.pack(fill=BOTH)

        # scrollbar for bsite view
        self.columns1 = ('bsite-#', 'Binding Site', 'Zscore')
        self.treeBox1 = Treeview(self.frameOut1, columns=self.columns1, show="headings", selectmode="extended")
        vsb1 = Scrollbar(self.frameOut1, orient="vertical", command=self.treeBox1.yview)
        vsb1.pack(side=RIGHT, fill=Y)
        self.treeBox1.configure(yscrollcommand=vsb1.set)
        self.treeBox1.pack(side=LEFT, fill=BOTH, expand=1, padx=5, pady=5)

        for col in self.columns1:
            self.treeBox1.heading(col, text=col.title(), command=lambda c=col: self.sortby(self.treeBox1, c, 0))
            if self.columns1.index(col) == 1:
                self.treeBox1.column(col, width=tkFont.Font().measure(col.title()) + 50, anchor=CENTER)
            else:
                self.treeBox1.column(col, width=tkFont.Font().measure(col.title()) + 5, anchor=CENTER)
            self.treeBox1.heading(col, text=col, anchor=CENTER)

        self.treeBox1.bind("<<TreeviewSelect>>", self.multiListBox1Select)

        # Design of the ligands frame
        self.columns2 = ('lig-#', 'Predicted Ligands', 'LigandRMSD')
        self.treeBox2 = Treeview(self.frameOut2, columns=self.columns2, show="headings", selectmode="extended")
        vsb2 = Scrollbar(self.frameOut2, orient="vertical", command=self.treeBox2.yview)
        vsb2.pack(side=RIGHT, fill=Y)
        self.treeBox2.configure(yscrollcommand=vsb2.set)
        self.treeBox2.pack(side=LEFT, fill=BOTH, expand=1, padx=5, pady=5)

        for col in self.columns2:
            self.treeBox2.heading(col, text=col.title(), command=lambda c=col: self.sortby(self.treeBox2, c, 0))
            if self.columns2.index(col) == 1:
                self.treeBox2.column(col, width=tkFont.Font().measure(col.title()) + 50, anchor=CENTER)
            else:
                self.treeBox2.column(col, width=tkFont.Font().measure(col.title()) + 5, anchor=CENTER)
            self.treeBox2.heading(col, text=col, anchor=CENTER)

        self.treeBox2.bind("<<TreeviewSelect>>", self.multiListBox2Select)
        self.treeBox2.bind("<Button-3>", self.resetRMSDbsite)

        # Design of the alignments frame
        self.columns3 = ('align-#','Alignments', 'Zscore')
        self.treeBox3 = Treeview(self.frameOut3, columns=self.columns3, show="headings", selectmode="extended")
        vsb3 = Scrollbar(self.frameOut3, orient="vertical", command=self.treeBox3.yview)
        vsb3.pack(side=RIGHT, fill=Y)
        self.treeBox3.configure(yscrollcommand=vsb3.set)
        self.treeBox3.pack(side=LEFT, fill=BOTH, expand=1, padx=5, pady=5)

        for col in self.columns3:
            self.treeBox3.heading(col, text=col.title(), command=lambda c=col: self.sortby(self.treeBox3, c, 0))
            if self.columns3.index(col) == 1:
                self.treeBox3.column(col, width=tkFont.Font().measure(col.title()) + 80, anchor=CENTER)
            else:
                self.treeBox3.column(col, width=tkFont.Font().measure(col.title()) + 5, anchor=CENTER)
            self.treeBox3.heading(col, text=col, anchor=CENTER)

        self.treeBox3.bind("<<TreeviewSelect>>", self.multiListBox3Select)


        #
        # alignment view tab
        #

        frameWrapper2 = Frame(self.outputTabAlign)
        frameWrapper2.pack(fill=BOTH, expand=1)
        self.frameOutA1=Frame(frameWrapper2)
        self.frameOutA1.pack(side=LEFT, fill=BOTH, expand=1)
        self.frameOutA2=Frame(frameWrapper2)
        self.frameOutA2.pack(side=LEFT, fill=BOTH, expand=1)
        self.frameOutA3=Frame(frameWrapper2)
        self.frameOutA3.pack(side=LEFT, fill=BOTH, expand=1)
        frameOutA4=Frame(self.outputTabAlign, style='My.TFrame')
        frameOutA4.pack(fill=BOTH)

        # scrollbar for bsite view
        self.columnsA1 = ('align-#','Alignments', 'Zscore')
        self.treeBoxA1 = Treeview(self.frameOutA1, columns=self.columnsA1, show="headings", selectmode="extended")
        vsbA1 = Scrollbar(self.frameOutA1, orient="vertical", command=self.treeBoxA1.yview)
        vsbA1.pack(side=RIGHT, fill=Y)
        self.treeBoxA1.configure(yscrollcommand=vsbA1.set)
        self.treeBoxA1.pack(side=LEFT, fill=BOTH, expand=1, padx=5, pady=5)

        for col in self.columnsA1:
            self.treeBoxA1.heading(col, text=col.title(), command=lambda c=col: self.sortby(self.treeBoxA1, c, 0))
            if self.columnsA1.index(col) == 1:
                self.treeBoxA1.column(col, width=tkFont.Font().measure(col.title()) + 50, anchor=CENTER)
            else:
                self.treeBoxA1.column(col, width=tkFont.Font().measure(col.title()) + 5, anchor=CENTER)
            self.treeBoxA1.heading(col, text=col, anchor=CENTER)

        self.treeBoxA1.bind("<<TreeviewSelect>>", self.multiListBoxA1Select)

        # Design of the ligands frame
        self.columnsA2 = ('lig-#', 'Predicted Ligands', 'LigandRMSD')
        self.treeBoxA2 = Treeview(self.frameOutA2, columns=self.columnsA2, show="headings", selectmode="extended")
        vsbA2 = Scrollbar(self.frameOutA2, orient="vertical", command=self.treeBoxA2.yview)
        vsbA2.pack(side=RIGHT, fill=Y)
        self.treeBoxA2.configure(yscrollcommand=vsbA2.set)
        self.treeBoxA2.pack(side=LEFT, fill=BOTH, expand=1, padx=5, pady=5)

        for col in self.columnsA2:
            self.treeBoxA2.heading(col, text=col.title(), command=lambda c=col: self.sortby(self.treeBoxA2, c, 0))
            if self.columnsA2.index(col) == 1:
                self.treeBoxA2.column(col, width=tkFont.Font().measure(col.title()) + 50, anchor=CENTER)
            else:
                self.treeBoxA2.column(col, width=tkFont.Font().measure(col.title()) + 5, anchor=CENTER)
            self.treeBoxA2.heading(col, text=col, anchor=CENTER)

        self.treeBoxA2.bind("<<TreeviewSelect>>", self.multiListBoxA2Select)
        self.treeBoxA2.bind("<Button-3>", self.resetRMSDalign)

        # Design of the alignments frame
        self.columnsA3 = ('bsite-#', 'Binding Site', 'Zscore')
        self.treeBoxA3 = Treeview(self.frameOutA3, columns=self.columnsA3, show="headings", selectmode="extended")
        vsbA3 = Scrollbar(self.frameOutA3, orient="vertical", command=self.treeBoxA3.yview)
        vsbA3.pack(side=RIGHT, fill=Y)
        self.treeBoxA3.configure(yscrollcommand=vsbA3.set)
        self.treeBoxA3.pack(side=LEFT, fill=BOTH, expand=1, padx=5, pady=5)

        for col in self.columnsA3:
            self.treeBoxA3.heading(col, text=col.title(), command=lambda c=col: self.sortby(self.treeBoxA3, c, 0))
            if self.columnsA3.index(col) == 1:
                self.treeBoxA3.column(col, width=tkFont.Font().measure(col.title()) + 80, anchor=CENTER)
            else:
                self.treeBoxA3.column(col, width=tkFont.Font().measure(col.title()) + 5, anchor=CENTER)
            self.treeBoxA3.heading(col, text=col, anchor=CENTER)

        self.treeBoxA3.bind("<<TreeviewSelect>>", self.multiListBoxA3Select)


        # quick tip
        #tipWrapper = Frame(self.outputTabAlign)
        #tipWrapper.pack(fill=X, expand=YES, padx=(6,5), pady=(0,2))
        #Label(tipWrapper, text="Enable PLIP visualization with Ctrl + P", anchor=W, justify=LEFT, font=("Helvetica", 8)).pack(side=LEFT, fill=X, expand=YES)

        #tipWrapper2 = Frame(self.outputTabSite)
        #tipWrapper2.pack(fill=X, expand=YES, padx=(6,5), pady=(0,2))
        #Label(tipWrapper2, text="Enable PLIP visualization with Ctrl + P", anchor=W, justify=LEFT, font=("Helvetica", 8)).pack(side=LEFT, fill=X, expand=YES)


        #
        # help tab
        #

        columnNameBox = LabelFrame(self.helpTab,text=" Column Legend ",labelanchor="nw",relief="ridge",borderwidth=4)
        columnNameBox.pack(fill=X,padx=(40,40),pady=(20,10))

        rowIndex = 0
        Label(columnNameBox, text="bsite-#", font=("Helvetica", 8)).grid(row=rowIndex, column=0, pady=3, padx=(10,5), sticky=W)
        Label(columnNameBox, text="ID of representative binding site", font=("Helvetica", 8)).grid(row=rowIndex, column=1, pady=3, padx=(10,5), sticky=W)

        rowIndex += 1
        Label(columnNameBox, text="Binding Site", font=("Helvetica", 8)).grid(row=rowIndex, column=0, pady=3, padx=(10,5), sticky=W)
        Label(columnNameBox, text="Representative binding site code: PDBID.#LIGATOMS.LIGCODE.LIGNUMBER.CHAINID.CHAINID_bsite-#", font=("Helvetica", 8)).grid(row=rowIndex, column=1, pady=3, padx=(10,5), sticky=W)

        rowIndex += 1
        Label(columnNameBox, text="Zscore", font=("Helvetica", 8)).grid(row=rowIndex, column=0, pady=3, padx=(10,5), sticky=W)
        Label(columnNameBox, text="Maximum normalized alignment score", font=("Helvetica", 8)).grid(row=rowIndex, column=1, pady=3, padx=(10,5), sticky=W)

        rowIndex += 1
        Label(columnNameBox, text="lig-#", font=("Helvetica", 8)).grid(row=rowIndex, column=0, pady=3, padx=(10,5), sticky=W)
        Label(columnNameBox, text="ID of predicted ligand (reference ligand for ligand RMSD calculations is marked with a star)", font=("Helvetica", 8)).grid(row=rowIndex, column=1, pady=3, padx=(10,5), sticky=W)

        rowIndex += 1
        Label(columnNameBox, text="Predicted Ligands", font=("Helvetica", 8)).grid(row=rowIndex, column=0, pady=3, padx=(10,5), sticky=W)
        Label(columnNameBox, text="Predicted ligand code: LIGCODE.LIGNUMBER.CHAINID.MODELID:ASSEMBLYID", font=("Helvetica", 8)).grid(row=rowIndex, column=1, pady=3, padx=(10,5), sticky=W)

        rowIndex += 1
        Label(columnNameBox, text="LigandRMSD", font=("Helvetica", 8)).grid(row=rowIndex, column=0, pady=3, padx=(10,5), sticky=W)
        Label(columnNameBox, text="Root-mean-square deviation of atomic positions", font=("Helvetica", 8)).grid(row=rowIndex, column=1, pady=3, padx=(10,5), sticky=W)

        rowIndex += 1
        Label(columnNameBox, text="align-#", font=("Helvetica", 8)).grid(row=rowIndex, column=0, pady=3, padx=(10,5), sticky=W)
        Label(columnNameBox, text="ID of alignment", font=("Helvetica", 8)).grid(row=rowIndex, column=1, pady=3, padx=(10,5), sticky=W)

        rowIndex += 1
        Label(columnNameBox, text="Alignments", font=("Helvetica", 8)).grid(row=rowIndex, column=0, pady=3, padx=(10,5), sticky=W)
        Label(columnNameBox, text="Alignment code: PDBID.#LIGATOMS.LIGCODE.LIGNUMBER.CHAINID.CHAINID", font=("Helvetica", 8)).grid(row=rowIndex, column=1, pady=3, padx=(10,5), sticky=W)

        rowIndex += 1
        Label(columnNameBox, text="Zscore", font=("Helvetica", 8)).grid(row=rowIndex, column=0, pady=3, padx=(10,5), sticky=W)
        Label(columnNameBox, text="Normalized alignment score", font=("Helvetica", 8)).grid(row=rowIndex, column=1, pady=3, padx=(10,5), sticky=W)


        keyboardBox = LabelFrame(self.helpTab,text=" ListBox Keyboard Shortcuts ",labelanchor="nw",relief="ridge",borderwidth=4)
        keyboardBox.pack(fill=X,padx=(40,40),pady=(10,20))

        rowIndex = 0
        Label(keyboardBox, text="Ctrl + left-mouse click", font=("Helvetica", 8)).grid(row=rowIndex, column=0, pady=3, padx=(10,5), sticky=W)
        Label(keyboardBox, text="Multiple selection", font=("Helvetica", 8)).grid(row=rowIndex, column=1, pady=3, padx=(10,5), sticky=W)

        rowIndex += 1
        Label(keyboardBox, text="Ctrl + P", font=("Helvetica", 8)).grid(row=rowIndex, column=0, pady=3, padx=(10,5), sticky=W)
        Label(keyboardBox, text="Show PLIP interactions for a ligand", font=("Helvetica", 8)).grid(row=rowIndex, column=1, pady=3, padx=(10,5), sticky=W)

        #rowIndex += 1
        #Label(keyboardBox, text="right-mouse click", font=("Helvetica", 8)).grid(row=rowIndex, column=0, pady=3, padx=(10,5), sticky=W)
        #Label(keyboardBox, text="Toggle PLIP visualization for (single!) selected ligand", font=("Helvetica", 8)).grid(row=rowIndex, column=1, pady=3, padx=(10,5), sticky=W)

        rowIndex += 1
        Label(keyboardBox, text="Ctrl + R", font=("Helvetica", 8)).grid(row=rowIndex, column=0, pady=3, padx=(10,5), sticky=W)
        Label(keyboardBox, text="Set selected ligand as reference ligand for ligand RMSD calculations", font=("Helvetica", 8)).grid(row=rowIndex, column=1, pady=3, padx=(10,5), sticky=W)

        rowIndex += 1
        Label(keyboardBox, text="right-mouse click", font=("Helvetica", 8)).grid(row=rowIndex, column=0, pady=3, padx=(10,5), sticky=W)
        Label(keyboardBox, text="Set selected ligand as reference ligand for ligand RMSD calculations", font=("Helvetica", 8)).grid(row=rowIndex, column=1, pady=3, padx=(10,5), sticky=W)

        rowIndex += 1
        Label(keyboardBox, text="left-mouse click on column header", font=("Helvetica", 8)).grid(row=rowIndex, column=0, pady=3, padx=(10,5), sticky=W)
        Label(keyboardBox, text="Sorting by column on the respective box", font=("Helvetica", 8)).grid(row=rowIndex, column=1, pady=3, padx=(10,5), sticky=W)

        # double click allows sorting by lig-#, or Predicted Ligand names, or LigandRMSD (on the respective box)

        #
        # file tab
        #

        loadProjectBox = LabelFrame(self.fileTab,text="Load Project",labelanchor="nw",relief="ridge",borderwidth=4)
        exportMoleculesBox = LabelFrame(self.fileTab,text="Save Molecules",labelanchor="nw",relief="ridge",borderwidth=4)
        exportProjectBox = LabelFrame(self.fileTab,text="Export Project",labelanchor="nw",relief="ridge",borderwidth=4)
        loadProjectBox.pack(fill=X,padx=(40,40),pady=(20,20))
        exportMoleculesBox.pack(fill=X,padx=(40,40),pady=(20,20))
        exportProjectBox.pack(fill=X,padx=(40,40),pady=(20,20))

        # load project box
        Label(loadProjectBox, text="File: ").grid(row=0, column=0,pady=(10, 10), padx=(25,5), sticky=W)
        self.entryProjectFileLocation = Entry(loadProjectBox, width=40)
        self.entryProjectFileLocation.grid(row=0, column=1, padx=(5,5), pady=3)

        Button(loadProjectBox, text='Search', command=lambda: self.searchPathFile(1)).grid(row=0, column=2, padx=(0,5), sticky=W)
        Button(loadProjectBox, text='Load', command=self.loadProjectFromFile).grid(row=0, column = 3, pady=(10,10), padx=(200,0), sticky=E)

        # save molecules
        Button(exportMoleculesBox, text='Save Molecules', command=self.saveSelectedObjects).grid(column=0, row=0,pady=10, padx=(20,0))

        # export project
        Label(exportProjectBox, text="File: ").grid(row=0, column=0,pady=(10, 10), padx=(25,5), sticky=W)
        self.entryExportProjectFile = Entry(exportProjectBox, width=40)
        self.entryExportProjectFile.grid(row=0, column=1, padx=(5,5), pady=3)

        Button(exportProjectBox, text='Search', command=lambda: self.searchPathFile(2)).grid(row=0, column=2, padx=(0,5), sticky=W)
        Button(exportProjectBox, text='Export', command=self.exportProjectToFile).grid(row=0, column = 3, pady=(10,10), padx=(200,0), sticky=E)

        #
        # about tab
        #

        self.createAboutTab()

        # status labels
        statusWrapper = Frame(self)
        statusWrapper.pack(fill=X, expand=YES, padx=20)

        statusWrapper2 = Frame(statusWrapper)
        statusWrapper2.pack(fill=X, expand=YES, padx=(0,5))

        #self.statusPlipLabel = StringVar(statusWrapper2)
        #self.statusPlipLabel.set("Press Ctrl+P to show PLIP interactions for a ligand")
        #Label(statusWrapper2, textvariable=self.statusPlipLabel, anchor=W, justify=LEFT, font=("Helvetica", 8)).pack(side=LEFT, fill=X, expand=YES)

        self.statusRMSDLabel = StringVar(statusWrapper2)
        Label(statusWrapper2, textvariable=self.statusRMSDLabel,  anchor=W, justify=LEFT, font=("Helvetica", 8)).pack(side=LEFT, fill=X, expand=YES)

        self.queryStringLabel = StringVar(statusWrapper2)
        Label(statusWrapper2, textvariable=self.queryStringLabel, font=("Helvetica", 8), anchor=E).pack(side=LEFT, fill=X, expand=YES)

        #Exit button
        Button(self, text='Exit', command=self.exitButton).pack(pady=10)

        self.pack(fill=BOTH, expand=1)

        # set global variables
        self.selectedLigands = []
        self.selectedAlignments = []
        self.selectedBindingSites = []
        self.bsiteId = ""
        self.bsiteIdPymol = -1
        self.bsiteIdPymolA = -1
        self.bsiteAId = ""
        self.selectedLigandsA = []
        self.selectedAlignmentsA = []
        self.selectedBindingSitesA = []

        self.queryProtein = ""
        self.queryChain = ""

        # PLIP-specific
        self.plipvisualizations = {} # Stores already calculated interactions

        # rmsg flags
        self.initBview = {}
        self.initAview = {}
        self.refLigands = {}


    #
    # set custom protein view
    #
    def createCustomFrame(self):
        for elt in self.inputTabCustom.winfo_children():
            elt.destroy()

        # download database frame
        if self.customProteinFrameStatus.get() == "databaseInput":

            Label(self.inputTabCustom, text="Running ProBiS custom protein calculation requires installation of \"livedb\" database.").grid(row=0, sticky=W,column=0, columnspan=2, padx=(50,0), pady=(50,5))

            Label(self.inputTabCustom, text="Please choose one of the following:").grid(row=1, column=0, pady=(5, 0), padx=(50,0), sticky=W)
            self.databaseRadioVal = IntVar(self)
            Radiobutton(self.inputTabCustom, text="Install ProBiS database", variable=self.databaseRadioVal, value=1).grid(row=2, column=0, pady=(20, 0), padx=(100,0), sticky=W)
            Radiobutton(self.inputTabCustom, text="ProBiS database is already installed", variable=self.databaseRadioVal, value=2).grid(row=3, column=0, pady=(15, 0), padx=(100,0), sticky=W)

            Button(self.inputTabCustom, text='Next >>', command=self.databaseOption).grid(row=4, column=1, pady=(50, 0), padx=(0,5), sticky=E)


        elif self.customProteinFrameStatus.get() == "databaseInput2":

            if self.databaseRadioVal.get() == 2:
                Label(self.inputTabCustom, text="Set ProBiS database directory: ").grid(row=0, column=0, sticky=W, padx=(100,0), pady=(80, 10))
            else:
                Label(self.inputTabCustom, text="Set installation directory: ").grid(row=0, column=0, sticky=W, padx=(100,0), pady=(80, 10))

            self.entryDatabaselocation = Entry(self.inputTabCustom, width=40)
            self.entryDatabaselocation.grid(row=1, column=0, padx=(100,0), pady=3)

            Button(self.inputTabCustom, text='Search', command=self.searchPathDir).grid(row=1, column=1, padx=(0,5), sticky=W)
            Button(self.inputTabCustom, text='<< Back', command=self.backDlFrame).grid(row=2, column=0, pady=(50, 0), padx=(0, 5), sticky=E)

            if self.databaseRadioVal.get() == 2:
                Button(self.inputTabCustom, text='Next >>', command=self.downloadOrCheck).grid(row=2, column=1, pady=(50, 0), padx=(0, 5), sticky=W)
            else:
                Button(self.inputTabCustom, text='Install', command=self.downloadOrCheck).grid(row=2, column=1, pady=(50, 0), padx=(0, 5), sticky=W)


        # probis is working frame
        elif self.customProteinFrameStatus.get() == "probisWorking":

            Label(self.inputTabCustom, text="ProBiS is working...", font=30).grid(row=0, sticky=W, padx=(50,0), pady=(50, 2))
            Label(self.inputTabCustom, text="This may take half an hour or more. Time for coffee break!").grid(row=1, sticky=W, padx=(50,0), pady=(20, 2))

            self.progressBarProbis = Progressbar(self.inputTabCustom, orient='horizontal', mode='indeterminate', length=750)
            self.progressBarProbis.grid(row=2, sticky=W, padx=50, pady=(20, 10))
            self.progressBarProbis.start()

            Button(self.inputTabCustom, text='Cancel ProBiS', command=self.cancelProbis).grid(row=3, sticky=E, padx=(0,50), pady=(20,2))

        # downloading database frame
        elif self.customProteinFrameStatus.get() == "databaseWorking":

            self.labelToDelete = Label(self.inputTabCustom, text="Downloading Database: ")
            self.labelToDelete.grid(row=1, sticky=W, padx=50, pady=(50,5))

            self.showStatus = DoubleVar(self)
            self.showStatus.set(0.0)
            self.progressBarDl = Progressbar(self.inputTabCustom, orient='horizontal', mode='determinate', length=750, variable=self.showStatus, maximum=1)
            self.progressBarDl.grid(row=2, sticky=W, padx=50, pady=(20,2))
            self.progressBarDl.start()

            Label(self.inputTabCustom, text="Extracting ProBiS database... Do not close the window.").grid(row=3, sticky=W, padx=50, pady=(50, 2))

            self.progressBarExtract = Progressbar(self.inputTabCustom, orient='horizontal', mode='indeterminate', length=750)
            self.progressBarExtract.grid(row=4, sticky=W, padx=50, pady=(20, 2))

        # input probis frame
        else:
            Label(self.inputTabCustom, text="Path to protein file: ").grid(row=0, sticky=W, column=0, padx=(50,0), pady=(50,2))
            self.entryProteinLocation = Entry(self.inputTabCustom, width=40)
            self.entryProteinLocation.grid(row=1, sticky=W, padx=(50,5), pady=3, column=0, columnspan=2)

            Label(self.inputTabCustom, text="Chain ID: ").grid(row=2, sticky=W, padx=(50,0), pady=(15, 2), column = 0)
            self.entryChainId = Entry(self.inputTabCustom, width=10)
            self.entryChainId.grid(row=2, sticky=W, padx=(180, 0), pady=(15, 10), column = 0)

            r = 3
            lbZscore = Label(self.inputTabCustom, text="ProBiS Z-score: ")
            lbZscore.grid(row=r, sticky=W, padx=(50,0), pady=(15, 2))
            CreateToolTip(lbZscore, "Minimium z-score of ligands to be considered in clustering")

            self.entryZscore = Entry(self.inputTabCustom, width=10)
            self.entryZscore.insert(0, '2.5')
            self.entryZscore.grid(row=r, sticky=W, padx=(180, 0), pady=(15, 2))
            #CreateToolTip(self.lb, "mouse is over button 1")

            r += 1
            lbClRadius = Label(self.inputTabCustom, text="Cluster Radius: ")
            lbClRadius.grid(row=r, sticky=W, padx=(50,0), pady=(15, 2))
            CreateToolTip(lbClRadius, "Cluster radius for predicted ligands by probis")

            self.entryClRadius = Entry(self.inputTabCustom, width=10)
            self.entryClRadius.insert(0, '3.0')
            self.entryClRadius.grid(row=r, sticky=W, padx=(180, 0), pady=(15, 2))

            r += 1
            lbCenRadius = Label(self.inputTabCustom, text="Centroid Radius: ")
            lbCenRadius.grid(row=r, sticky=W, padx=(50,0), pady=(15, 2))
            CreateToolTip(lbCenRadius, "Cluster radius for centroid centers")

            self.entryCenRadius = Entry(self.inputTabCustom, width=10)
            self.entryCenRadius.insert(0, '3.0')
            self.entryCenRadius.grid(row=r, sticky=W, padx=(180, 0), pady=(15, 2))

            r += 1
            lbInter = Label(self.inputTabCustom, text="Interatomic: ")
            lbInter.grid(row=r, sticky=W, padx=(50,0), pady=(15, 2))
            CreateToolTip(lbInter, "Maximum interatomic distance")

            self.entryInter = Entry(self.inputTabCustom, width=10)
            self.entryInter.insert(0, '8.0')
            self.entryInter.grid(row=r, sticky=W, padx=(180, 0), pady=(15, 2))

            r += 1
            lbExcluded = Label(self.inputTabCustom, text="Excluded Radius: ")
            lbExcluded.grid(row=r, sticky=W, padx=(50,0), pady=(15, 2))
            CreateToolTip(lbExcluded, "Distance between surface of protein atom and surface of binding site grid")

            self.entryExcluded = Entry(self.inputTabCustom, width=10)
            self.entryExcluded.insert(0, '0.8')
            self.entryExcluded.grid(row=r, sticky=W, padx=(180, 0), pady=(15, 2))

            r = 3
            lbGrid = Label(self.inputTabCustom, text="Grid Spacing: ")
            lbGrid.grid(row=r, sticky=W, padx=(50,0), pady=(15, 2), column=1)
            CreateToolTip(lbGrid, "Distance between binding site grid points")

            self.entryGrid = Entry(self.inputTabCustom, width=10)
            self.entryGrid.insert(0, '0.5')
            self.entryGrid.grid(row=r, sticky=W, padx=(180, 0), pady=(15, 2), column=1)

            r += 1
            lbNumSites = Label(self.inputTabCustom, text="Number of BSites: ")
            lbNumSites.grid(row=r, sticky=W, padx=(50,0), pady=(15, 2), column=1)
            CreateToolTip(lbNumSites, "Maximum number of predicted (or given) binding sites to consider for docking")

            self.entryNumSites = Entry(self.inputTabCustom, width=10)
            self.entryNumSites.insert(0, '3')
            self.entryNumSites.grid(row=r, sticky=W, padx=(180, 0), pady=(15, 2), column=1)


            r += 1
            lbRmsdScore = Label(self.inputTabCustom, text="ligandRMSD Z-score: ")
            lbRmsdScore.grid(row=r, sticky=W, padx=(50,0), pady=(15, 2), column=1)
            CreateToolTip(lbRmsdScore, "Minimium z-score of ligands to be considered for LigandRMSD screening")

            self.entryRmsdScore = Entry(self.inputTabCustom, width=10)
            self.entryRmsdScore.insert(0, '0.5')
            self.entryRmsdScore.grid(row=r, sticky=W, padx=(180, 0), pady=(15, 2), column=1)


            r += 1
            lbPts = Label(self.inputTabCustom, text="Min Points: ")
            lbPts.grid(row=r, sticky=W, padx=(50,0), pady=(15, 2), column=1)
            CreateToolTip(lbPts, "The minimum number of points (for predicted ligands) required to form a cluster")

            self.entryPts = Entry(self.inputTabCustom, width=10)
            self.entryPts.insert(0, '10')
            self.entryPts.grid(row=r, sticky=W, padx=(180, 0), pady=(15, 2), column=1)


            r += 1
            lbCutoff = Label(self.inputTabCustom, text="Cutoff Length: ")
            lbCutoff.grid(row=r, sticky=W, padx=(50,0), pady=(15, 2), column=1)
            CreateToolTip(lbCutoff, "Possible input values: 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15")

            self.entryCutoff = Entry(self.inputTabCustom, width=10)
            self.entryCutoff.insert(0, '6')
            self.entryCutoff.grid(row=r, sticky=W, padx=(180, 0), pady=(15, 2), column=1)


            r += 1
            Button(self.inputTabCustom, text='Search', command=lambda: self.searchPathFile(0)).grid(row=1, column=1, padx=(60,0))
            Button(self.inputTabCustom, text='Start ProBiS', command=self.customProtein).grid(row=r, pady=(30,10), sticky=E, ipadx=5, ipady=5, column=1)

        self.inputTabCustom.update()


    #
    # create about tab
    #
    def createAboutTab(self):
        for elt in self.aboutTab.winfo_children():
            elt.destroy()

        productVersion = "unknown"
        latestVersion = ""

        versionFile = join(probisliteDir,"version.txt")
        if os.path.isfile(versionFile):
            with open(versionFile) as lFile:
                lines = lFile.readlines()
            productVersion = lines[0].strip()
            try:
                url = 'http://insilab.org/files/probis-plugin/version.txt'
                u = urllib2.urlopen(url, timeout=5)
                latestVersion = u.read().strip()
                if len(latestVersion) > 6:
                    latestVersion = ""
                u.close()
            except:
                pass


        versionBox = LabelFrame(self.aboutTab,text="Product Version Information",labelanchor="nw",relief="ridge",borderwidth=4)
        contactBox = LabelFrame(self.aboutTab,text="Contact",labelanchor="nw",relief="ridge",borderwidth=4)
        versionBox.pack(fill=X,padx=(40,40),pady=(20,20))
        contactBox.pack(fill=X,padx=(40,40),pady=(20,20))

        # version box
        Label(versionBox, text="ProBiS plugin version: " + productVersion).grid(row=0, column=0, pady=(10, 10), padx=10, sticky=W)
        if latestVersion != productVersion and productVersion != "unknown" and latestVersion != "":
            Label(versionBox, text="There is a "+ latestVersion + " version of ProBiS available.").grid(row=1, column=0, pady=(0, 10), padx=10, sticky=W)
            Button(versionBox, text='Update Now', command=lambda: self.updateProgramVersion(latestVersion) ).grid(row=1, column=1, pady=(0,10), padx=(160,0), sticky=E)

        # contact box
        Label(contactBox, text="Contact us at: konc@cmm.ki.si").grid(row=0, column=0, pady=(10, 3), padx=10, sticky=W)
        Label(contactBox, text="For more information visit:").grid(row=1, column=0, pady=(3, 10), padx=(10,5), sticky=W)
        urlLabel = Label(contactBox, text="http://insilab.org/probis-plugin", font=("Times",11),foreground="blue",underline=True)
        urlLabel.grid(row=1, column=1, pady=(3, 10), padx=(5,5), sticky=W)
        urlLabel.bind("<Button-1>", self.open_url)




    #
    # sort treeview by column
    #

    def sortby(self, tree, col, descending):

        data = [(tree.set(child, col), child) for child in tree.get_children('')]

        if col in ["bsite-#", "Zscore", "LigandRMSD", "align-#"]:
            data.sort(reverse=descending, key=lambda txt: float(txt[0]))
        elif col == "lig-#":
            data.sort(reverse=descending, key=lambda txt: float(txt[0].replace("*", "")))
        else:
            data.sort(reverse=descending)

        for indx, item in enumerate(data):
            tree.move(item[1], '', indx)
        # switch the heading so that it will sort in the opposite direction
        tree.heading(col, command=lambda col=col: self.sortby(tree, col, int(not descending)))




    #
    # update progress
    #
    def poll(self):
        if self.queue.qsize():
            try:
                msg = self.queue.get(block = True)
                if msg == "Done":
                    self.progressBarExtract.start()
                    self.queue.queue.clear()
                self.showStatus.set(msg)
            except Exception:
                pass

        if self.running == 1:
            self.parent.after(200, self.poll)

        elif self.running == 0:
            self.progressBarExtract.stop()
            tkMessageBox.showinfo("ProBiS", "Installation of ProBiS database succeeded.", parent=self)
            self.customProteinFrameStatus.set("probisInput")
            self.createCustomFrame()


    #
    # update progress
    #
    def pollRmsd(self):

        if self.runningRmsd == 1:
            self.parent.after(200, self.pollRmsd)

        elif self.runningRmsd == 2:
            view = self.queueRMSD.get(block = True)
            indexes = self.queueRMSD.get(block = True)
            rmsd = self.queueRMSD.get(block = True)
            self.afterLigandRMSD(view, indexes, rmsd)


    #
    # start probis for custom protein
    #
    def runProbis (self, pdbID, chainID):

        if self.queue.qsize():
            if self.queue.get() != -1:

                chimera.openModels.open(self.proteinLocation, identifyAs=pdbID+chainID)
                for objekt in chimera.openModels.list():
                    if objekt.name == pdbID+chainID:
                        chimera.runCommand("center #"+str(objekt.id))
                        chimera.runCommand("~show #" + str(objekt.id))
                        chimera.runCommand("~ribbon #" + str(objekt.id))
                        l = chainID.split()
                        for i in range(0, len(l)):
                            chimera.runCommand("ribbon #" + str(objekt.id) + ":." + str(l[i]))
                        break

                # save ligands names to list [(bsite name, [ligands]), (bsite name2), [ligands]...)
                numGrids = 0
                clusterLigands = []
                prevName = ""
                ligandContent = ""
                ligandName = ""
                ligandsDict = {}
                alignDict = {}
                zscoresAlignments = {}
                maxZscores = {}
                alignIdx = 0
                self.bsiteObjects = []
                self.ligsObjects = []
                self.alignObjects = []

                try:
                    with open(join(self.databaseLocation, "livedb/results/zscores.pdb"), "r") as myfile:
                        zscores = myfile.read().splitlines()

                        for i in range(0, len(zscores)):
                            if len(zscores[i]) > 0:
                                zscore = zscores[i].split(' ')
                                aCluster = int(zscore[0])
                                aName = (zscore[1].split(":"))[6]
                                aScore = float(zscore[2])
                                zscoresAlignments[aName] = aScore
                                if aCluster in maxZscores:
                                    if maxZscores[aCluster] < aScore:
                                        maxZscores[aCluster] = aScore
                                else:
                                    maxZscores[aCluster] = aScore

                    for line in open(join(self.databaseLocation, "livedb/results/ligands.pdb"), 'r'):
                        if re.search("CLUSTER", line):
                            numGrids += 1
                        elif re.search("MOLECULE", line):
                            names = (line.split(' ')[5]).replace('\n','').split(":")

                            if ligandName != "":
                                ligandsDict[ligandName] = ligandContent
                                self.alignObjects[alignIdx].ligs.append(len(self.ligsObjects) - 1 + len(clusterLigands))
                                alignDict[ligandName] = alignIdx

                            if numGrids != len(self.bsiteObjects):
                                if prevName != "":
                                    self.bsiteObjects.append(bsiteObject(len(self.bsiteObjects), prevName, pdbID, chainID, maxZscores[(len(self.bsiteObjects)+1)], 0, 0,
                                        [i for i in range(len(self.ligsObjects), len(self.ligsObjects)+len(clusterLigands))]))

                                    for i in range(0, len(clusterLigands)):
                                        self.ligsObjects.append(ligsObject(len(self.ligsObjects), clusterLigands[i], len(self.bsiteObjects) - 1, 0, 0, alignDict[clusterLigands[i]]))

                                    cpickle.dump( ligandsDict, open( join(probisliteDir, "data/ligs-"+prevName+".p"), "wb" ) )
                                    ligandsDict = {}
                                    alignDict = {}
                                    clusterLigands = []
                                else:
                                    numGrids = 0

                            if prevName.split('_')[0] != names[6]:

                                alignName = names[6]
                                insertFlag = True

                                for i in range (0, len(self.alignObjects)):
                                    tmpObj = self.alignObjects[i]
                                    if alignName == tmpObj.name:
                                        alignIdx = i
                                        insertFlag = False
                                        break

                                if insertFlag:
                                    item = (len(self.treeBoxA1.get_children()), alignName, zscoresAlignments[alignName])
                                    self.treeBoxA1.insert('', 'end', values=item)

                                    for indx, val in enumerate(item):
                                        ilen = tkFont.Font().measure(val)
                                        if self.treeBoxA1.column(self.columnsA1[indx], width=None) < ilen:
                                            self.treeBoxA1.column(self.columnsA1[indx], width=ilen)

                                    alignIdx = len(self.alignObjects)
                                    self.alignObjects.append(alignObject(len(self.alignObjects), alignName, 0, 0,[], zscoresAlignments[alignName]))

                            prevName = names[6]+"_"+str(numGrids)

                            ligandName = names[0]+"."+names[1]+"."+names[2]+"."+names[3]+"."+names[4]+":"+(names[5].split('.')[0])
                            clusterLigands.append(ligandName)
                            ligandContent = line

                        elif "REMARK" not in line and "CONECT" not in line:
                            ligandContent += line

                    ligandsDict[ligandName] = ligandContent
                    self.alignObjects[alignIdx].ligs.append(len(self.ligsObjects) - 1 + len(clusterLigands))
                    alignDict[ligandName] = alignIdx

                    self.bsiteObjects.append(bsiteObject(len(self.bsiteObjects), prevName, pdbID, chainID, maxZscores[(len(self.bsiteObjects)+1)], 0, 0,
                                                 [i for i in range(len(self.ligsObjects), len(self.ligsObjects)+len(clusterLigands))]))
                    for i in range(0, len(clusterLigands)):
                        self.ligsObjects.append(ligsObject(len(self.ligsObjects), clusterLigands[i], len(self.bsiteObjects) - 1, 0, 0, alignDict[clusterLigands[i]]))

                    cpickle.dump(ligandsDict, open(join(probisliteDir, "data/ligs-" + prevName + ".p"), "wb"))
                    del ligandsDict, alignDict

                    # copy ligands file

                    shutil.copy2(join(self.databaseLocation, "livedb/results/ligands.pdb"), join(probisliteDir, "data/ligs-"+pdbID+chainID+".pdb"))
                    shutil.copy2(join(self.databaseLocation, "livedb/results/zscores.pdb"), join(probisliteDir, "data/zscores.txt"))

                    #add grids
                    lines = 0
                    with open (join(self.databaseLocation, "livedb/results/grid-tmp.pdb"), "r") as oldGrid:
                        with open (join(self.databaseLocation, "livedb/results/grid.pdb"), "w") as newGrid:
                            for line in oldGrid:
                                lines += 1
                                if "ATOM" in line:
                                    if lines % 3 == 0:
                                        newGrid.write(line)
                                else:
                                    newGrid.write(line)

                    with open(join(self.databaseLocation, "livedb/results/grid.pdb"), "r") as myfile:
                        grids = myfile.read().split("ENDMDL")

                    shutil.copy2(join(self.databaseLocation, "livedb/results/grid.pdb"), join(probisliteDir, "data/grids-"+pdbID+chainID+".pdb"))


                    for i in range(0, len(grids)-1):
                        with open(join(probisliteDir, "data/grid-"+self.bsiteObjects[i].name+".pdb"), "w") as tmpfile:
                            tmpfile.write(grids[i] + "ENDMDL")

                        item = (self.bsiteObjects[i].id, self.bsiteObjects[i].name, self.bsiteObjects[i].score)
                        self.treeBox1.insert('', 'end', values=item)

                        for indx, val in enumerate(item):
                            ilen = tkFont.Font().measure(val)
                            if self.treeBox1.column(self.columns1[indx], width=None) < ilen:
                                self.treeBox1.column(self.columns1[indx], width=ilen)

                    # add ligands
                    with open(join(self.databaseLocation, "livedb/results/ligands.pdb"), "r") as myfile:
                        ligands = myfile.read().split("CLUSTER")
                    for i in range(1, len(ligands)):
                        with open(join(probisliteDir, "data/ligs-"+self.bsiteObjects[i-1].name+".pdb"), "w") as tmpfile:
                            tmpfile.write(ligands[i])

                    # copy alignment file
                    shutil.copy2(join(self.databaseLocation, "livedb/results/super.pdb"), join(probisliteDir, "data/super-"+pdbID+chainID+".pdb"))


                    ### PLIP=specific code
                    chimera.runCommand('background solid white')
                    chimera.runCommand('color gray')


                except Exception:
                    pass
                    print "not all data found"


                self.sortby(self.treeBoxA1, self.columnsA1[2], 1)
                self.parent.update()
                self.progressBarProbis.stop()
                tkMessageBox.showinfo("ProBiS", "Done with calculation of binding sites (" + pdbID + ":" + chainID + ").")
                print "Done with ProBiS (" + pdbID + ":" + chainID + ")"
                self.customProteinFrameStatus.set("probisInput")
                self.createCustomFrame()
                self.note.select(self.outputTab)

        else:
            try:
                self.parent.after(200, lambda: self.runProbis(pdbID, chainID))
            except:
                print "pass error"
                pass

    def cancelProbis(self):
        result = tkMessageBox.askquestion("Cancel", "Calculation of binding sites will be terminated.\n \nAre you sure?", icon='warning', parent = self)
        if result == 'yes':
            self.probisProcess.kill()
            print "ProBiS job termination."
            self.customProteinFrameStatus.set("probisInput")
            self.createCustomFrame()
            self.queue.put(-1)


    def exitButton(self):
        #exit(1) #dev
        #alert if exit while probis is still working
        if hasattr(self, 'probisProcess'):
            if self.probisProcess.poll() is None:
                result = tkMessageBox.askquestion("ProBiS", "ProBiS is still working. Calculation of binding sites will be terminated.\n \nExit anyway?", icon='warning', parent = self)
                if result == 'yes':
                    self.probisProcess.kill()
                    self.parent.destroy()
                    self.queue.put(-1)
                    print "ProBiS: normal program termination"
            else:
                print "ProBiS: normal program termination"
                self.resetWorkspace(False)
                self.parent.destroy()

        #alert if exit while still dl
        elif hasattr(self, 'dlInProgress'):
            if self.dlInProgress == 1:
                result = tkMessageBox.askquestion("ProBiS", "ProBiS database is still downloading. \n \nExit anyway?", icon='warning', parent = self)
                if result == 'yes':
                    if hasattr(self, 'running'):
                        if self.running == 1:
                            self.running = 0
                    self.resetWorkspace(False)
                    self.parent.destroy()
                    print "ProBiS: normal program termination"
            elif self.dlInProgress == 2:
                result = tkMessageBox.askquestion("ProBiS", "ProBiS database is still in extraction process. \n \nExit anyway?", icon='warning', parent = self)
                if result == 'yes':
                    if hasattr(self, 'running'):
                        if self.running == 1:
                            self.running = 0
                    self.parent.destroy()
                    self.resetWorkspace(False)
                    print "ProBiS: normal program termination"
            else:
                print "ProBiS: normal program termination"
                self.resetWorkspace(False)
                self.parent.destroy()

        else:
            print "ProBiS: normal program termination"
            self.resetWorkspace(False)
            self.parent.destroy()


    #set view for grids
    def showFancyGrid(self, name, id):

        from chimera import SubprocessMonitor

        try:
            SubprocessMonitor.Popen([chimera.openModels.open(join(probisliteDir, "data/grid-"+name+".pdb"), identifyAs="bsite-"+str(id)),
                                     chimera.runCommand("~show #"+str([objekt.id for objekt in chimera.openModels.list() if objekt.name == 'bsite-'+str(id)][0])),
                                     chimera.runCommand("vdw #"+str([objekt.id for objekt in chimera.openModels.list() if objekt.name == 'bsite-'+str(id)][0])),
                                     chimera.runCommand("color "+colors[id]+" #"+str([objekt.id for objekt in chimera.openModels.list() if objekt.name == 'bsite-'+str(id)][0])) ],
                                            daemon = True)
        except Exception, e:
            #print e
            pass # 'list' object has no attribute 'rfind' ???


    #get data for pdb protein
    def pdbProtein(self):
        if self.entryPdbID.get() == "" or self.entryChainID.get() == "":
            tkMessageBox.showinfo( "ProBiS error!", "Input for PDB ID and chain ID is required.", parent=self)
        elif len(self.entryPdbID.get()) != 4:
            tkMessageBox.showinfo( "ProBiS error!", "Wrong PDB ID.", parent=self)
        elif not self.internetOn():
            tkMessageBox.showinfo("Communication Error", "No Internet connection available. Please make sure that your device is connected to the Internet.")
        elif self.resetWorkspace():
            pdbID = self.entryPdbID.get().lower()
            chainID = self.entryChainID.get()
            self.entryPdbID.delete(0,END)
            self.entryChainID.delete(0,END)
            self.getProbisData(pdbID, chainID)


    # check chain id
    def checkChainId(self, chainId, proteinFile):
        ret = False
        with open(proteinFile) as pdb:
            for line in pdb:
                if "ATOM" in line and len(line.split()) > 10:
                    if line.split()[4] == chainId:
                        ret = True
                        break

        return ret

    # get data for custom protein
    def customProtein(self):

        chainID = self.entryChainId.get()
        pdbID = self.entryProteinLocation.get()
        statusValues = self.checkCustomInput()

        if pdbID == "":
            tkMessageBox.showinfo( "ProBiS error!", "Please set path to protein location.", parent=self)
        elif chainID == "":
            tkMessageBox.showinfo( "ProBiS error!", "Please set chain ID.", parent=self)
        elif statusValues[1] == 0:
            tkMessageBox.showinfo( "ProBiS error!", "Input for " + str(statusValues[0]) + " needs to be a number.", parent=self)
        elif statusValues[1] == 2:
            tkMessageBox.showinfo( "ProBiS error!", "Input for " + str(statusValues[0]) + " needs to be a POSITIVE number.", parent=self)
        elif statusValues[1] == 3:
            tkMessageBox.showinfo( "ProBiS error!", "Wrong input for " + str(statusValues[0]) + ". Entered value is not allowed.", parent=self)

        elif os.path.isfile(pdbID) == False:
            tkMessageBox.showinfo( "ProBiS error!", "Protein file "+pdbID+" does not exists.", parent=self)
            self.entryProteinLocation.delete(0, END)
            self.entryChainId.delete(0, END)
        elif self.checkChainId(chainID, pdbID) == False:
            tkMessageBox.showinfo( "ProBiS error!", "Wrong protein chain: "+chainID+".", parent=self)
            self.entryChainId.delete(0, END)
        elif self.resetWorkspace():
            if os.path.isfile(join(self.databaseLocation, "livedb/results/grid-tmp.pdb")):
                os.remove(join(self.databaseLocation, "livedb/results/grid-tmp.pdb"))
            if os.path.isfile(join(self.databaseLocation, "livedb/results/centro.txt")):
                os.remove(join(self.databaseLocation, "livedb/results/centro.txt"))
            if os.path.isfile(join(self.databaseLocation, "livedb/results/ligands.pdb")):
                os.remove(join(self.databaseLocation, "livedb/results/ligands.pdb"))
            if os.path.isfile(join(self.databaseLocation, "livedb/results/zscores.pdb")):
                os.remove(join(self.databaseLocation, "livedb/results/zscores.pdb"))

            pdbID = pdbID[-8:][:4].lower()
            self.queryProtein = pdbID
            self.queryChain = chainID
            self.queryStringLabel.set("Query Protein: "+self.queryProtein+", Chain: "+self.queryChain)

            sys = platform.system()

            with open(join(self.databaseLocation, "livedb/data/bslib.txt")) as f:
                file_str = f.read()

            if sys == "Windows":
                file_str = file_str.replace("replace", join(self.databaseLocation, "livedb\\data\srf\\rep"))
            else:
                file_str = file_str.replace("replace\\", join(self.databaseLocation, "livedb/data/srf/rep/"))

            with open(join(self.databaseLocation, "livedb/bslib.txt"), "w+") as f:
                f.write(file_str)

            if sys == "Windows":

                if platform.architecture()[0] == "64bit":
                    windowsExe = "64\probislite.exe"
                else:
                    windowsExe = "32\probislite.exe"

                bashCommand = [join(probisliteDir, "bin", windowsExe),
                     "--ncpu", str(multiprocessing.cpu_count() - 1),
                     "--bslib", join(self.databaseLocation, "livedb\\bslib.txt"),
                     "--bio", join(self.databaseLocation, "livedb\\data\\bio" ),
                     "--receptor", os.path.abspath(self.entryProteinLocation.get()),
                     "--receptor_chain_id", chainID,
                     "--gridpdb_hcp", join(self.databaseLocation, "livedb\\results\grid-tmp.pdb"),
                     "--centro_out", join(self.databaseLocation, "livedb\\results\centro.txt"),
                     "--lig_clus_file", join(self.databaseLocation, "livedb\\results\ligands.pdb"),
                     "--z_scores_file", join(self.databaseLocation, "livedb\\results\zscores.pdb"),
                     "--nosql", join(self.databaseLocation, "livedb\\results\probis.nosql"),
                     "--json", join(self.databaseLocation, "livedb\\results\probis.json"),
                     "--jsonwl", join(self.databaseLocation, "livedb\\results\probis_with_ligands.json"),
                     "--superimpose_file", join(self.databaseLocation, "livedb\\results\super.pdb"),
                     "--interatomic", str(float(self.entryInter.get())),
                     "--excluded", str(float(self.entryExcluded.get())),
                     "--grid", str(float(self.entryGrid.get())),
                     "--num_bsites", str(int(float(self.entryNumSites.get()))),
                     "--cutoff", str(int(float(self.entryCutoff.get()))),
                     "--centro_clus_rad", str(float(self.entryCenRadius.get())),
                     "--ligandRMSD_min_z_score", str(float(self.entryRmsdScore.get())),
                     "--probis_min_z_score", str(float(self.entryZscore.get())),
                     "--probis_clus_rad", str(float(self.entryClRadius.get())),
                     "--probis_min_pts", str(int(float(self.entryPts.get()))),
                     "--srf_dir", join(self.databaseLocation, "livedb\\data\\srf\\rep")]

            else:
                bashCommand = [join(probisliteDir, "bin/probislite"),
                    "--ncpu", str(multiprocessing.cpu_count()),
                    "--bslib", join(self.databaseLocation, "livedb/bslib.txt"),
                    "--bio", join(self.databaseLocation, "livedb/data/bio" ),
                    "--srf_dir", join(self.databaseLocation, "livedb/data/srf/rep" ),
                    "--receptor", self.entryProteinLocation.get(),
                    "--receptor_chain_id", chainID,
                    "--gridpdb_hcp", join(self.databaseLocation, "livedb/results/grid-tmp.pdb"),
                    "--centro_out", join(self.databaseLocation, "livedb/results/centro.txt"),
                    "--lig_clus_file", join(self.databaseLocation, "livedb/results/ligands.pdb"),
                    "--z_scores_file", join(self.databaseLocation, "livedb/results/zscores.pdb"),
                    "--nosql", join(self.databaseLocation, "livedb/results/probis.nosql"),
                    "--json", join(self.databaseLocation, "livedb/results/probis.json"),
                    "--jsonwl", join(self.databaseLocation, "livedb/results/probis_with_ligands.json"),
                    "--interatomic", str(float(self.entryInter.get())),
                    "--excluded", str(float(self.entryExcluded.get())),
                    "--grid", str(float(self.entryGrid.get())),
                    "--num_bsites", str(int(float(self.entryNumSites.get()))),
                    "--cutoff", str(int(float(self.entryCutoff.get()))),
                    "--centro_clus_rad", str(float(self.entryCenRadius.get())),
                    "--ligandRMSD_min_z_score", str(float(self.entryRmsdScore.get())),
                    "--probis_min_z_score", str(float(self.entryZscore.get())),
                    "--probis_clus_rad", str(float(self.entryClRadius.get())),
                    "--probis_min_pts", str(int(float(self.entryPts.get()))),
                    "--superimpose_file", join(self.databaseLocation, "livedb/results/super.pdb")]

            # print bashCommand
            # make results dir if it doesnt exists
            if not os.path.exists(join(self.databaseLocation, "livedb/results")):
                os.makedirs(join(self.databaseLocation, "livedb/results"))

            self.proteinLocation = self.entryProteinLocation.get()
            self.customProteinFrameStatus.set("probisWorking")
            self.createCustomFrame()
            self.queue.queue.clear()
            self.runProbis(pdbID, chainID)
            self.t = RunInThread(self, bashCommand, "probis", pdbID, chainID, self.queue)
            self.t.daemon = True
            self.t.start()


    # check if input is correct
    def checkCustomInput(self):

        values = {"ProBiS Z-score": self.entryZscore,
                  "Cluster Radius": self.entryClRadius,
                  "Centroid Radius": self.entryCenRadius,
                  "Interatomic": self.entryInter,
                  "Excluded Radius": self.entryExcluded,
                  "Grid Spacing": self.entryGrid,
                  "ligandRMSD Z-score": self.entryRmsdScore,
                  "Number of Sites": self.entryNumSites,
                  "Number of Points": self.entryPts,
                  "Cutoff Length": self.entryCutoff }

        defaultVals = {"ProBiS Z-score": "2.5",
                  "Cluster Radius": "3.0",
                  "Centroid Radius": "3.0",
                  "Interatomic": "0.8",
                  "Excluded Radius": "0.8",
                  "Grid Spacing": "0.5",
                  "Number of Sites": "3",
                  "ligandRMSD Z-score": "0.5",
                  "Number of Points": "10",
                  "Cutoff Length": "6" }


        for key, value in values.iteritems():
            try:
                val = float(value.get())
                if "Number" in key:
                    val = int(val)
                if "Cutoff" in key:
                    val = int(val)
                    if val not in [4, 5,6,7,8,9.10,11,12,13,14.15]:
                        return (key, 3)

                if val <= 0:
                    value.delete(0, END)
                    return (key, 2)
            except ValueError:
                value.delete(0, END)
                value.insert(0, defaultVals[key])
                return (key, 0)

        return ("ok", 1)


    def databaseOption(self):
        value = self.databaseRadioVal.get()

        if value is 0:
            tkMessageBox.showinfo("ProBiS error!", "Please select one of the database options.", parent=self)
        else:
            self.customProteinFrameStatus.set("databaseInput2")
            self.createCustomFrame()


    def downloadOrCheck(self):
        value = self.databaseRadioVal.get()
        if value is 1:
            self.downloadDatabase()
        else:
            self.checkDatabasePath()

    def backDlFrame(self):
        self.databaseRadioVal.set(0)
        self.customProteinFrameStatus.set("databaseInput")
        self.createCustomFrame()

    # download probis database, needed for custom protein binding sites calculation
    def downloadDatabase(self):

        installPath = self.entryDatabaselocation.get()

        if installPath == "":
            tkMessageBox.showinfo("ProBiS error!", "Please set path to database location.", parent=self)
        elif not self.internetOn():
            tkMessageBox.showinfo("Communication Error", "No Internet connection available. Please make sure that your device is connected to the Internet.")
        else:
            self.databaseLocation = os.path.normpath(installPath)
            self.running = 1
            self.customProteinFrameStatus.set("databaseWorking")
            self.createCustomFrame()
            self.poll()
            self.t = RunInThread(self, "", "download", "", "", self.queue)
            self.t.daemon = True
            self.t.start()


    def open_url(self, e):
        try:
            webbrowser.open_new(r"http://insilab.org/probis-plugin")
        except:
            print "Error: Could not open the website."

    # update program version
    def updateProgramVersion(self, newVersion):
        #shutil.rmtree(probisliteDir)
        versionFile = os.path.join(os.environ["probis-home"], ".probis", "version.txt")
        with open(versionFile, 'w') as myfile:
                myfile.write("UPDATE NOW")
        tkMessageBox.showinfo("Update", "Please restart probis.")
        self.parent.destroy()

    # search for location of probis database
    def searchPathDir(self):
        dirname = askdirectory(parent=self, initialdir=os.environ["probis-home"])
        self.entryDatabaselocation.delete(0, END)
        self.entryDatabaselocation.insert(0, dirname)


    # search for location
    def searchPathFile(self, status):
        # custom protein file
        if status == 0:
            filename = askopenfilename(parent=self,filetypes=[("Pdb files","*.pdb")], initialdir=os.environ["probis-home"])
            self.entryProteinLocation.delete(0, END)
            self.entryProteinLocation.insert(0, filename)

        # load project file
        elif status == 1:
            filename = askopenfilename(parent=self,filetypes=[("ProBiS files","*.probis")], initialdir=os.environ["probis-home"])
            self.entryProjectFileLocation.delete(0, END)
            self.entryProjectFileLocation.insert(0, filename)

        # export project file
        elif status == 2:
            filename = asksaveasfilename(parent=self,filetypes=[("ProBiS files","*.probis")], initialdir=os.environ["probis-home"])
            self.entryExportProjectFile.delete(0, END)
            if filename and ".probis" not in filename:
                filename += ".probis"
            self.entryExportProjectFile.insert(0, filename)

        # export molecules to file
        elif status == 3:
            filename = asksaveasfilename(parent=self.window,filetypes=[("Pdb files","*.pdb")], initialdir=os.environ["probis-home"])
            self.entryExportMolecules.delete(0, END)
            if filename and ".pdb" not in filename:
                filename += ".pdb"
            self.entryExportMolecules.insert(0, filename)


    #
    # check if probis database is in already installed
    #
    def databaseAlreadyInstalled(self):

        if os.path.isfile(databaseFile):
            with open (databaseFile, "r+") as myfile:
                self.databaseLocation = myfile.read()
                self.databaseLocation = os.path.abspath(self.databaseLocation)
            if os.path.isdir(join(self.databaseLocation, "livedb")):
                self.customProteinFrameStatus.set("probisInput")
            else:
                self.customProteinFrameStatus.set("databaseInput")
        else:
            self.customProteinFrameStatus.set("databaseInput")
        self.createCustomFrame()


    #
    # check if probis database is in the right place
    #
    def checkDatabasePath(self):
        self.databaseLocation = os.path.abspath(self.entryDatabaselocation.get())
        if self.databaseLocation.endswith('/'):
            self.databaseLocation = self.databaseLocation[:-1]

        if os.path.isdir(join(self.databaseLocation, "livedb")):
            with open(databaseFile, "w+") as myfile:
                myfile.truncate()
                myfile.write(self.databaseLocation)
            self.customProteinFrameStatus.set("probisInput")
            self.createCustomFrame()
        else:
            tkMessageBox.showinfo( "ProBiS error!", "There is no ProBiS database at "+self.databaseLocation+". Please set correct path or install ProBiS database.", parent=self)



    #
    # check internet connection
    #
    def internetOn(self):
        try:
            urllib2.urlopen('http://www.google.com',timeout=1)
            return True
        except:
            pass
        return False


    #
    #get probis data from our server
    #
    def getProbisData(self, pdbID, chainID):
        print pdbID + chainID
        self.queryProtein = pdbID
        self.queryChain = chainID
        self.queryStringLabel.set("Query Protein: "+self.queryProtein+", Chain: "+self.queryChain)

        try:
            url = 'http://insilab.org/api/predb?pdbid='+pdbID+'&chain='+chainID
            response = urllib2.urlopen(url)
            data = json.load(response)

            if (data["grid"] == False or data["ligands"] == False or data["zscores"] == False):
                tkMessageBox.showinfo('ProBiS Results','Sorry, we could not predict binding sites for ' + pdbID +', ' + chainID + ' chain. Make sure that entered protein and chain ID are correct.', parent=self)
                print 'Sorry, we could not predict binding sites for' + pdbID +', ' + chainID + ' chain.'

            else:
                #load protein to pymol
                chimera.openModels.open(pdbID, type="PDB")
                for objekt in chimera.openModels.list():
                    if objekt.name == pdbID:
                        chimera.runCommand("setattr m name "+pdbID+chainID+" #"+str(objekt.id))
                        chimera.runCommand("center #" + str(objekt.id))
                        chimera.runCommand("~show #" + str(objekt.id))
                        chimera.runCommand("~ribbon #" + str(objekt.id))
                        l = chainID.split()
                        for i in range(0, len(l)):
                            #### does this works with later versions ???
                            chimera.runCommand("ribbon #" + str(objekt.id) + ":." + str(l[i]))
                        break

                # add ligands
                with open(join(probisliteDir, "data/ligs-"+pdbID+chainID+".pdb"), "w") as tmpfile:
                    tmpfile.write(data["ligands"])

                # save ligands names to list [(bsite name, [ligands]), (bsite name2), [ligands]...)
                numGrids = 0
                clusterLigands = []
                prevName = ""
                ligandName = ""
                ligandContent = ""
                ligandsDict = {}
                alignDict = {}
                alignIdx = 0
                zscoresAlignments = {}
                maxZscores = {}
                self.bsiteObjects = []
                self.ligsObjects = []
                self.alignObjects = []

                # zscores
                zscores = data["zscores"].splitlines()

                for i in range(0, len(zscores)):
                    if len(zscores[i]) > 0:
                        zscore = zscores[i].split(' ')
                        aCluster = int(float(zscore[0]))
                        aName = zscore[1].split(":")[5]
                        aScore = float(zscore[2])
                        zscoresAlignments[aName] = aScore
                        if aCluster in maxZscores:
                            if maxZscores[aCluster] < aScore:
                                maxZscores[aCluster] = aScore
                        else:
                            maxZscores[aCluster] = aScore

                # add zscore to file
                with open(join(probisliteDir, "data/zscores.txt"), "w") as tmpfile:
                    tmpfile.write(data["zscores"])
                del zscores

                for line in open(join(probisliteDir, "data/ligs-"+pdbID+chainID+".pdb"), 'r'):
                    if re.search("CLUSTER", line):
                        numGrids += 1
                    elif re.search("MOLECULE", line):
                        names = (line.split(' ')[5]).replace('\n','').split(":")

                        if ligandName != "":
                            ligandsDict[ligandName] = ligandContent
                            self.alignObjects[alignIdx].ligs.append(len(self.ligsObjects) - 1 + len(clusterLigands))
                            alignDict[ligandName] = alignIdx

                        if numGrids != len(self.bsiteObjects):
                            if prevName != "":

                                self.bsiteObjects.append(bsiteObject(len(self.bsiteObjects), prevName, pdbID, chainID, maxZscores[(len(self.bsiteObjects)+1)], 0, 0,
                                                                     [i for i in range(len(self.ligsObjects), len(self.ligsObjects)+len(clusterLigands))]))

                                for i in range(0, len(clusterLigands)):
                                    self.ligsObjects.append(ligsObject(len(self.ligsObjects), clusterLigands[i], len(self.bsiteObjects)-1, 0, 0, alignDict[clusterLigands[i]]))

                                cpickle.dump( ligandsDict, open( join(probisliteDir, "data/ligs-"+prevName+".p"), "wb" ) )
                                ligandsDict = {}
                                alignDict = {}
                                clusterLigands = []
                            else:
                                numGrids = 0

                        # alignment
                        if prevName.split('_')[0] != names[6]:

                            alignName = names[6]
                            insertFlag = True

                            for i in range (0, len(self.alignObjects)):
                                tmpObj = self.alignObjects[i]
                                if alignName == tmpObj.name:
                                    alignIdx = i
                                    insertFlag = False
                                    break

                            if insertFlag:
                                item = (len(self.treeBoxA1.get_children()), names[6], zscoresAlignments[alignName])
                                self.treeBoxA1.insert('', 'end', values=item)

                                for indx, val in enumerate(item):
                                    ilen = tkFont.Font().measure(val)
                                    if self.treeBoxA1.column(self.columnsA1[indx], width=None) < ilen:
                                        self.treeBoxA1.column(self.columnsA1[indx], width=ilen)

                                alignIdx = len(self.alignObjects)
                                self.alignObjects.append(alignObject(len(self.alignObjects), alignName, 0, 0,[], zscoresAlignments[alignName]))

                        prevName = names[6]+"_"+str(numGrids)


                        ligandName = names[0]+"."+names[1]+"."+names[2]+"."+names[3]+"."+names[4]+":"+(names[5].split('.')[0])
                        clusterLigands.append(ligandName)
                        ligandContent = line


                    elif "REMARK" not in line and "CONECT" not in line:
                        ligandContent += line

                self.bsiteObjects.append(bsiteObject(len(self.bsiteObjects), prevName, pdbID, chainID, maxZscores[(len(self.bsiteObjects)+1)], 0, 0,
                                                     [i for i in range(len(self.ligsObjects), len(self.ligsObjects)+len(clusterLigands))]))

                ligandsDict[ligandName] = ligandContent
                self.alignObjects[alignIdx].ligs.append(len(self.ligsObjects) - 1 + len(clusterLigands))
                alignDict[ligandName] = alignIdx

                for i in range(0, len(clusterLigands)):
                    self.ligsObjects.append(ligsObject(len(self.ligsObjects), clusterLigands[i], len(self.bsiteObjects) - 1, 0, 0,alignDict[clusterLigands[i]]))

                cpickle.dump(ligandsDict, open(join(probisliteDir, "data/ligs-" + prevName + ".p"), "wb"))
                del alignDict, ligandsDict

                #os.remove(join(probisliteDir, "data/ligs-"+pdbID+chainID+".pdb"))


                grids = data["grid"].split("ENDMDL")
                # add grids
                with open(join(probisliteDir, "data/grids-"+pdbID+chainID+".pdb"), "w") as tmpfile:
                    tmpfile.write(data["grid"])

                #add grids to bsite view first listbox

                for i in range(0, len(grids)-1):
                    with open(join(probisliteDir, "data/grid-"+self.bsiteObjects[i].name+".pdb"), "w") as tmpfile:
                        tmpfile.write(grids[i]+"ENDMDL")

                    item = (self.bsiteObjects[i].id, self.bsiteObjects[i].name, self.bsiteObjects[i].score)
                    self.treeBox1.insert('', 'end', values=item)

                    for indx, val in enumerate(item):
                        ilen = tkFont.Font().measure(val)
                        if self.treeBox1.column(self.columns1[indx], width=None) < ilen:
                            self.treeBox1.column(self.columns1[indx], width=ilen)

                # write alignments to file
                with open(join(probisliteDir, "data/super-"+pdbID+chainID+".pdb"), "w") as tmpfile:
                    tmpfile.write(data["super"])


                self.parent.update()
                self.note.select(self.outputTab)

                self.sortby(self.treeBoxA1, self.columnsA1[2], 1)

                ### LigandRMSD-specific code
                #self.updateReferenceLigand()

                ### PLIP=specific code
                chimera.runCommand('background solid white')
                chimera.runCommand('color gray')

        except Exception, e:
            print "Unexpected error:", e




    #
    # load project from file
    #

    def loadProjectFromFile(self):

        projectFile = self.entryProjectFileLocation.get()
        if projectFile == "":
            tkMessageBox.showerror("Export Project", "You need to select file location.")
            return

        if len(self.treeBox1.get_children()) != 0:
            if self.resetWorkspace() == False:
                return

        if os.path.isfile(projectFile):
            flag = 0
            proteinId = ""
            proteinChain = ""
            content = ""

            numGrids = 0
            clusterLigands = []
            prevName = ""
            ligandContent = ""
            ligandsDict = {}
            alignDict = {}
            ligandName = ""
            alignIdx = 0
            maxZscores = {}
            zscoresAlignments = {}

            fieldnames = ('AA', 'dbSNP ID', 'PDB', 'chain', 'SNP', 'species')
            self.snpsObjects = {}

            self.bsiteObjects = []
            self.ligsObjects = []
            self.alignObjects = []

            for line in open(projectFile, "r"):
                if re.search("SPLIT_POINT", line):
                    if flag == 2:
                        with open(join(probisliteDir, "data/zscores.txt"), "w") as myfile:
                            zscores =content.splitlines()

                            for i in range(0, len(zscores)):
                                if len(zscores[i]) > 0:
                                    zscore = zscores[i].split(' ')
                                    aCluster = int(float(zscore[0]))
                                    aName = zscore[1].split(":")[5]
                                    aScore = float(zscore[2])
                                    zscoresAlignments[aName] = aScore
                                    if aCluster in maxZscores:
                                        if maxZscores[aCluster] < aScore:
                                            maxZscores[aCluster] = aScore
                                    else:
                                        maxZscores[aCluster] = aScore

                            del zscores

                            myfile.write(content)

                    elif flag == 3:

                        ligandsDict[ligandName] = ligandContent
                        self.alignObjects[alignIdx].ligs.append(len(self.ligsObjects) - 1 + len(clusterLigands))
                        alignDict[ligandName] = alignIdx
                        cpickle.dump( ligandsDict, open( join(probisliteDir, "data/ligs-"+prevName+".p"), "wb" ) )

                        with open(join(probisliteDir, "data/ligs-"+proteinId+proteinChain+".pdb"), "w") as myfile:
                            self.bsiteObjects.append(bsiteObject(len(self.bsiteObjects), prevName, proteinId, proteinChain, maxZscores[(len(self.bsiteObjects)+1)], 0, 0,
                                                     [i for i in range(len(self.ligsObjects), len(self.ligsObjects)+len(clusterLigands))]))
                            for i in range(0, len(clusterLigands)):
                                self.ligsObjects.append(ligsObject(len(self.ligsObjects), clusterLigands[i], len(self.bsiteObjects) - 1, 0, 0, alignDict[clusterLigands[i]]))

                            #ligands = content.split("CLUSTER")
                            #for i in range(1, len(ligands)):
                            #    with open(join(probisliteDir, "data/ligs-"+self.bsiteObjects[i-1].name+".pdb"), "a") as tmpfile:
                            #        tmpfile.write(ligands[i])
                            myfile.write(content)

                        del alignDict, ligandsDict


                    elif flag == 4:
                        with open(join(probisliteDir, "data/grids-"+proteinId+proteinChain+".pdb"), "w") as myfile:
                            myfile.write(content)
                        grids = content.split("ENDMDL")
                        with open(join(probisliteDir, "data/zscores.txt"), "w") as myfile:
                            for i in range(0, len(grids)-1):
                                with open(join(probisliteDir, "data/grid-"+self.bsiteObjects[i].name+".pdb"), "w") as tmpfile:
                                    tmpfile.write(grids[i]+"ENDMDL")

                                item = (self.bsiteObjects[i].id, self.bsiteObjects[i].name, self.bsiteObjects[i].score)
                                self.treeBox1.insert('', 'end', values=item)

                                for indx, val in enumerate(item):
                                    ilen = tkFont.Font().measure(val)
                                    if self.treeBox1.column(self.columns1[indx], width=None) < ilen:
                                        self.treeBox1.column(self.columns1[indx], width=ilen)

                                myfile.write(proteinId+proteinChain+"_"+str(self.bsiteObjects[i].id)+" "+str(self.bsiteObjects[i].score)+"\n")


                    elif flag == 5:
                        with open(join(probisliteDir, "data/super-"+proteinId+proteinChain+".pdb"), "w") as myfile:
                            myfile.write(content)


                    content = ""
                    flag = flag + 1
                else:
                    if flag == 1:
                        proteinId = line[:4]
                        proteinChain = line[4:5]

                    elif flag == 3:
                        if re.search("CLUSTER", line):
                            numGrids += 1
                        elif re.search("MOLECULE", line):
                            names = (line.split(' ')[5]).replace('\n','').split(":")

                            if ligandName != "":
                                ligandsDict[ligandName] = ligandContent
                                self.alignObjects[alignIdx].ligs.append(len(self.ligsObjects) - 1 + len(clusterLigands))
                                alignDict[ligandName] = alignIdx

                            if numGrids != len(self.bsiteObjects):
                                if prevName != "":
                                    self.bsiteObjects.append(bsiteObject(len(self.bsiteObjects), prevName, proteinId, proteinChain, maxZscores[(len(self.bsiteObjects)+1)], 0, 0,
                                                                     [i for i in range(len(self.ligsObjects), len(self.ligsObjects)+len(clusterLigands))]))
                                    for i in range(0, len(clusterLigands)):
                                        self.ligsObjects.append(ligsObject(len(self.ligsObjects), clusterLigands[i], len(self.bsiteObjects) - 1, 0, 0, alignDict[clusterLigands[i]]))

                                    cpickle.dump( ligandsDict, open( join(probisliteDir, "data/ligs-"+prevName+".p"), "wb" ) )
                                    ligandsDict = {}
                                    alignDict = {}
                                    clusterLigands = []
                                else:
                                    numGrids = 0

                            if prevName.split('_')[0] != names[6]:

                                alignName = names[6]
                                insertFlag = True

                                for i in range (0, len(self.alignObjects)):
                                    tmpObj = self.alignObjects[i]
                                    if alignName == tmpObj.name:
                                        alignIdx = i
                                        insertFlag = False
                                        break

                                if insertFlag:
                                    item = (len(self.treeBoxA1.get_children()), alignName, zscoresAlignments[alignName])
                                    self.treeBoxA1.insert('', 'end', values=item)

                                    for indx, val in enumerate(item):
                                        ilen = tkFont.Font().measure(val)
                                        if self.treeBoxA1.column(self.columnsA1[indx], width=None) < ilen:
                                            self.treeBoxA1.column(self.columnsA1[indx], width=ilen)

                                    alignIdx = len(self.alignObjects)
                                    self.alignObjects.append(alignObject(len(self.alignObjects), alignName, 0, 0,[], zscoresAlignments[alignName]))


                            prevName = names[6]+"_"+str(numGrids)

                            ligandName = names[0]+"."+names[1]+"."+names[2]+"."+names[3]+"."+names[4]+":"+(names[5].split('.')[0])
                            clusterLigands.append(ligandName)
                            ligandContent = line
                        else:
                            ligandContent += line

                    content = content + line

            self.sortby(self.treeBoxA1, self.columnsA1[2], 1)
            #load protein to pymol
            chimera.openModels.open(proteinId, type="PDB")
            for objekt in chimera.openModels.list():
                if objekt.name == proteinId:
                    chimera.runCommand("setattr m name "+proteinId+proteinChain+" #"+str(objekt.id))
                    chimera.runCommand("center #" + str(objekt.id))
                    chimera.runCommand("~show #" + str(objekt.id))
                    chimera.runCommand("~ribbon #" + str(objekt.id))
                    l = proteinChain.split()
                    for i in range(0, len(l)):
                        chimera.runCommand("ribbon #" + str(objekt.id) + ":." + str(l[i]))
                    break

            self.parent.update()
            self.note.select(self.outputTab)
            self.entryProjectFileLocation.delete(0, END)

            ### PLIP=specific code
            #chimera.runCommand('background solid white')
            chimera.runCommand('color gray')

            self.queryProtein = proteinId
            self.queryChain = proteinChain
            self.queryStringLabel.set("Query Protein: "+self.queryProtein+", Chain: "+self.queryChain)

        else:
            print "Load Error: "+projectFile+" doesn't exist"
            tkMessageBox.showerror("Load Error", projectFile+" doesn't exist")
            self.entryProjectFileLocation.delete(0, END)



    #
    # export projcet to file
    #

    def exportProjectToFile(self):
        projectFile = self.entryExportProjectFile.get()
        pdbIdChain = ""

        if projectFile == "":
            tkMessageBox.showerror("Export Project", "You need to select file location.")
            self.entryExportProjectFile.focus()

        else:
            for i in os.listdir(join(probisliteDir, "data")):
                if os.path.isfile(join(probisliteDir, "data/"+i)) and 'super' in i:
                    pdbIdChain = i[6:-4]
                    break
            with open(projectFile, "w") as myFile:

                myFile.write("SPLIT_POINT\n")
                myFile.write(pdbIdChain+"\n")

                myFile.write("SPLIT_POINT\n")
                with open(join(probisliteDir, "data/zscores.txt"), 'r') as scoreFile:
                    myFile.write(scoreFile.read())

                myFile.write("SPLIT_POINT\n")
                with open(join(probisliteDir, "data/ligs-"+pdbIdChain+".pdb"), 'r') as ligsFile:
                    myFile.write(ligsFile.read())

                myFile.write("SPLIT_POINT\n")
                with open(join(probisliteDir, "data/grids-"+pdbIdChain+".pdb"), 'r') as gridFile:
                    for line in gridFile:
                        myFile.write(line)
                myFile.write("TEST\n")

                myFile.write("SPLIT_POINT\n")
                with open(join(probisliteDir, "data/super-"+pdbIdChain+".pdb"), 'r') as superFile:
                    for line in superFile:
                        myFile.write(line)
                myFile.write("SPLIT_POINT\n")
                myFile.write("SPLIT_POINT\n")


            tkMessageBox.showinfo("Export Project", "Project was exported successfully.")
            self.entryExportProjectFile.delete(0, END)
            print "file exported"


    #
    # show corresponding elements in tab and pymol when clicked on specific row
    #

    def multiListBox1Select(self, ar):

        #try:

        selFrame = self.noteOutput.tab(self.noteOutput.select(), "text")
        self.removeMoleculeFromPymol(self.ligrmsdreferencename)

        array = []
        for item in self.treeBox1.selection():
            array.append((int)(self.treeBox1.item(item, "values")[0]))

        for i in range(0, len(self.bsiteObjects)):
            tmp = self.bsiteObjects[i]

            if tmp.sel == 0 and tmp.id in array:
                tmp.sel = 1
                for j in tmp.ligs:

                    if self.refLigands.get(self.ligsObjects[j].bsite) != None and self.refLigands.get(
                            self.ligsObjects[j].bsite) == self.ligsObjects[j].id:
                        item = (str(self.ligsObjects[j].id) + "*", self.ligsObjects[j].name, self.ligsObjects[j].rmsd)
                        self.treeBox2.insert('', 'end', values=item)

                        for indx, val in enumerate(item):
                            ilen = tkFont.Font().measure(val)
                            if self.treeBox2.column(self.columns2[indx], width=None) < ilen:
                                self.treeBox2.column(self.columns2[indx], width=ilen)

                        self.loadMoleculeToPymol("ligs-" + tmp.name + ".p", self.ligsObjects[j].name,
                                                 self.ligrmsdreferencename)
                    else:
                        item = (self.ligsObjects[j].id, self.ligsObjects[j].name, self.ligsObjects[j].rmsd)
                        self.treeBox2.insert('', 'end', values=item)

                        for indx, val in enumerate(item):
                            ilen = tkFont.Font().measure(val)
                            if self.treeBox2.column(self.columns2[indx], width=None) < ilen:
                                self.treeBox2.column(self.columns2[indx], width=ilen)

                    if self.ligsObjects[j].selA == 1:
                        self.treeBox2.selection_add(self.treeBox2.get_children()[len(self.treeBox2.get_children()) - 1])

                if tmp.selA == 0:
                    self.showFancyGrid(tmp.name, tmp.id)
                    #from chimera import SubprocessMonitor as SM
                    #SM.chimera.openModels.open(join(probisliteDir, "data/grid-"+tmp.name+".pdb"), identifyAs="bsite-"+str(tmp.id))
                    #chimera.SubprocessMonitor.Popen([])
                    #chm = ChimeraSubProcess(tmp.name, tmp.id)

                    #bsiteThread2 = FuncThread(self.showFancyGrid, tmp.name, tmp.id)
                    #bsiteThread2.daemon = True
                    #bsiteThread2.start()

                if selFrame == "BSite View":
                    for item in self.treeBoxA3.get_children():
                        if tmp.id == (int)(self.treeBoxA3.item(item, "values")[0]):
                            self.treeBoxA3.selection_add(item)


            elif tmp.sel == 1 and tmp.id not in array:
                tmp.sel = 0
                for j in tmp.ligs:
                    for item in self.treeBox2.get_children():
                        if (self.treeBox2.item(item, "values")[0]).replace("*", "") == str(j):
                            if item in self.treeBox2.selection():
                                self.treeBox2.selection_remove(item)
                            self.treeBox2.delete(item)
                            break

                if tmp.selA == 0:
                    self.removeMoleculeFromPymol("bsite-"+str(tmp.id))

                if selFrame == "BSite View":
                    for item in self.treeBoxA3.get_children():
                        if tmp.id == (int)(self.treeBoxA3.item(item, "values")[0]):
                            self.treeBoxA3.selection_remove(item)

            #vis = PyMOLVisualizer(None)
            #vis.set_initial_representations()

        if selFrame ==  "BSite View":
            indexes = self.getIdsForRMSD("bsite")

            if len(indexes) > 0:
                if self.runningRmsd != 0:
                    print "Added LigandRMSD job"
                    self.rmsdJobs = ("bsite", indexes)

                else:
                    self.statusRMSDLabel.set("Calculating LigandRMSDs...")
                    self.runningRmsd = 1
                    self.pollRmsd()
                    threadRmsd1 = RmsdThread(self, "bsite", indexes)
                    threadRmsd1.daemon = True
                    threadRmsd1.start()


        # error from listbox on reset workspace; there is problem with empty listbox and execution of command function
        #except Exception:
        #    pass


    def multiListBox2Select(self, ar):

        selFrame = self.noteOutput.tab(self.noteOutput.select(), "text")

        array = []
        ligandBoxIdx = {}
        self.setSelection = {}

        for item in self.treeBox2.selection():
            id = (int)(self.treeBox2.item(item, "values")[0].replace("*", ""))
            array.append(id)
            ligandBoxIdx[id] = item

        for i in range(0, len(self.ligsObjects)):
            tmp = self.ligsObjects[i]

            if tmp.sel == 0 and tmp.id in array:
                tmp.sel = 1
                flag = False
                for item3 in self.treeBox3.get_children():
                    if self.treeBox3.item(item3, "values")[0] == str(tmp.align):
                        flag = True
                if not flag:
                    line = (tmp.align, self.alignObjects[tmp.align].name,  self.alignObjects[tmp.align].zscore)
                    self.treeBox3.insert('', 'end', values=line)

                    for indx, val in enumerate(line):
                        ilen = tkFont.Font().measure(val)
                        if self.treeBox3.column(self.columns3[indx], width=None) < ilen:
                            self.treeBox3.column(self.columns3[indx], width=ilen)

                    if self.alignObjects[tmp.align].selA == 1:
                        self.treeBox3.selection_add(self.treeBox3.get_children()[len(self.treeBox3.get_children()) - 1])

                if tmp.selA == 0:
                    ligandFile = "ligs-"+self.bsiteObjects[tmp.bsite].name+".p"
                    self.loadMoleculeToPymol(ligandFile, tmp.name, "lig-"+str(tmp.id))
                    #color_carbons(random.choice(ligandcolors), "lig-"+str(tmp.id))

                if selFrame == "BSite View":
                    for item in self.treeBoxA2.get_children():
                        if (int)(self.treeBoxA2.item(item, "values")[0].replace("*", "")) == tmp.id:
                            self.treeBoxA2.selection_add(item)


                self.removeMoleculeFromPymol("PLIP-"+str(tmp.id))
                #    cmd.delete('PLIP*')  # Delete all PLIP groups
                #    cmd.hide('spheres')  # Hide water molecules as well
                #    bsite = self.bsiteObjects[tmp.bsite]
                #    cmd.hide('sticks', '%s%s' % (bsite.protein, bsite.chain))  # Hide interacting residues


            elif tmp.sel == 1 and tmp.id not in array:
                tmp.sel = 0
                flag = False
                for elt in self.ligsObjects:
                    if elt.sel == 1 and elt.align == tmp.align:
                        flag = True
                        break

                if not flag:
                    for item in self.treeBox3.get_children():
                        if str(tmp.align) == self.treeBox3.item(item, "values")[0]:
                            if item in self.treeBox3.selection():
                                self.treeBox3.selection_remove(item)
                            self.treeBox3.delete(item)
                            break

                if tmp.selA == 0:
                    self.removeMoleculeFromPymol("lig-"+str(tmp.id))

                if selFrame == "BSite View":
                    for item in self.treeBoxA2.get_children():
                        if (int)(self.treeBoxA2.item(item, "values")[0].replace("*", "")) == tmp.id:
                            self.treeBoxA2.selection_remove(item)

                self.removeMoleculeFromPymol("PLIP-"+str(tmp.id))
                ### PLIP-specific code
                #cmd.delete('PLIP-%i' % tmp.id)
                #cmd.delete('Residues-%i' % tmp.id)
                #bsite = self.bsiteObjects[tmp.bsite]
                #cmd.hide('sticks', '%s%s' % (bsite.protein, bsite.chain))  # Hide interacting residues



    def multiListBox3Select(self, ar):

        selFrame = self.noteOutput.tab(self.noteOutput.select(), "text")

        array = []
        for item in self.treeBox3.selection():
            array.append((int)(self.treeBox3.item(item, "values")[0]))

        for i in range(0, len(self.alignObjects)):
            tmp = self.alignObjects[i]

            if tmp.sel == 0 and tmp.id in array:
                tmp.sel = 1

                if tmp.selA == 0:
                    alignmentFile = "super-"+self.bsiteObjects[0].protein+self.bsiteObjects[0].chain+".pdb"
                    self.loadMoleculeToPymol(alignmentFile, tmp.name,"align-"+str(tmp.id))

                if selFrame == "BSite View":
                    for item in self.treeBoxA1.get_children():
                        if (int)(self.treeBoxA1.item(item, "values")[0].replace("*", "")) == tmp.id:
                            self.treeBoxA1.selection_add(item)

            elif tmp.sel == 1 and tmp.id not in array:
                tmp.sel = 0

                if tmp.selA == 0:
                    self.removeMoleculeFromPymol("align-"+str(tmp.id))

                if selFrame == "BSite View":
                    for item in self.treeBoxA1.get_children():
                        if (int)(self.treeBoxA1.item(item, "values")[0].replace("*", "")) == tmp.id:
                            self.treeBoxA1.selection_remove(item)




    def multiListBoxA1Select(self, ar):

        selFrame = self.noteOutput.tab(self.noteOutput.select(), "text")
        self.removeMoleculeFromPymol(self.ligrmsdreferencename)

        array = []
        for item in self.treeBoxA1.selection():
            array.append((int)(self.treeBoxA1.item(item, "values")[0].replace("*", "")))

        for i in range(0, len(self.alignObjects)):
            tmp = self.alignObjects[i]

            if tmp.selA == 0 and tmp.id in array:
                tmp.selA = 1
                for j in tmp.ligs:

                    if self.refLigands.get(self.ligsObjects[j].bsite) != None and self.refLigands.get(
                            self.ligsObjects[j].bsite) == self.ligsObjects[j].id:
                        line = (str(self.ligsObjects[j].id) + "*", self.ligsObjects[j].name, self.ligsObjects[j].rmsd)
                        self.treeBoxA2.insert('', 'end', values=line)

                        for indx, val in enumerate(line):
                            ilen = tkFont.Font().measure(val)
                            if self.treeBoxA2.column(self.columnsA2[indx], width=None) < ilen:
                                self.treeBoxA2.column(self.columnsA2[indx], width=ilen)

                        self.loadMoleculeToPymol("ligs-" + self.bsiteObjects[self.ligsObjects[j].bsite].name + ".p",
                                                 self.ligsObjects[j].name, self.ligrmsdreferencename)
                    else:
                        line = (self.ligsObjects[j].id, self.ligsObjects[j].name, self.ligsObjects[j].rmsd)
                        self.treeBoxA2.insert('', 'end', values=line)

                        for indx, val in enumerate(line):
                            ilen = tkFont.Font().measure(val)
                            if self.treeBoxA2.column(self.columnsA2[indx], width=None) < ilen:
                                self.treeBoxA2.column(self.columnsA2[indx], width=ilen)

                    if self.ligsObjects[j].sel == 1:
                        self.treeBoxA2.selection_add(self.treeBoxA2.get_children()[len(self.treeBoxA2.get_children()) - 1])


                if tmp.sel == 0:
                    alignmentFile = "super-"+self.bsiteObjects[0].protein+self.bsiteObjects[0].chain+".pdb"
                    self.loadMoleculeToPymol(alignmentFile, tmp.name,"align-"+str(tmp.id))

                if selFrame == "Alignment View":
                    for item in self.treeBox3.get_children():
                        if (int)(self.treeBox3.item(item, "values")[0].replace("*", "")) == tmp.id:
                            self.treeBox3.selection_add(item)

            elif tmp.selA == 1 and tmp.id not in array:
                tmp.selA = 0
                for k in tmp.ligs:
                    for item in self.treeBoxA2.get_children():
                        if self.treeBoxA2.item(item, "values")[0].replace("*", "") == str(k):
                            if item in self.treeBoxA2.selection():
                                self.treeBoxA2.selection_remove(item)
                            self.treeBoxA2.delete(item)
                            break

                if tmp.sel == 0:
                    self.removeMoleculeFromPymol("align-"+str(tmp.id))

                if selFrame == "Alignment View":
                    for item in self.treeBox3.get_children():
                        if (int)(self.treeBox3.item(item, "values")[0]) == tmp.id:
                            self.treeBox3.selection_add(item)


        if selFrame == "Alignment View":
            indexes = self.getIdsForRMSD("align")

            if len(indexes) > 0:
                if self.runningRmsd != 0:
                    print "Added LigandRMSD job"
                    self.rmsdJobs = ("align", indexes)

                else:
                    self.statusRMSDLabel.set("Calculating LigandRMSDs...")
                    self.runningRmsd = 1
                    self.pollRmsd()
                    rmsdThread2 = RmsdThread(self, "align", indexes)
                    rmsdThread2.daemon = True
                    rmsdThread2.start()






    def multiListBoxA2Select(self, ar):

        selFrame = self.noteOutput.tab(self.noteOutput.select(), "text")


        array = []
        ligandBoxIdx = {}
        self.setSelection = {}

        for item in self.treeBoxA2.selection():
            id = (int)(self.treeBoxA2.item(item, "values")[0].replace("*", ""))
            array.append(id)
            ligandBoxIdx[id] = item

        for i in range(0, len(self.ligsObjects)):
            tmp = self.ligsObjects[i]

            if tmp.selA == 0 and tmp.id in array:
                tmp.selA = 1
                flag = False
                for item3 in self.treeBoxA3.get_children():
                    if (self.treeBoxA3.item(item3, "values")[0]).replace("*", "") == str(tmp.bsite):
                        flag = True
                if not flag:
                    grid = self.bsiteObjects[tmp.bsite]
                    line = (grid.id, grid.name, grid.score)

                    self.treeBoxA3.insert('', 'end', values=line)

                    for indx, val in enumerate(line):
                        ilen = tkFont.Font().measure(val)
                        if self.treeBoxA3.column(self.columnsA3[indx], width=None) < ilen:
                            self.treeBoxA3.column(self.columnsA3[indx], width=ilen)

                    if grid.sel == 1:
                        self.treeBoxA3.selection_add(
                            self.treeBoxA3.get_children()[len(self.treeBoxA3.get_children()) - 1])

                if tmp.sel == 0:
                    ligandFile = "ligs-"+self.bsiteObjects[tmp.bsite].name+".p"
                    self.loadMoleculeToPymol(ligandFile, tmp.name, "lig-"+str(tmp.id))

                if selFrame == "Alignment View":
                    for item in self.treeBox2.get_children():
                        if (int)(self.treeBox2.item(item, "values")[0].replace("*", "")) == tmp.id:
                            self.treeBox2.selection_set(item)

                self.removeMoleculeFromPymol("PLIP-"+str(tmp.id))

            elif tmp.selA == 1 and tmp.id not in array:
                tmp.selA = 0
                flag = False
                for elt in self.ligsObjects:
                    if elt.selA == 1 and elt.bsite == tmp.bsite:
                        flag = True
                        break

                if not flag:
                    for item in self.treeBoxA3.get_children():
                        print self.treeBoxA3.item(item, "values")[0].replace("*", "")
                        if self.treeBoxA3.item(item, "values")[0].replace("*", "") == str(tmp.bsite):
                            if item in self.treeBoxA3.selection():
                                self.treeBoxA3.selection_remove(item)
                            self.treeBoxA3.delete(item)
                            break

                if tmp.sel == 0:
                    self.removeMoleculeFromPymol("lig-"+str(tmp.id))

                if selFrame == "Alignment View":
                    for item in self.treeBox2.get_children():
                        if tmp.id == (int)(self.treeBox2.item(item, "values")[0].replace("*", "")):
                            self.treeBox2.selection_remove(item)

                ### PLIP-specific code
                self.removeMoleculeFromPymol("PLIP-"+str(tmp.id))



    def multiListBoxA3Select(self, ar):

        selFrame = self.noteOutput.tab(self.noteOutput.select(), "text")


        array = []
        for item in self.treeBoxA3.selection():
            array.append((int)(self.treeBoxA3.item(item, "values")[0]))

        for i in range(0, len(self.bsiteObjects)):
            tmp = self.bsiteObjects[i]

            if tmp.selA == 0 and tmp.id in array:
                tmp.selA = 1

                if tmp.sel == 0:
                    self.showFancyGrid(tmp.name, tmp.id)

                if selFrame == "Alignment View":
                    for item in self.treeBox1.get_children():
                        if tmp.id == (int)(self.treeBox1.item(item, "values")[0]):
                            self.treeBox1.selection_add(item)

            elif tmp.selA == 1 and tmp.id not in array:
                tmp.selA = 0

                if tmp.sel == 0:
                    self.removeMoleculeFromPymol("bsite-"+str(tmp.id))

                if selFrame == "Alignment View":
                    for item in self.treeBox1.get_children():
                        if tmp.id == (int)(self.treeBox1.item(item, "values")[0]):
                            self.treeBox1.selection_remove(item)


    #
    # detect keyboard events
    #
    def keyBoardEvent(self, event):

        # toggle plip enable / disable
        if len(event.char) == 1 and event.keysym == 'p' and event.char == '\x10':
            self.togglePLIPObjects()

        if len(event.char) == 1 and event.keysym == 'r': #and event.char == '\x10':
            if self.noteOutput.tab(self.noteOutput.select(), "text") == "BSite View":
                self.resetRMSDbsite()
            else:
                self.resetRMSDalign()



    def togglePLIPObjects(self, e = None):

        ligandIdx = -1

        if self.noteOutput.tab(self.noteOutput.select(), "text") == "BSite View":
            if len(self.treeBox2.selection()) == 1:
                selectedIdx = self.treeBox2.selection()[0]
                ligandIdx = (int)(self.treeBox2.item(selectedIdx, "values")[0].replace("*", ""))

        elif len(self.treeBoxA2.selection()) == 1:
            selectedIdx = self.treeBoxA2.selection()[0]
            ligandIdx = (int)(self.treeBoxA2.item(selectedIdx, "values")[0].replace("*", ""))

        if ligandIdx != -1:
            tmp = self.ligsObjects[ligandIdx]

            plipObject = [object for object in chimera.openModels.list() if "PLIP" in object.name]

            showFlag = True

            for objekt in plipObject:
                if "PLIP-"+str(tmp.id) == objekt.name:
                    showFlag = False
                chimera.runCommand("close #" + str(objekt.id))
                chimera.runCommand('modeldisplay #0')

            if showFlag:
                self.plipFunction(tmp)



    #
    # reset rmsd
    #

    def resetRMSDbsite(self, e = None):

        if len(self.treeBox2.selection()) == 1:

            selectedIdx = self.treeBox2.selection()[0]
            selectedLigand = (int)(self.treeBox2.item(selectedIdx, "values")[0].replace("*", ""))
            self.updateReferenceLigand(selectedLigand)

            i = 0
            for item in self.treeBox2.get_children():

                ligandIdx = (int)(self.treeBox2.item(item, "values")[0].replace("*", ""))
                self.ligsObjects[ligandIdx].rmsd = ""

                self.treeBox2.delete(item)

                if ligandIdx == selectedLigand:
                    line = (str(ligandIdx) + "*", self.ligsObjects[ligandIdx].name, self.ligsObjects[ligandIdx].rmsd)
                    self.treeBox2.insert('', i, values=line)
                    self.treeBox2.selection_add(self.treeBox2.get_children()[i])
                else:
                    line = (ligandIdx, self.ligsObjects[ligandIdx].name, self.ligsObjects[ligandIdx].rmsd)
                    self.treeBox2.insert('', i, values=line)

                for indx, val in enumerate(line):
                    ilen = tkFont.Font().measure(val)
                    if self.treeBox2.column(self.columns2[indx], width=None) < ilen:
                        self.treeBox2.column(self.columns2[indx], width=ilen)

                i += 1

            indexes = self.getIdsForRMSD("bsite")

            if len(indexes) > 0:
                if self.runningRmsd != 0:
                    print "Added LigandRMSD job"
                    self.rmsdJobs = ("bsite", indexes)

                else:
                    self.statusRMSDLabel.set("Calculating LigandRMSDs...")
                    self.runningRmsd = 1
                    self.pollRmsd()
                    threadRmsd3 = RmsdThread(self, "bsite", indexes)
                    threadRmsd3.daemon = True
                    threadRmsd3.start()


    def resetRMSDalign(self, e = None):

        if len(self.treeBoxA2.selection()) == 1:

            selectedIdx = self.treeBoxA2.selection()[0]
            selectedLigand = (int)(self.treeBoxA2.item(selectedIdx, "values")[0].replace("*", ""))
            self.updateReferenceLigand(selectedLigand)

            i = 0
            for item in self.treeBoxA2.get_children():

                ligandIdx = (int)(self.treeBoxA2.item(item, "values")[0].replace("*", ""))
                self.ligsObjects[ligandIdx].rmsd = ""
                self.treeBoxA2.delete(item)

                if ligandIdx == selectedLigand:
                    line = (str(ligandIdx) + "*", self.ligsObjects[ligandIdx].name, self.ligsObjects[ligandIdx].rmsd)
                    self.treeBoxA2.insert('', i, values=line)
                    self.treeBoxA2.selection_add(self.treeBoxA2.get_children()[i])
                else:
                    line = (ligandIdx, self.ligsObjects[ligandIdx].name, self.ligsObjects[ligandIdx].rmsd)
                    self.treeBoxA2.insert('', i, values=line)

                for indx, val in enumerate(line):
                    ilen = tkFont.Font().measure(val)
                    if self.treeBoxA2.column(self.columnsA2[indx], width=None) < ilen:
                        self.treeBoxA2.column(self.columnsA2[indx], width=ilen)

                i += 1

            indexes = self.getIdsForRMSD("align")
            if len(indexes) > 0:
                if self.runningRmsd != 0:
                    print "Added LigandRMSD job"
                    self.rmsdJobs = ("align", indexes)

                else:
                    self.statusRMSDLabel.set("Calculating LigandRMSDs...")
                    self.runningRmsd = 1
                    self.pollRmsd()
                    rmsdThread4 = RmsdThread(self, "align", indexes)
                    rmsdThread4.daemon = True
                    rmsdThread4.start()


    def getIdsForRMSD(self, view):

        indexes = []
        first = True

        try:
            if view == "bsite":
                for item in self.treeBox2.get_children():
                    ligandIdx = (int)(self.treeBox2.item(item, "values")[0].replace("*", ""))
                    # default reference ligand is the first ligand in list box
                    if first and self.initBview.get(self.ligsObjects[ligandIdx].bsite) is None:
                        self.updateReferenceLigand(ligandIdx)
                        self.initBview[self.ligsObjects[ligandIdx].bsite] = True

                    if self.ligsObjects[ligandIdx].rmsd == "":
                        indexes.append( ligandIdx)

                    first = False

            else:
                for item in self.treeBoxA2.get_children():
                    ligandIdx = (int)(self.treeBoxA2.item(item, "values")[0].replace("*", ""))

                    if first and self.initAview.get(self.ligsObjects[ligandIdx].bsite) is None:
                        self.updateReferenceLigand(ligandIdx)
                        self.initAview[self.ligsObjects[ligandIdx].bsite] = True

                    if self.ligsObjects[ligandIdx].rmsd == "":
                        indexes.append(ligandIdx)

                    first = False

        # if exit it tries to get one more indexes from listbox which doesnt exist anymore
        except Exception, e:
            pass


        return indexes



    #
    # run ligand rmsd calculations when binding site is selected
    #
    def afterLigandRMSD(self, view, indexes, rmsds):

        if view == "bsite":

            #when closing plugin there is some problem with listbox indexes
            try:

                if len(indexes) > 0:
                    for i in range(0, len(indexes)):
                        ligandIdx = indexes[i]
                        self.ligsObjects[ligandIdx].rmsd = "%.2f" % rmsds[i]

                    selectedItems = self.treeBox2.selection()

                    i = 0
                    for item in self.treeBox2.get_children():
                        ligandIdx = (int)(self.treeBox2.item(item, "values")[0].replace("*", ""))

                        if self.ligsObjects[ligandIdx].rmsd != self.treeBox2.item(item, "values")[2]:
                            self.treeBox2.delete(item)

                            if self.refLigands.get(self.ligsObjects[ligandIdx].bsite) != None and self.refLigands.get(
                                    self.ligsObjects[ligandIdx].bsite) == ligandIdx:
                                line = (str(ligandIdx) + "*", self.ligsObjects[ligandIdx].name,
                                        self.ligsObjects[ligandIdx].rmsd)

                            else:
                                line = (ligandIdx, self.ligsObjects[ligandIdx].name, self.ligsObjects[ligandIdx].rmsd)

                            self.treeBox2.insert('', i, values=line)
                            # adjust columns lenghts if necessary
                            for indx, val in enumerate(line):
                                ilen = tkFont.Font().measure(val)
                                if self.treeBox2.column(self.columns2[indx], width=None) < ilen:
                                    self.treeBox2.column(self.columns2[indx], width=ilen)

                            if item in selectedItems:
                                self.treeBox2.selection_add(self.treeBox2.get_children()[i])

                            i += 1

            except Exception, e:
                print "Error while computing LigandRMSD. Parser error?"
                #from kendrew.toolchain.errors import getTraceback
                #print getTraceback()
                #raise e

            self.runningRmsd = 0
            self.statusRMSDLabel.set("")


        elif view == "align":

            try:

                #multiquerycode
                if len(indexes) > 0:

                    for i in range(0, len(indexes)):
                        ligandIdx = indexes[i]
                        self.ligsObjects[ligandIdx].rmsd = "%.2f" % rmsds[i]

                    selectedItems = self.treeBoxA2.selection()

                    i = 0
                    for item in self.treeBoxA2.get_children():
                        ligandIdx = (int)(self.treeBoxA2.item(item, "values")[0].replace("*", ""))

                        if self.ligsObjects[ligandIdx].rmsd != self.treeBoxA2.item(item, "values")[2]:
                            self.treeBoxA2.delete(item)

                            if self.refLigands.get(self.ligsObjects[ligandIdx].bsite) != None and self.refLigands.get(
                                    self.ligsObjects[ligandIdx].bsite) == ligandIdx:
                                line = (str(ligandIdx) + "*", self.ligsObjects[ligandIdx].name,
                                        self.ligsObjects[ligandIdx].rmsd)
                            else:
                                line = (ligandIdx, self.ligsObjects[ligandIdx].name, self.ligsObjects[ligandIdx].rmsd)

                            self.treeBoxA2.insert('', i, values=line)
                            # adjust columns lenghts if necessary
                            for indx, val in enumerate(line):
                                ilen = tkFont.Font().measure(val)
                                if self.treeBoxA2.column(self.columnsA2[indx], width=None) < ilen:
                                    self.treeBoxA2.column(self.columnsA2[indx], width=ilen)

                            if item in selectedItems:
                                self.treeBoxA2.selection_add(self.treeBoxA2.get_children()[i])

                            i += 1

            except:
                print "Error while computing LigandRMSD. Parser error?"



        if self.rmsdJobs:
            view = self.rmsdJobs[0]
            indexes = self.rmsdJobs[1]
            self.rmsdJobs = ()
            self.runningRmsd = 1
            self.pollRmsd()
            threadRmsd1 = RmsdThread(self, view, indexes)
            threadRmsd1.daemon = True
            threadRmsd1.start()

        else:
            self.runningRmsd = 0
            self.statusRMSDLabel.set("")


    #
    # PLIP specific code
    #
    def plipFunction(self, tmp):

        ligids = tmp.name.split('.')
        uid = ':'.join([ligids[0], ligids[2], ligids[1]])
        bsite = self.bsiteObjects[tmp.bsite]
        lig_obj_name = "lig-"+str(tmp.id)  # Name of ligand object
        prot_obj_name = ''.join([bsite.protein, bsite.chain])  # Name of protein object

        if tmp.id in self.plipvisualizations:
            plipvis = self.plipvisualizations[tmp.id]
            plipvis.update_class()
        else:
            plipvis = PLIPVisualization(prot_obj_name, lig_obj_name, tmp.id, uid)
            self.plipvisualizations[tmp.id] = plipvis

        plipvis.show_interactions()


    def removeMoleculeFromPymol(self, moleculeId):
        for objekt in chimera.openModels.list():
                if objekt.name == moleculeId:
                    chimera.runCommand("close #" + str(objekt.id))
                    if "PLIP" in moleculeId:
                        chimera.runCommand('modeldisplay #0')
                    break


    def loadMoleculeToPymol(self, readFile, readId, pymolId):
        flag = False
        moleculeContent = ""

        if "ligs-" in readFile:
            ligs = cpickle.load( open(join(probisliteDir, "data/"+readFile), "rb" ))
            moleculeContent = ligs[readId]

        else:
            for line in open(join(probisliteDir, "data/"+readFile), "r"):
                if re.search(readId, line):
                    flag = True
                if flag:
                    moleculeContent += line
                    if re.search("ENDMDL", line):
                        break
        if pymolId == "export":
            return moleculeContent
        else:
            with open(join(probisliteDir, "data/temp.pdb"), 'w') as myfile:
                myfile.write(moleculeContent)
            chimera.openModels.open(join(probisliteDir, "data/temp.pdb"), identifyAs=pymolId)



    def loadMoleculesForLigandRMSD(self, indexes):

        readMoleculesPDB = {}

        read = collections.defaultdict(list)
        for i in range (0, len(indexes)):
            bsite = self.bsiteObjects[self.ligsObjects[indexes[i]].bsite].name
            read[bsite].append(i)

        for bsite, arrayIndexes in read.iteritems():

            ligs = cpickle.load( open(join(probisliteDir, "data/ligs-"+bsite+".p"), "rb" ))
            for arrayIdx in arrayIndexes:
                readMoleculesPDB[arrayIdx] = ligs[self.ligsObjects[indexes[arrayIdx]].name]

            del ligs

        indexes = sorted(readMoleculesPDB)
        return [readMoleculesPDB[key] for key in indexes]



    #
    # prepare file to be exported
    #
    def saveSelectedObjects(self):
        self.window = Toplevel(self, width=250, height=200)
        self.window.title("Save Molecules")

        frame = Frame(self.window)
        frame.pack()

        Label(frame, text="Which object or selection would you like to save?", anchor=CENTER).pack(pady=(20,5), padx=(10,10), fill=BOTH, expand=1)

        self.columnsExport = ('Name', 'Pymol Name')
        self.treeBoxExport = Treeview(frame, columns=self.columnsExport, show="headings", selectmode="extended")
        vsbEx = Scrollbar(frame, orient="vertical", command=self.treeBoxExport.yview)
        vsbEx.pack(side=RIGHT, fill=Y)
        self.treeBoxExport.configure(yscrollcommand=vsbEx.set)
        self.treeBoxExport.pack(side=LEFT, fill=BOTH, expand=1, padx=20)


        self.checkSelBsite = IntVar(self)
        self.checkSelLigands = IntVar(self)
        self.checkSelAlignment = IntVar(self)

        self.checkSelBsite.set(1)
        self.checkSelLigands.set(1)
        self.checkSelAlignment.set(1)


        #add objects to listbox
        self.applyFilterSelection(True)


        underFrame = Frame(self.window)
        underFrame.pack()


        Label(underFrame, text="Filter:").grid(row=0, column=0, pady=(10,0))
        Checkbutton(underFrame, text="alignment", variable=self.checkSelAlignment, command=self.applyFilterSelection).grid(row=0, column=1, pady=(10,0), padx=(10,0))
        Checkbutton(underFrame, text="ligands", variable=self.checkSelLigands, command=self.applyFilterSelection).grid(row=0, column=2, pady=(10,0), padx=(10,0))
        Checkbutton(underFrame, text="binding sites", variable=self.checkSelBsite, command=self.applyFilterSelection).grid(row=0, column=3, pady=(10,0), padx=(10,50))

        saveToFrame = Frame(self.window)
        saveToFrame.pack()

        #self.checkSaveFile = IntVar(self)

        #Label(saveToFrame, text="Save to:").grid(row=0, column=0, sticky=W)
        #Radiobutton(saveToFrame, text="one file",variable=self.checkSaveFile, value=0).grid(row=0, column=1, pady=(10,10), padx=(10,0))
        #Radiobutton(saveToFrame, text="multiple files", variable=self.checkSaveFile, value=1).grid(row=0, column=2, pady=(10,10), padx=(10,120))
        self.entryExportMolecules = Entry(saveToFrame, width=32)
        self.entryExportMolecules.grid(row=1, column=0, padx=(5,5), pady=(20,10))

        Button(saveToFrame, text='Save As', command=lambda: self.searchPathFile(3)).grid(row=1, column=1, padx=(0,5), pady=(18,10), sticky=W)

        exportBox = LabelFrame(self.window,borderwidth=2)
        exportBox.pack(fill=X)

        Button(exportBox, text='Cancel', command = self.window.destroy).grid(column=0, row=0,pady=10, padx=(70,40))
        Button(exportBox, text='OK', command=self.exportMoleculesFromPymol).grid(column=1, row=0,pady=10, padx=(40,60))

        #Button(exportMoleculesBox, text='Save Molecule', command=self.saveSelectedObjects).grid(column=0, row=0,pady=10, padx=(20,0))


        # move window to center
        self.window.update_idletasks()
        w = self.window.winfo_screenwidth()
        h = self.window.winfo_screenheight()
        size = tuple(int(_) for _ in self.window.geometry().split('+')[0].split('x'))
        x = w/2 - size[0]/2
        y = h/2 - size[1]
        self.window.geometry("%dx%d+%d+%d" % (size + (x, y)))
        self.window.update_idletasks()


    #
    # export selected molecules from pymol
    #

    def exportMoleculesFromPymol(self):
        print "exporting molecules"
        exportFile = self.entryExportMolecules.get()
        if exportFile == "":
            tkMessageBox.showerror("Error Export", "Please enter path to export file.")
        else:
            for item in self.treeBoxExport.selection():
                for objekt in chimera.openModels.list():
                    if objekt.name == self.treeBoxExport.item(item, "values")[1]:
                        tmpFile = join(probisliteDir, "data/tmp" + str(item) + ".pdb")
                        chimera.runCommand("write #" + str(objekt.id)+" "+tmpFile)
                        break

            with open(exportFile, 'w') as myfile:
                myfile.write("REMARK ProBiS EXPORT\n")
                for item in self.treeBoxExport.selection():
                    myfile.write("REMARK\n")
                    myfile.write("REMARK " + self.treeBoxExport.item(item, "values")[0] + "\n")
                    myfile.write("REMARK\n")
                    if re.search("lig-", self.treeBoxExport.item(item, "values")[1]):
                        myfile.write("MODEL\t" + str(item) + "\n")
                    with open(join(probisliteDir, "data/tmp" + str(item) + ".pdb"), 'r') as copyFile:
                        myfile.write(copyFile.read())
                    if re.search("lig-", self.treeBoxExport.item(item, "values")[1]):
                        myfile.write("ENDMDL\n")
                    os.remove(join(probisliteDir, "data/tmp" + str(item) + ".pdb"))


            print "chimera save "+exportFile
            self.window.destroy()


    #
    # filer on selected molecules
    #

    def applyFilterSelection(self, flag = False):

        self.treeBoxExport.delete(*self.treeBoxExport.get_children())

        if len(self.bsiteObjects) != 0:
            pdbId = self.bsiteObjects[0].protein
            chainId = self.bsiteObjects[0].chain

            item = ("protein", pdbId + chainId)
            self.treeBoxExport.insert('', 'end', values=item)
            for indx, val in enumerate(item):
                ilen = tkFont.Font().measure(val)
                if self.treeBoxExport.column(self.columnsExport[indx], width=None) < ilen:
                    self.treeBoxExport.column(self.columnsExport[indx], width=ilen)


            statusBsite = self.checkSelBsite.get()
            statusLigands = self.checkSelLigands.get()
            statusAlign = self.checkSelAlignment.get()

            for objekt in chimera.openModels.list():
                item = ""
                if re.search("bsite-", objekt.name) and statusBsite == 1:
                    idx = int(objekt.name.split('-')[1])
                    item = (self.bsiteObjects[idx].name, objekt.name)

                elif re.search("lig-", objekt.name) and statusLigands == 1:
                    idx = int(objekt.name.split('-')[1])
                    item = (self.ligsObjects[idx].name, objekt.name)

                elif re.search("align-", objekt.name) and statusAlign == 1:
                    idx = int(objekt.name.split('-')[1])
                    item = (self.alignObjects[idx].name, objekt.name)

                if item != "":
                    self.treeBoxExport.insert('', 'end', values=item)
                    for indx, val in enumerate(item):
                        ilen = tkFont.Font().measure(val)
                        if self.treeBoxExport.column(self.columnsExport[indx], width=None) < ilen:
                            self.treeBoxExport.column(self.columnsExport[indx], width=ilen)

            # select all at the beginning
            if flag:
                for item in self.treeBoxExport.get_children():
                    self.treeBoxExport.selection_add(item)



    #
    # reset workspace - delete all data when preform probis for new protein
    #

    def resetWorkspace(self, ask=True):

        flag = True
        if len(self.treeBox1.get_children()) != 0:
            if ask:
                result = tkMessageBox.askquestion("Delete Project", "All data for current project will be lost. Continue?", icon='warning')
                if result == "no":
                    flag = False

            if flag:
                # remove from pymol
                for objekt in chimera.openModels.list():
                     if re.search("bsite", objekt.name) or re.search("lig", objekt.name) or re.search("align", objekt.name):
                         self.removeMoleculeFromPymol(objekt.name)


                # remove protein assembly from pymol
                pdbId = self.bsiteObjects[0].protein
                chainId = self.bsiteObjects[0].chain
                self.removeMoleculeFromPymol(pdbId+chainId)

                ## Delete all PLIP visualizations
                #cmd.delete('PLIP*')
                #cmd.delete(self.ligrmsdmatchname)
                #cmd.delete(self.ligrmsdreferencename)
                for md in chimera.openModels.list():
                    if md.name.startswith('PLIP'):
                        self.removeMoleculeFromPymol(md.name)
                self.plipvisualizations = {}
                self.removeMoleculeFromPymol(self.ligrmsdreferencename)


                # set selected objects to null
                self.alignObjects = []
                self.ligsObjects = []
                self.bsiteObjects = []

                # clear all listboxs
                self.treeBox1.delete(*self.treeBox1.get_children())
                self.treeBox2.delete(*self.treeBox2.get_children())
                self.treeBox3.delete(*self.treeBox3.get_children())

                self.treeBoxA1.delete(*self.treeBoxA1.get_children())
                self.treeBoxA2.delete(*self.treeBoxA2.get_children())
                self.treeBoxA3.delete(*self.treeBoxA3.get_children())

                self.queryChain = ""
                self.queryStringLabel.set("")

                self.initAview = {}
                self.initBview = {}
                self.rmsdJobs = ()

                if self.runningRmsd == 1:
                    self.LigandRMSD.killjob()

                # delete files in working directory
                for file in os.listdir(join(probisliteDir, "data")):
                    filePath = os.path.join(probisliteDir, "data/"+file)
                    print "delete " + filePath
                    try:
                        if os.path.isfile(filePath):
                            os.unlink(filePath)
                    except Exception, e:
                        print e

        return flag




def main():

    #create probislite directory
    if not os.path.exists(probisliteDir):
        os.makedirs(probisliteDir)
    if not os.path.exists(join(probisliteDir, "data")):
        os.makedirs(join(probisliteDir, "data"))
    root = Tk()

    if platform.system()=='Linux':
        treectrl=os.path.join(probisliteDir,'treectrl2.4.1-linux')
    else:
        if platform.architecture()[0] == "64bit":
            treectrl=os.path.join(probisliteDir,'treectrl2.4.1')
        else:
            treectrl=os.path.join(probisliteDir,'treectrl2.4.1_32')

    root.tk.eval('lappend auto_path {'+treectrl+'}')

    app = ProbisFrame(root)
    root.protocol('WM_DELETE_WINDOW', app.exitButton)
    root.mainloop()

#main()
