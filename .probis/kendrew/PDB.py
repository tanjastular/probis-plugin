# The MIT License (MIT)
#
# Copyright (c) 2016 Joachim Haupt (TU Dresden / Germany)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import os
from comprot.db import Comprot
from kendrew.toolchain.checktypes import *
from kendrew.toolchain.files import read
from multiprocessing import Process
import resource
import sys
import pybel
from kendrew.toolchain.Types import CountedDict
from kendrew.Molecules import readmol
import pickle


def pdblinesplit(line,verbose=False,trimspace=False):
    """Handles parsing of ATOM records for PDB input"""
    record = line[:6]
    atomnum = line[6:11]
    atomname = line[12:16]
    alternate = line[16]
    resname = line[17:20]
    chain = line[21]
    resnum = line[22:26]
    xcoor = line[30:38]
    ycoor = line[38:46]
    zcoor = line[46:54]
    if verbose: print record,atomnum,atomname,alternate,resname,chain,resnum,xcoor,ycoor,zcoor
    atomnum = int(atomnum)
    resnum = int(resnum)
    if trimspace: atomname = atomname.strip().lstrip()
    return record,atomnum,atomname,alternate,resname,chain,resnum,xcoor,ycoor,zcoor

def pdbsplitbychain(pdbfname,name='0xxx'):
    """Splits a given PDB file by Chain. For each chain, one output file is written to the current wd."""
    lastchain = ''
    seen = set()
    seenatoms = set()
    files = set()
    f = read(pdbfname)
    out = None
    for line in f:
        if len(line)<6 or line[:4] != 'ATOM' and line[:6] != 'HETATM':
            continue
        record,atomnum,atomname,alternate,resname,chain,resnum,xcoor,ycoor,zcoor = pdblinesplit(line)
        if chain != lastchain:
            if chain not in seen:
                fname = '%s_%s.pdb' % (name,chain)
                files.add(fname)
                if out is not None:
                    out.close()
                out = open('%s_%s.pdb' % (name,chain),'w')
            else:
                if out is not None:
                    out.close()
                out = open(fname,'a')
            seen.add(chain)
            lastchain = chain
        if (chain,atomnum) not in seenatoms: #for NMR structures, only keep the first model
            out.write(line)
        seenatoms.add((chain,atomnum))
    if out is not None:
        out.close()
    f.close()
    return files

def hasLigand(pdbfname):
    """Checks whether the given PDB file or list of PDB files contains HETATM entries."""
    success = False
    try:
        f = read(pdbfname)
    except IOError:
        print "  Error: PDB file not found at %s!" % (pdbfname)
    else:
        for line in f:
            if line.startswith('HETATM'):
                success = True
                break
        f.close()
    return success

def hasLigandList(pdbfnames):
    """Checks whether at least one of the given list of PDB files contains HETATM entries."""
    for f in pdbfnames:
        success = hasLigand(f)
        if success:
            break
    return success


def readPDBfile(pdbfname, safe=False):
    """Reads a given PDB file and returns a Pybel Molecule. If requested, do it
    safely to except Open Babel crashes. All bonds are read in as single bonds
    if requested, saving a lot of time at OpenBabel import."""

    #workaround for a bug in OpenBabel
    resource.setrlimit(resource.RLIMIT_STACK, (2**28, -1))  # set stack size to 256MB
    sys.setrecursionlimit(10**5)  # increase Python recoursion limit

    success = True
    exitcode = 0
    # Safe mode not required in OpenBabel 2.3.x as bug has been fixed (tested with largest PDB structure 4pth)
    if safe:  # read the file safely, since it can happen, that babel crashes on large files
        if os.path.exists(pdbfname):
            def f(fname):
                #pybel.readfile('pdb', fname).next()
                readmol('pdb', fname)
            p = Process(target=f, args=(pdbfname,))  # make the file reading a separate process
            p.start()
            p.join()
            exitcode = p.exitcode
            success = exitcode == 0
            del p
        else:
            print "  Error: PDB file not found!"
            success = False
            exitcode = 1
    if success:
        mol = readmol('pdb', pdbfname)  # only read the file iff it was successful before
    else:
        mol = pybel.Molecule(pybel.ob.OBMol())
        print "  Error: Failed to read '%s' with OpenBabel (exit code %d)!" % (pdbfname, exitcode)
#    gc.collect()
    return mol


def getLigands(pdbfname=None, mol=None):
    if pdbfname is not None:
        mol = readPDBfile(pdbfname)
    ligands = []  # all ligands in the PDB file

    for obresidue in pybel.ob.OBResidueIter(mol.OBMol):
        #iterate over all residues
        hetatoms = set([(obatom.GetIdx(), obatom) for obatom in pybel.ob.OBResidueAtomIter(obresidue) if ((obresidue.GetName() != "HOH") and obresidue.IsHetAtom(obatom) and (not obatom.IsHydrogen()))])

        #hetatoms.discard(None) # all het atoms of the current residue as a list of tuples (idx,OBAtom)
        if len(hetatoms) == 0:
            continue
            #mol.OBMol.DeleteResidue(obresidue)
        hetatoms = dict(hetatoms)  # make it a dict with idx as key and OBAtom as value
        lig = pybel.ob.OBMol()  # new ligand mol
        neighbours = dict()
        newidx = dict()
        for obatom in hetatoms.values():
            # iterate over atom objects
            idx = obatom.GetIdx()
            lig.AddAtom(obatom)
            # ids of all neighbours of obatom
            neighbours[idx] = set([neighbour_atom.GetIdx() for neighbour_atom in pybel.ob.OBAtomAtomIter(obatom)]) & set(hetatoms.keys())
            #print len(neighbours[idx])
        # map the old atom idx of obmol to the new idx of lig
        newidx = dict(zip(hetatoms.keys(), [obatom.GetIdx() for obatom in pybel.ob.OBMolAtomIter(lig)]))
        #print lig.AddResidue(obresidue)
        # copy the bonds
        for obatom in hetatoms:
            for neighbour_atom in neighbours[obatom]:
                bond = hetatoms[obatom].GetBond(hetatoms[neighbour_atom])
                lig.AddBond(newidx[obatom],newidx[neighbour_atom],bond.GetBondOrder())
        #print lig.NumBonds()
        lig = pybel.Molecule(lig)
        lig.data.update({'Name': obresidue.GetName(), 'Chain': obresidue.GetChain(), 'ResNr': obresidue.GetNum()})
        lig.title = '-'.join((obresidue.GetName(), obresidue.GetChain(), str(obresidue.GetNum())))
        ligands.append(lig)
        #print 'mol',lig.write('smi'),lig.data
    #print 'ligands:', len(ligands)
    return ligands




def getPDBid(pdbid):
        return pdbid.lower()

def stringtoID(pdbidchain):
        """Converts a PDB ID and Chain as a string and tries to convert it to a valid tuple."""
        i = pdbidchain.split(':')
        if len(i) == 1:
            i = pdbidchain.split('_')
        if len(i) != 2:
            raise NameError("Invalid string '%s' for conversion to a PDB ID/Chain tuple!" % pdbidchain)
        return (getPDBid(i[0]), i[1])

class Ligands(object):

    def __init__(self, use_single_file=False):

        self.ligand_library_file = os.path.expanduser("~/lib/pdbsuppl/all-sdf.sdf.gz")
        self.ligand_library = None
        self.ligand_library_path = os.path.expanduser("~/lib/pdbsuppl/ligand_structures")
        self.use_file = use_single_file #use the files in lib path if False

        if self.use_file: # use the single file representation
            #use the file instead of the file repository
            f = read(self.ligand_library_file)
            self.ligand_library_lines = tuple(f)
            f.close()

            f = read(self.ligand_library_file+'.idx.gz')
            self.ligand_library = pickle.load(f)
            f.close()


        self.hasligstore = CountedDict()
        self.hasligstore.setMaxStoreItems(500)

        self.ligstore = CountedDict()
        self.ligstore.setMaxStoreItems(100)

    def hasLigand(self, pdbfname=None, pdbid=None):
        if pdbfname is not None:
            if pdbfname not in self.hasligstore:
                tmp = hasLigand(pdbfname)
                self.hasligstore[pdbfname] = tmp
                return tmp
            else:
                return self.hasligstore[pdbfname]
        elif self.use_file:
            return pdbid in self.ligand_library
        else:
            pdbid = pdbid.lower()
            if pdbid not in self.hasligstore:
                path = os.path.join(self.ligand_library_path, "%s/%s" % (pdbid[1:3], pdbid) )
                tmpr = os.path.exists(path) and len(os.listdir(path)) > 0
                self.hasligstore[pdbid] = tmpr
                return tmpr
            else:
                return self.hasligstore[pdbid]


    def hasLigandList(self, pdbfnames=None, pdbids=None):
        if pdbfnames is not None:
            for pdbfname in pdbfnames:
                success = self.hasLigand(pdbfname=pdbfname)
                if success:
                    break
            return success
        elif self.use_file:
            for pdbid in pdbids:
                success = self.hasLigand(pdbid=pdbid)
                if success:
                    break
            return success
        else:
            for pdbid in pdbids:
                success = self.hasLigand(pdbid=pdbid)
                if success:
                    break
            return success


    def getLigands(self, mol=None, pdbid=None):
        if mol is not None:
            key = mol
            if key not in self.ligstore:
                ligands = getLigands(mol=mol)
        elif self.use_file:
            key = pdbid
            if key not in self.ligstore:
                ligands = []
                if pdbid in self.ligand_library:
                    tmpligands = self.ligand_library[pdbid] # dict of (hetid, chain, resnr) as key and position in the file as value
                    def _getligs(pos, ligand):
                        hetid,chain,resi,model = ligand
                        i,j = pos
                        ligand = pybel.readstring('mol', ''.join(self.ligand_library_lines[i:j]))
                        ligand.data.update({'Name':hetid,'Chain':chain,'ResNr':resi, 'Model':model})
                        ligand.title = '-'.join( (hetid,chain,resi,model) )
                        return ligand
                    ligands = [_getligs(pos, ligand) for ligand,pos in tmpligands.items()]
        else:
            key = pdbid
            if key not in self.ligstore:
                path = os.path.join(self.ligand_library_path, "%s/%s" % (pdbid[1:3], pdbid) )
                if os.path.exists(path) and len(os.listdir(path)) > 0:
                    ligfiles = os.listdir(path)
                    ligands = [readmol('mol', os.path.join(path,file)) for file in ligfiles]
                    for ligand in ligands:
                        pdbid, hetid, model, chain, resi = ligand.title.split('_')[:5]
                        ligand.data.update({'Name':hetid,'Chain':chain,'ResNr':resi})
                        ligand.title = '-'.join( (hetid,chain,resi) )
                else:
                    ligands = []

        if key not in self.ligstore:
            self.ligstore[key] = ligands
            return ligands
        else:
            return self.ligstore[key]



class longIDsingle(tuple):
    """This is a data structure to store one PDB ID - PDB Chain combination as a standardized tuple. The ID 1ABC and Chain A would be represented as `(1abc,A)'."""
    def __new__(cls, *args):
        lid = ()
        #store the given ID and try to be tolerant
        #check for correct argument count
        if len(args) == 0:
            return tuple.__new__(cls)
        elif len(args) > 1:
            raise TypeError("This function takes max. 2 agument (%d given)" % (len(args)+1) )
        else:
            lid = args[0]
        #check types and convert the id
        if type(lid) is str:
            lid = stringtoID(lid)
        else:
            forcelisttuple(lid)
            if len(lid) != 2:
                raise TypeError("Wrong length of '%s' for conversion to a PDB ID/Chain tuple!" % str(lid))
            else:
                lid = (getPDBid(lid[0]) ,lid[1])
        return tuple.__new__(cls, lid)

    def __init__(self, *ids):
        super(longIDsingle,self).__init__(self)


class longID(longIDsingle):
    """This is a data structure to store several PDB ID - PDB Chain combinations as a standardized tuple. The lexicographically smallest tuple should be used to represent all tuples. Then, ID 1ABC and Chain A, and 1AAB and Chain D would be e.g. represented as `(1aab,X1, ((1aab,D),(1abc,A)) )'."""

    def __new__(cls, represent, *ids):
        lid = ()
        allids = list(ids)
        #check for correct argument counts
        if len(allids) < 1:
            raise TypeError("This function takes min. 3 agument (%d given)" % (len(allids)+2))
        elif len(allids) == 1:
            #check if a list of ids or similar is given and unnest it
            t = (list,set,tuple)
            if isinstance(allids[0],t) and not isinstance(allids[0],longID):
                tmpids = list(allids[0])
        #check types
        for i in allids:
            if not type(i) is longIDsingle:
                raise TypeError("The provided argument '%s' is not of type longIDsingle." % str(i))
        #store the given IDs and try to be tolerant

        allids.sort()
        lid = (represent[0], represent[1], tuple(allids))

        return tuple.__new__(cls, lid)

    def __init__(self, pdbman, *ids):
        pass
        #super(longIDsingle,self).__init__(self.longid)


class IDmanage(object):
    """This class manages PDB ids in longID and longIDsingle datatypes. Chains represented by this IDs can be processed using common dictionary ops."""

    def __init__(self):
        self.childs = dict() #holds all longIDsingle ids in a tuple (value) repesented by the representative longIDsingle (key)
        # e.g. {  ('1abc','X1'):( ('1abc','A'),('1abc','B') )  }

        self.reps = dict() #for a given tuple of IDs, return its representative

        self.repcount = dict() # current count of X*'s for each PDB ID. Key is the PDB ID and the value the number of already assigned ids. If it is not present, this corresponds to not yet assigned
        # e.g. { '1abc':1 }     for (1abc,X1)


    def add(self, *ids):
        """Adds one or more present PDB ID / Chain pair to the Management. The ids can be in a list-like data structure or can be provided individually, being of a type supported by longIDsingle. The lexicographically smalles PDB ID is selected as representative. The representative is returned."""
        #unify ID list
        childs = set()
        for i in ids:
            childs.add(longIDsingle(i))

        if len(childs) == 0:
            raise NameError("List of IDs must not be empty!")

        #pick the lexicographic smalles PDB id as representative
        childs = tuple(sorted(childs))
        pdbid = childs[0][0]
        chain = childs[0][1]

        if childs in self.reps:
            #these ids are already present and mapped to a representative
            rep = self.reps[childs]

        elif len(childs) > 1:
            #add a representative and at least one real child

            #count the number of representatives
            if pdbid not in self.repcount:
                self.repcount[pdbid] = 1
            else:
                self.repcount[pdbid] +=1
            #add the representing chain to the dict
            chain = 'X%d' % self.repcount[pdbid]
            rep = longIDsingle( (pdbid, chain) )
            #add childs
            self.childs[rep] = childs
            #update reverse lookup dict
            self.reps[childs] = rep
        else:
            #add only a single ID with no childs
            rep = longIDsingle( (pdbid, chain) )
            self.childs[rep] = tuple(rep)
            self.repcount[rep] = 0
            #update reverse lookup dict
            self.reps[rep] = (rep)
        return rep


    def get(self, longid):
        """For a given ID, it returns all childs."""
        return ( longid, self.childs[longid] )

class IDs(object):

    def __init__(self, pdbpath='~/lib/pdb', obsoletefile='~/lib/pdbsuppl/obsolete.dat'):
        # read mapping of obsolete PDB ids
        path = os.path.expanduser(obsoletefile)
        f = open(path,'r')

        self.succ = dict() # dictionary mapping a pdb id to its successor

        for line in f:
            line = line.strip().split()
            try:
                self.succ[line[2].lower()] = line[3].lower()
            except IndexError:
                pass
        f.close()
        self.pdbpath = pdbpath



    def getPDBid(self, pdbid):
        return getPDBid(pdbid)

    def getlatestPDBid(self, pdbid):
        """Checks the list of obsolete PDB IDs and returns the successor."""
        pdbid = self.getPDBid(pdbid)
        if pdbid in self.succ:
            pdbid = self.succ[pdbid]
        return pdbid

    def getPDBfile(self,pdbid,get_latest_id=True):
        """Returns the path to the file of a given pdb id."""
        if get_latest_id:
            pdbid = self.getlatestPDBid(pdbid)
        pdbdir = os.path.expanduser(self.pdbpath)
        pdbfile = os.path.join(pdbdir,pdbid[1:3],'pdb'+pdbid+'.ent.gz')
        if not os.path.exists(pdbfile):
            raise IOError("No such file: '"+pdbfile+"'")
        return pdbfile



class Cluster(object):

    def __init__(self, **args):
        self.verbose = args.get('verbose') or False
        self.noDB = args.get('noDB') or False
        self.pdbclustid = dict() #maps (PDB id, chain id) to the cluster id
        clustidpdb = dict() #maps the cluster id to the chain representative
        self.clustidmembers = dict() #maps the cluster id to a set of cluster members

        #make a mapping from the PDB/Chain id to the clusterid and
        # a mapping from the cluster id to the cluster representative PDB id
        for row in self.pdbclust:
            #pdbclustid[(pdbid,chainid)] = clusterid
            clustid = row[0]
            self.pdbclustid[(row[2].lower(),row[3])] = clustid

            #record all members of a cluster
            if clustid not in self.clustidmembers:
                self.clustidmembers[clustid] = set()
            self.clustidmembers[clustid].add((row[2].lower(),row[3])) #add a new member
            if int(row[1]) == 1: #cluster representative
                clustidpdb[row[0]] = (row[2].lower(),row[3])

        #generate the dictionary mapping PDBid and Chain to the (PDBid, ChainID) cluster representative tuple, ommiting non protein chains
        nonprots = Comprot(noDB=self.noDB).PDBnonProteins()
        self.mapclust = dict()
        for tmptuple in self.pdbclustid:
            if tmptuple in nonprots:
                    print tmptuple, 'is not a protein chain, thus skipping it.'
                    sys.stdout.flush()
            else:
                clusttuple = clustidpdb[self.pdbclustid[tmptuple]]
                if tmptuple[0] not in self.mapclust:
                    self.mapclust[tmptuple[0]] = dict()
                self.mapclust[tmptuple[0]][tmptuple[1]] = clusttuple

    def represent(self, t):
        """Returns the cluster representative tuple for a given tuple of PDB id and chain id."""
        return self.mapclust[t[0].lower()][t[1]]

    def members(self, t):
        """Returns all members of the cluster for a given tuple of PDB id and chain id."""
        clustid = self.pdbclustid[t]
        return self.clustidmembers[clustid]


class Cluster95(Cluster):
    def __init__(self, **args):
        self.noDB = args.get('noDB') or False
        self.pdbclust = Comprot(noDB=self.noDB).PDBclust95() # get pdb chain clusters from comprot
        super(Cluster95,self).__init__(**args)


def test_pdbligand():

    f = '/tmp/bab/pdb6cox.pdb'
    getLigands(f)

if __name__ == "__main__":
    test_pdbligand()
