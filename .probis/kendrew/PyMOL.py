# The MIT License (MIT)
#
# Copyright (c) 2016 Joachim Haupt (TU Dresden / Germany)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

'''
Created on May 30, 2013

@author: joachimh
'''

from __future__ import division

from comprot.db import Comprot

from kendrew.toolchain.files import dictread
from kendrew.toolchain.Types import NormTuple
from kendrew.toolchain.basic import flatten
from kendrew.Molecules import transform_coordinate

import kendrew.PDB as pdb

from plipanalyzer import PLIPAnalyzer  # plipanalyzer script from plip-research git repo

from collections import defaultdict
import numpy
import json
import math
import re
import os
import sys

from pymol import cmd, CmdException, stored

cmd.set_color('myorange', [253, 174, 97])
cmd.set_color('mygreen', [171, 221, 164])
cmd.set_color('myred', [215, 25, 28])
cmd.set_color('myblue', [43, 131, 186])
cmd.set_color('mylightblue',[158, 202, 225] )
cmd.set_color('mylightgreen', [229, 245, 224])

PLIP_XML_PATH = '/ssd/neo4jdata/plip-xml'  # Path with PLIP xml result files for all PDB structures
PLIP_XML_PATH_BREAKCOMPOSITE = '/ssd/neo4jdata/plip-xml-breakcomposite'  # Same as above, but with split ligands



class SCOPloader(object):

    def __init__(self):

        fname = '/ssd/neo4jdata/bs-sim/02_fixed_1.5Amedian/BSdata_Aug2015_removed_unspecific_grouped/BSLigRMSD_1.500000_BSdata_Aug2015_removed_unspecific_grouped.csv'

        self.alignments = None
        self.pos = None

        self.d = defaultdict(set)
        f,lines = dictread(fname)
        for line in lines:
            scops = NormTuple([line['SCOP1'],line['SCOP2']])
            if 'NA' not in scops:
                self.d[scops].add((float(line['LigRMSD']),line['AlignedLigands']))
        f.close()

    def set(self, scop1, scop2):

        scops = NormTuple([scop1, scop2])
        self.alignments = sorted(self.d[scops]) #sort by LigandRMSD
        self.pos = -1
        print 'Found %d ligand pairs for %s.' % (len(self.alignments), str(scops))
        if len(self.alignments) > 0:
            self.next()


    def next(self):
        self.pos += 1
        if self.pos >= len(self.alignments):
            print "WARNING: Reached last match! Restarting at first match..."
            self.pos = 0
        ligtuple = self.alignments[self.pos][1]
        print 'Loading results for %s.' % (ligtuple)
        return clusterloaderid(ligtuple)


class ClusterLoader(object):

    def __init__(self, pdb1, chain1, pdb2, chain2, het1=None, het2=None, cutoff = 1.5, find_clust_rep=True, plip=True, split=False):
        self.cluster = None
        self.plip = plip  # Turns on/off PLIP visualization. Default is 'True'.
        self.het1 = het1
        self.het2 = het2
        self.pdb1 = pdb1
        self.pdb2 = pdb2
        self.chain1 = chain1
        self.chain2 = chain2
        self.plip = plip
        self.split = split
        self.saved_interactions = {}  # Dictionary for saved PLIP Interaction data

        comprot = Comprot()
        if find_clust_rep:
            pdb1, chain1 = comprot.PDBclust95getRepresentative(pdb1, chain1)
            pdb2, chain2 = comprot.PDBclust95getRepresentative(pdb2, chain2)
        else:
            pdb1 = pdb1.upper()
            pdb2 = pdb2.upper()
        self.cutoff = cutoff
        self.cluster = comprot.BS_sim_probis(pdb1,pdb2,chain1,chain2,self.cutoff)
        if len(self.cluster) == 0:
            print "Warning: %s_%s - %s_%s not in the cluster representatives. Checking among the members..." % (pdb1, chain1, pdb2, chain2)
            self.cluster = comprot.BS_sim_probis(pdb1,pdb2,chain1,chain2,self.cutoff,get_members=True)
        self.cluster.sort(key=lambda x: x['LIG_RMSD'])

        self.psefname = None
        self.pos = -1

        print "Found %d BS matches in the data set with a cutoff at %.2f." % (len(self.cluster), self.cutoff)

        if len(self.cluster) > 0:
            self.next()

    def next(self):

        self.pos += 1
        if not self.pos >= len(self.cluster):
            return self.get(self.pos)
        else:
            self.pos = -1
            print "WARNING: Reached last match! Restarting at first match..."
            self.next()

    def save(self):
        cmd.save(self.psefname)
        print "Wrote PyMOL session file '%s'" % (self.psefname)

    def get(self, pos):

        c = Comprot()
        self.pos = int(pos)
        e = self.cluster[self.pos]

        # Fetch and save additional PLIP data if necessary
        current_pdbid1, current_pdbid2 = e['PDB_ID1_ORIG'], e['PDB_ID2_ORIG']
        if current_pdbid1 not in self.saved_interactions:
            self.saved_interactions[current_pdbid1] = self.load_plip(current_pdbid1, self.split)
        if current_pdbid2 not in self.saved_interactions:
            self.saved_interactions[current_pdbid2] = self.load_plip(current_pdbid2, self.split)

        # Check the PLIP data for the current complexes (cluster members)
        if self.plip:
            interactions1 = self.saved_interactions[current_pdbid1]
            interactions2 = self.saved_interactions[current_pdbid2]
            if interactions1 is None or interactions2 is None:  # Turn off PLIP vis. if data is not available
                self.plip = False


        pdb1 = e['PDB_ID1']
        chain1 = e['PDB_CHAIN1']
        name1 = c.PDBchainName(pdb1, chain1)
        pdb2 = e['PDB_ID2']
        chain2 = e['PDB_CHAIN2']
        name2 = c.PDBchainName(pdb2, chain2)
        if self.het1 is not None and pdb1 == self.pdb1 and chain1 == self.chain1:
            het1 = self.het1 # preserve provided HET ID if possible
        else:
            het1 = e['PDB_HET_ID1']
        cid1, hetname1 = c.HETtoCID(het1)
        if self.het2 is not None and pdb2 == self.pdb2 and chain2 == self.chain2:
            het2 = self.het2 # preserve provided HET ID if possible
        else:
            het2 = e['PDB_HET_ID2']
        cid2, hetname2 = c.HETtoCID(het2)

        self.psefname = "%s_%s_%s_%s_%s.pse" % (pdb1, pdb2, str(self.pos).zfill(2),het1, het2)

        tmp = "Observation Nr. %d" % self.pos
        print """
        %s
        %s
        PyMOL Session File      :  %s

        Cluster Representative 1:  %s    (%s)
                               2:  %s    (%s)
        Cluster Member  (blue) 1:  %s
                       (green) 2:  %s
        HET ID                 1:  %s    (%s, %d)
                               2:  %s    (%s, %d)
        Ligand RMSD             :  %s
                    Raw         :  %s
                    Ratio       :  %s
        Matched Atoms           :  %s
        """ % (tmp,
               "-" * len(tmp),
               self.psefname,\
               pdb1, name1,\
               pdb2, name2,\
               e['PDB_ID1_ORIG'],\
               e['PDB_ID2_ORIG'],\
               het1.ljust(4), hetname1, cid1,\
               het2.ljust(4), hetname2, cid2,\
               e['LIG_RMSD'],\
               e['LIG_RMSD_RAW'],\
               e['LIG_RMSD_RATIO'],\
               e['LIG_RMSD_MATCH']\
               )

        return probis_load_entry(self.cluster[self.pos], interactions1, interactions2, plip=self.plip)

    def load_plip(self, pdbid, split):
        if not split:  # Standard
            plip_path = PLIP_XML_PATH
        else:  # Split ligands
            plip_path = PLIP_XML_PATH_BREAKCOMPOSITE
        pdbpath = '/'.join([plip_path, pdbid[1:3], pdbid, 'report.xml'])
        if os.path.isfile(pdbpath):  # Does the file exist?
            return PLIPAnalyzer(pdbpath)  # Read and store information from XML file
        else:
            print "The PLIP XML file for %s could not be found at %s" % (pdbid, pdbpath)
            return None


def probis_load_entry(entry,\
                      interactions1,
                      interactions2,
                      pdb1='PDB_ID1_ORIG',\
                      pdb2='PDB_ID2_ORIG',\
                      het1='PDB_HET_ID1',\
                      het2='PDB_HET_ID2',\
                      trans1='TRANSFORM1',\
                      trans2='TRANSFORM2',\
                      deleteall=True,
                      plip=True):
    """
DESCRIPTION

    Loads a ProBiS aligend pair from the local files and
    decorates the ligand.

USAGE

    probis_load_example PDB1, PDB2, HET1, HET2 [,False]

ARGUMENTS

    PDB1    -- Query
    PDB2    -- Target
    HET1    -- Hetero ID for Query
    HET2    -- Hetero ID for Target
    [False] -- don't delete all objects before starting
    """

    sys.stdout.write("Building PyMOL Session...")
    sys.stdout.flush()

    if deleteall:
        cmd.delete("all")

    het1 = entry[het1]
    het2 = entry[het2]

    #init PDB stuff
    pdb1 = entry[pdb1]
    pdb2 = entry[pdb2]
    ids = pdb.IDs() #PDB ID management constructor

    #find the alignment file
    #

    def _dist(obj, sele1, sele2):
#        return cmd.distance(obj, sele1, sele2)
        stored.pos = []
        cmd.iterate_state(1, sele1,'stored.pos.append((x,y,z))')
        coords1 = stored.pos
        stored.pos = []
        cmd.iterate_state(1, sele2,'stored.pos.append((x,y,z))')
        coords2 = stored.pos
        l = [ sum( (a-b)**2 for a, b in zip(coord1, coord2) ) for coord1 in coords1 for coord2 in coords2 ]
        l.sort()
        l = l[:int(len(l)/3)]
        l = [math.sqrt(tmp) for tmp in l]
        if len(l) == 0:
            return float('inf')
        else:
            return sum(l)/len(l)

    def open_files(pdb1,pdb2):
        #open the transformed query and the target pdb file
        #if they don't exist return an error
        f1 = ids.getPDBfile(pdb.getPDBid(pdb1),get_latest_id=False)
        f2 = ids.getPDBfile(pdb.getPDBid(pdb2),get_latest_id=False)
        return [f1,f2]

#     rev = pdb1 > pdb2 # to reverse or not reverse...
#     pdbs = [pdb1,pdb2]
#     if rev:
#         pdbs.reverse()
#     pdbs = tuple(pdbs)
#     pdb1, pdb2 = pdbs
    files = open_files(pdb1, pdb2)
#
#     if rev:
#         files.reverse()
    f1,f2 = files

    #load files

    pdb1 = pdb1.lower().strip()
    pdb2 = pdb2.lower().strip()
#     print pdb1,pdb2


    #fresh pdb files
    cmd.load(f1,pdb1)
    cmd.load(f2,pdb2)

    #transform the fresh to the aligned

    transi = entry[trans1]
    transj = entry[trans2]

    if transi == None:
        print 'Transformation not found.'
        sys.exit()
    transi, roti = json.loads(transi)
    transj, rotj = json.loads(transj)
    roti = flatten(roti)
    rotj = flatten(rotj)
    pdb1_tmp = "%s_tmp" % pdb1
    pdb2_tmp = "%s_tmp" % pdb2
    cmd.create(pdb1_tmp, pdb1)
    cmd.create(pdb2_tmp, pdb2)

    cmd.transform_selection(pdb1,
       [roti[0], roti[1], roti[2], transi[0],
        roti[3], roti[4], roti[5], transi[1],
        roti[6], roti[7], roti[8], transi[2],
        0, 0, 0, 1], homogenous=1)
    cmd.transform_selection(pdb2_tmp,
       [roti[0], roti[1], roti[2], transi[0],
        roti[3], roti[4], roti[5], transi[1],
        roti[6], roti[7], roti[8], transi[2],
        0, 0, 0, 1], homogenous=1)
    cmd.transform_selection(pdb2,
       [rotj[0], rotj[1], rotj[2], transj[0],
        rotj[3], rotj[4], rotj[5], transj[1],
        rotj[6], rotj[7], rotj[8], transj[2],
        0, 0, 0, 1], homogenous=1)
    cmd.transform_selection(pdb1_tmp,
       [rotj[0], rotj[1], rotj[2], transj[0],
        rotj[3], rotj[4], rotj[5], transj[1],
        rotj[6], rotj[7], rotj[8], transj[2],
        0, 0, 0, 1], homogenous=1)

    cmd.hide('everything')

    def _select_transform(pdb1, pdb2, het1, het2):
        #get the HET IDs and identify the ligands
        ligs1 = "ligand_%s" % pdb1
        cmd.select(ligs1,"none")
        ligs2 = "ligand_%s" % pdb2
        cmd.select(ligs2,"none")

    #     print ligs1, ligs2, [het1,het2]
        for het in [het1,het2]:
            cmd.select(ligs1,"%s | (%s and resn %s)" % (ligs1, pdb1, het))
            cmd.select(ligs2,"%s | (%s and resn %s)" % (ligs2, pdb2, het))

        #get the two ligands, which are closest together
        ligs1resi = []
        myspace = {'ligs1resi': ligs1resi}
        cmd.iterate(ligs1,"ligs1resi.append((resi,chain))", space=myspace)
        ligs1resi = set(ligs1resi)
        ligs2resi = []
        myspace = {'ligs2resi': ligs2resi}
        cmd.iterate(ligs2,"ligs2resi.append((resi,chain))", space=myspace)
        ligs2resi = set(ligs2resi)

        dists = []
        tmp = list(ligs1resi)
        while len(tmp) > 0:
            resi1,chain1 = tmp.pop()
            for resi2,chain2 in ligs2resi:
                dist = _dist('tmp', '%s and resi %s and chain %s' % (ligs1,resi1,chain1), '%s and resi %s and chain %s' % (ligs2,resi2,chain2))
                dists.append( (dist,resi1,resi2,chain1,chain2) )
        cmd.delete('tmp')
        dists.sort(key = lambda x:x[0])
        dist, resi1, resi2, chain1, chain2 = dists[0]

        return dist, ligs1, ligs2, resi1, resi2, chain1, chain2

    first_transform = _select_transform(pdb1, pdb2, het1, het2)
    second_transform = _select_transform(pdb1_tmp, pdb2_tmp, het1, het2)

    if first_transform[0] > second_transform[0]:
        #second transformation is better
        dist, ligs1, ligs2, resi1, resi2, chain1, chain2 = second_transform
        cmd.delete(pdb1)
        cmd.delete(pdb2)
        cmd.set_name(pdb1_tmp, pdb1)
        cmd.set_name(pdb2_tmp, pdb2)
        print "Used inverse rotation scheme. Diff in dist is %.2f" % (first_transform[0] - second_transform[0])
        inverse_transform = False
    else:
        dist, ligs1, ligs2, resi1, resi2, chain1, chain2 = first_transform
        cmd.delete(pdb1_tmp)
        cmd.delete(pdb2_tmp)

        # Unique ligand selection
        cmd.select(ligs1,"%s and resi %s and chain %s" % (ligs1, resi1, chain1))
        cmd.select(ligs2,"%s and resi %s and chain %s" % (ligs2, resi2, chain2))

        #color C_alphas
        cmd.color('myblue', "%s and symbol c" % pdb1)
        cmd.color('mygreen', "%s and symbol c" % pdb2)
        inverse_transform = True

    if plip:  # Visualization using PLIP Interaction Data
        # Sets up standard settings for a nice visualization.
        cmd.set('bg_rgb', [1.0, 1.0, 1.0])  # White background
        cmd.set('depth_cue', 0)  # Turn off depth cueing (no fog)
        cmd.set('cartoon_side_chain_helper', 0)  # Improve combined visualization of sticks and cartoon
        cmd.set('cartoon_fancy_helices', 1)  # Nicer visualization of helices (using tapered ends)
        cmd.set('transparency_mode', 1)  # Turn on multilayer transparency
        cmd.set('dash_radius', 0.05)
        cmd.set('dash_gap', 0)  # Show not dashes, but lines for the pliprofiler
        cmd.set('ray_shadow', 0)  # Turn on ray shadows for clearer ray-traced images

        # Get the correct residue number of the ligand
        ligids = {'l1': [], 'l2': []}
        cmd.iterate(ligs1, 'l1.append((resn, chain, resi))', space=ligids)
        cmd.iterate(ligs2, 'l2.append((resn, chain, resi))', space=ligids)
        hetid1, chain1, pos1 = list(set(ligids['l1']))[0]
        hetid2, chain2, pos2 = list(set(ligids['l2']))[0]
        uid1, uid2 = ':'.join([hetid1, chain1, pos1]), ':'.join([hetid2, chain2, pos2])

        # Now search in the XML data for the correct binding site, also in the members in case of composite ligands
        contacts1, contacts2 = None, None
        for bsid in interactions1.bsites:
            if bsid == uid1 or uid1 in interactions1.bsites[bsid].members:
                contacts1 = interactions1.bsites[bsid]
        for bsid in interactions2.bsites:
            if bsid == uid2 or uid2 in interactions2.bsites[bsid].members:
                contacts2 = interactions2.bsites[bsid]
        if contacts1 is None or contacts2 is None:
            print "Warning: Problem with selecting binding sites from PLIP data. Trying without PLIP..."
            print "\tComplex1: %s (Interactions: %s)\n\tComplex2: %s (Interactions: %s)" \
                  % (':'.join([pdb1, hetid1, chain1, pos1]), 'No' if contacts1 is None else 'Yes',
                     ':'.join([pdb2, hetid2, chain2, pos2]), 'No' if contacts2 is None else 'Yes')
            plip = False
        else:
            # #@Todo Visualize metal ions if there are any

            # Expand selection for composite ligands and show them
            for member in contacts1.members:
                resid, chain, resnr = member.split(':')
                cmd.select(ligs1, '%s or (resn %s and chain %s and resi %s)' % (ligs1, resid, chain, resnr))
                cmd.color('mylightblue', ligs1)
            for member in contacts2.members:
                resid, chain, resnr = member.split(':')
                cmd.select(ligs2, '%s or (resn %s and chain %s and resi %s)' % (ligs2, resid, chain, resnr))
                cmd.color('mylightgreen', ligs2)
            cmd.show('sticks', '%s or %s' % (ligs1, ligs2))
            cmd.util.cnc('all')

            i = 0
            for complex, pdbid in (contacts1, pdb1), (contacts2, pdb2):
                i += 1

                #################################################
                # Select correct translation, rotation matrices #
                #################################################

                if inverse_transform:
                    if i == 1:  # Protein 1
                        translation, rotation = transi, roti
                    else:  # Protein 2
                        translation, rotation = transj, rotj
                else:
                    if i == 1:  # Protein 1
                        translation, rotation = transj, rotj
                    else:
                        translation, rotation = transi, roti

                translation = numpy.array(translation)
                rotation = numpy.matrix(rotation)
                rotation.shape = (3, 3)

                ###########################
                # Create empty selections #
                ###########################
                for group in ['Hydrophobic-P', 'Hydrophobic-L', 'HBondDonor-P', 'HBondDonor-L', 'HBondAccept-P', 'HBondAccept-L',
                              'HalogenAcc', 'HalogenDonor', 'Water', 'MetalIons', 'StackRings-P', 'PosCharge-P', 'PosCharge-L',
                              'NegCharge-P', 'NegCharge-L', 'PiCatRing-P', 'StackRings-L', 'PiCatRing-L', 'Metal-M', 'Metal-P',
                              'Metal-W', 'Metal-L', 'Unpaired-HBA', 'Unpaired-HBD', 'Unpaired-HAL', 'Unpaired-RINGS']:
                    cmd.select('%s_%i' % (group, i), 'None')

                ######################################
                # Visualize hydrophobic interactions #
                ######################################

                if not len(complex.hydrophobics) == 0:
                    select_P = 'Hydrophobic-P_%i' % i
                    select_L = 'Hydrophobic-L_%i' % i
                    select_I = 'Hydrophobic_%i' % i

                    for hydroph in complex.hydrophobics:
                        cmd.select(select_P, '%s | (%s & id %i)' % (select_P, pdbid, hydroph.protcarbonidx))
                        cmd.select(select_L, '%s | (%s & id %i)' % (select_L, pdbid, hydroph.ligcarbonidx))
                        cmd.select('tmp_bs', '%s & id %i' % (pdbid, hydroph.protcarbonidx))
                        cmd.select('tmp_lig', '%s & id %i' % (pdbid, hydroph.ligcarbonidx))
                        cmd.distance(select_I, 'tmp_bs', 'tmp_lig')
                    cmd.set('dash_gap', 0.5, 'Hydrophobic_%i' % i)
                    cmd.set('dash_color', 'grey50', 'Hydrophobic_%i' % i)

                #####################
                # Visualize H-Bonds #
                #####################

                if not len(complex.hbonds) == 0:
                    select_PD = 'HBondDonor-P_%i' % i
                    select_LD = 'HbondDonor-L_%i' % i
                    select_PA = 'HBondAccept-P_%i' % i
                    select_LA = 'HBondAccept-L_%i' % i
                    select_I = 'HBonds_%i' % i
                    for hbond in complex.hbonds:
                        if hbond.protisdon == 'T':
                            cmd.select(select_PD, '%s | (%s & id %i)' % (select_PD, pdbid, hbond.donoridx))
                            cmd.select(select_LA, '%s | (%s & id %i)' % (select_LA, pdbid, hbond.acceptoridx))
                            cmd.select('tmp_bs', '%s and id %i' % (pdbid, hbond.donoridx))
                            cmd.select('tmp_lig', '%s and id %i' % (pdbid, hbond.acceptoridx))
                            cmd.distance(select_I, 'tmp_bs', 'tmp_lig')

                        else:
                            cmd.select(select_PA, '%s | (%s & id %i)' % (select_PA, pdbid, hbond.acceptoridx))
                            cmd.select(select_LD, '%s | (%s & id %i)' % (select_LD, pdbid, hbond.donoridx))
                            cmd.select('tmp_bs', '%s and id %i' % (pdbid, hbond.acceptoridx))
                            cmd.select('tmp_lig', '%s and id %i' % (pdbid, hbond.donoridx))
                            cmd.distance(select_I, 'tmp_bs', 'tmp_lig')
                    cmd.set('dash_color', 'blue', select_I)

                ###########################
                # Visualize Halogen Bonds #
                ###########################

                if not len(complex.halogens) == 0:
                    select_P = 'HalogenAcc_%i' % i
                    select_L = 'HalogenDonor_%i' % i
                    select_I = 'HalogenBonds_%i' % i
                    for halogen in complex.halogens:
                        cmd.select(select_P, '%s | (%s & id %i)' % (select_P, pdbid, halogen.acc_idx))
                        cmd.select(select_L, '%s | (%s & id %i)' % (select_L, pdbid, halogen.don_idx))
                        cmd.select('tmp_bs', '%s & id %i' % (pdbid, halogen.acc_idx))
                        cmd.select('tmp_lig', '%s & id %i' % (pdbid, halogen.don_idx))
                        cmd.distance(select_I, 'tmp_bs', 'tmp_lig')
                    cmd.set('dash_color', 'greencyan', select_I)

                #########################
                # Visualize Pi-Stacking #
                #########################

                if not len(complex.pi_stacks) == 0:
                    select_P = 'StackRings-P_%i' % i
                    select_L = 'StackRings-L_%i' % i
                    select_CP = 'Centroids-P_%i' % i
                    select_CL = 'Centroids-L_%i' % i
                    select_IParallel = 'PiStackingP_%i' % i
                    select_ITShaped = 'PiStackingT_%i' % i
                    for j, stack in enumerate(complex.pi_stacks):
                        pilig_ids = '+'.join(map(str, stack.lig_idx_list))
                        cmd.select(select_L, '%s | (%s & id %s)' % (select_L, pdbid, pilig_ids))
                        cmd.select(select_P, '%s | (%s & i. %i & chain %s)' % (select_P, pdbid, stack.resnr, stack.reschain))

                        stack_protcoo = transform_coordinate(stack.protcoo, translation, rotation, flat=True)
                        stack_ligcoo = transform_coordinate(stack.ligcoo, translation, rotation, flat=True)
                        cmd.pseudoatom('ps-pistack-1-%i_%i' % (j, i), pos=stack_protcoo)
                        cmd.pseudoatom('ps-pistack-2-%i_%i' % (j, i), pos=stack_ligcoo)
                        cmd.pseudoatom(select_CP, pos=stack_protcoo)
                        cmd.pseudoatom(select_CL, pos=stack_ligcoo)

                        if stack.type == 'P':
                            cmd.distance(select_IParallel, 'ps-pistack-1-%i_%i' % (j, i), 'ps-pistack-2-%i_%i' % (j, i))
                        if stack.type == 'T':
                            cmd.distance(select_ITShaped, 'ps-pistack-1-%i_%i' % (j, i), 'ps-pistack-2-%i_%i' % (j, i))

                    if object_exists(select_IParallel):
                        cmd.set('dash_color', 'green', select_IParallel)
                        cmd.set('dash_gap', 0.3, select_IParallel)
                        cmd.set('dash_length', 0.6, select_IParallel)
                    if object_exists(select_ITShaped):
                        cmd.set('dash_color', 'smudge', select_ITShaped)
                        cmd.set('dash_gap', 0.3, select_ITShaped)
                        cmd.set('dash_length', 0.6, select_ITShaped)

                ####################################
                # Visualize Cation-pi interactions #
                ####################################

                if not len(complex.pi_cations) == 0:
                    select_PC = 'Chargecenter-P_%i' % i
                    select_LC = 'Chargecenter-L_%i' % i
                    select_PR = 'Centroids-P_%i' % i
                    select_LR = 'Centroids-L_%i' % i
                    select_I = 'PiCation_%i' % i
                    for j, picat in enumerate(complex.pi_cations):
                        picat_protcoo = transform_coordinate(picat.protcoo, translation, rotation, flat=True)
                        picat_ligcoo = transform_coordinate(picat.ligcoo, translation, rotation, flat=True)
                        cmd.pseudoatom('ps-picat-1-%i_%i' % (j, i), pos=picat_protcoo)
                        cmd.pseudoatom('ps-picat-2-%i_%i' % (j, i), pos=picat_ligcoo)

                        picatlig_ids = '+'.join(map(str, picat.lig_idx_list))
                        if picat.protcharged:
                            cmd.pseudoatom(select_PC, pos=picat_protcoo)
                            cmd.pseudoatom(select_LR, pos=picat_ligcoo)
                            cmd.select('PiCatRing-L_%i' % i, 'PiCatRing-L_%i | (%s & id %s)' % (i, pdbid, picatlig_ids))
                            cmd.select('PosCharge-P_%i' % i, 'PosCharge-P_%i | (%s & i. %i & chain %s)' % (i, pdbid, picat.resnr, picat.reschain))

                        else:
                            cmd.pseudoatom(select_LC, pos=picat_ligcoo)
                            cmd.pseudoatom(select_PR, pos=picat_protcoo)
                            cmd.select('PosCharge-L_%i' % i, 'PosCharge-L_%i | (%s & id %s)' % (i, pdbid, picatlig_ids))
                            cmd.select('PiCatRing-P_%i' % i, 'PiCatRing-P_%i | (%s & i. %i & chain %s)' % (i, pdbid, picat.resnr, picat.reschain))
                        cmd.distance(select_I, 'ps-picat-1-%i_%i' % (j, i), 'ps-picat-2-%i_%i' % (j, i))
                    cmd.set('dash_color', 'orange', select_I)
                    cmd.set('dash_gap', 0.3, select_I)
                    cmd.set('dash_length', 0.6, select_I)

                ##########################
                # Visualize salt bridges #
                ##########################

                if not len(complex.sbridges) == 0:
                    select_PN = 'NegCharge-P_%i' % i
                    select_PP = 'PosCharge-P_%i' % i
                    select_LN = 'NegCharge-L_%i' % i
                    select_LP = 'NegCharge-P_%i' % i
                    select_I = 'Saltbridges_%i' % i

                    for j, sb in enumerate(complex.sbridges):
                        sblig_ids = '+'.join(map(str, sb.lig_idx_list))
                        sb_protcoo = transform_coordinate(sb.protcoo, translation, rotation, flat=True)
                        sb_ligcoo = transform_coordinate(sb.ligcoo, translation, rotation, flat=True)
                        if sb.protispos:
                            cmd.select(select_LN, '%s | (%s & id %s)' % (select_LN, pdbid, sblig_ids))
                            cmd.select(select_PP, '%s | (%s & i. %i & chain %s)' % (select_PP, pdbid, sb.resnr, sb.reschain))
                        else:
                            cmd.select(select_LP, '%s | (%s & id %s)' % (select_LP, pdbid, sblig_ids))
                            cmd.select(select_PN, '%s | (%s & i. %i & chain %s)' % (select_PN, pdbid, sb.resnr, sb.reschain))

                        cmd.pseudoatom('Chargecenter-P_%i' % i, pos=sb_protcoo)
                        cmd.pseudoatom('Chargecenter-L_%i' % i, pos=sb_ligcoo)
                        cmd.pseudoatom('ps-sbp-%i_%i' % (j, i), pos=sb_protcoo)
                        cmd.pseudoatom('ps-sbl-%i_%i' % (j, i), pos=sb_ligcoo)
                        cmd.distance(select_I, 'ps-sbp-%i_%i' % (j, i), 'ps-sbl-%i_%i' % (j, i))
                    cmd.set('dash_color', 'yellow', select_I)
                    cmd.set('dash_gap', 0.5, select_I)

                ########################################
                # Water-bridged H-Bonds (first degree) #
                ########################################
                if not len(complex.wbridges) == 0:
                    select_HBAP = 'HbondAccept-P_%i' % i
                    select_HBDP = 'HbondDonor-P_%i' % i
                    select_HBAL = 'HbondAccept-L_%i' % i
                    select_HBDL = 'HbondDonor-P_%i' % i
                    select_W = 'Water_%i' % i
                    select_I = 'WaterBridges_%i' % i

                for j, bridge in enumerate(complex.wbridges):
                    if bridge.protisdon:
                        cmd.select(select_HBDP, '%s | (%s & id %i)' % (select_HBDP, pdbid, bridge.donor_idx))
                        cmd.select(select_HBAL, '%s | (%s & id %i)' % (select_HBAL, pdbid, bridge.acceptor_idx))
                    else:
                        cmd.select(select_HBDL, '%s | (%s & id %i)' % (select_HBDL, pdbid, bridge.donor_idx))
                        cmd.select(select_HBAP, '%s | (%s & id %i)' % (select_HBAP, pdbid, bridge.acceptor_idx))
                    cmd.select(select_W, '%s | (%s & id %i)' % (select_W, pdbid, bridge.water_idx))
                    cmd.select('tmp_don_%i' % i, '%s & id %i' % (pdbid, bridge.donor_idx))
                    cmd.select('tmp_water_%i' % i, '%s & id %i' % (pdbid, bridge.water_idx))
                    cmd.select('tmp_acc_%i' % i, '%s & id %i' % (pdbid, bridge.acceptor_idx))
                    cmd.distance(select_I, 'tmp_acc_%i' % i, 'tmp_water_%i' % i)
                    cmd.distance(select_I, 'tmp_don_%i' % i, 'tmp_water_%i' % i)

                    cmd.set('dash_color', 'lightblue', select_I)
                    cmd.delete('tmp_water_%i or tmp_acc_%i or tmp_don_%i' % (i, i, i))
                    cmd.color('lightblue', select_W)
                    cmd.show('spheres', select_W)

                ###################
                # Metal Complexes #
                ###################
                if not len(complex.metal_complexes) == 0:
                    select_M = 'Metal-M_%i' % i
                    select_W = 'Metal-W_%i' % i
                    select_P = 'Metal-P_%i' % i
                    select_L = 'Metal-L_%i' % i
                    select_I = 'MetalComplexes_%i' % i
                    for j, metalc in enumerate(complex.metal_complexes):
                        cmd.select(select_M, '%s | (%s & id %s)' % (select_M, pdbid, metalc.metal_idx))
                        cmd.select('tmp_m', '%s & id %i' % (pdbid, metalc.metal_idx))
                        cmd.select('tmp_t', '%s & id %i' % (pdbid, metalc.target_idx))
                        if metalc.location == 'water':
                            cmd.select(select_W, '%s | (%s & id %s)' % (select_W, pdbid, metalc.target_idx))
                        if metalc.location.startswith('protein'):
                            cmd.select(select_P, '%s | (%s & id %s)' % (select_P, pdbid, metalc.target_idx))
                        if metalc.location == 'ligand':
                            cmd.select(select_L, '%s | (%s & id %s)' % (select_L, pdbid, metalc.target_idx))
                        cmd.distance(select_I, 'tmp_m', 'tmp_t')
                        cmd.delete('tmp_m or tmp_t')

                    cmd.set('dash_color', 'violetpurple', select_I)
                    cmd.set('dash_gap', 0.5, select_I)
                    # Show water molecules for metal complexes
                    cmd.show('spheres', select_M)
                    cmd.show('spheres', select_W)
                    cmd.color('lightblue', select_W)
                    cmd.color('hotpink', select_M)
                    cmd.hide('sticks', select_M)
                    cmd.set('sphere_scale', 0.3, select_M)


                ######################
                # Visualize the rest #
                ######################

                # Show sticks for all residues interacing with the ligand
                cmd.select('AllBSRes_%i' % i, 'byres (Hydrophobic-P_%i or HBondDonor-P_%i or HBondAccept-P_%i '
                                              'or PosCharge-P_%i or NegCharge-P_%i or StackRings-P_%i or PiCatRing-P_%i '
                                              'or HalogenAcc_%i or Metal-P_%i)'
                           % (i, i, i, i, i, i, i, i, i))
                allbsresi = 'AllBSRes_%i' % i
                allbsresiextra = 'AllBSResExtra_%i' % i
                # show adjecent amino acids in the other protein
                cmd.select(allbsresiextra, 'none')
                tmpdict = { 'interactingresidues' : [] }
                cmd.iterate("(%s and name ca)" % allbsresi,"interactingresidues.append((model, chain, resi))",space=tmpdict)
                interactingresidues = tmpdict['interactingresidues']
                for interactingresidue in interactingresidues:
                    seleinteractingresidue = "(model '%s' and chain '%s'  and resi '%s')" % interactingresidue
                    cmd.select(allbsresiextra, "%s or byres ( ((sidechain and %s) around 2) and (!%s) and sidechain)" % (allbsresiextra, seleinteractingresidue, interactingresidue[0]))

                cmd.show('sticks', allbsresi)
                # Show spheres for the ring centroids
                cmd.hide('everything', 'centroids*')
                cmd.show('nb_spheres', 'centroids*')
                # Show spheres for centers of charge
                if object_exists('Chargecenter-P_%i' % i) or object_exists('Chargecenter-L_%i' % i):
                    cmd.hide('nonbonded', 'chargecenter*')
                    cmd.show('spheres', 'chargecenter*')
                    cmd.set('sphere_scale', 0.4, 'chargecenter*')
                    cmd.color('yellow', 'chargecenter*')

                ####################
                # Last refinements #
                ####################

                cmd.set('valence', 1)  # Show bond valency (e.g. double bonds)
                # Optional cartoon representation of the protein
                copycartoon = '%sCartoon' % pdbid
                # make PyMOL shut up...
                cmd.feedback("disable","all","actions")
                cmd.feedback("disable","all","results")
                cmd.copy(copycartoon, pdbid)
                cmd.feedback("enable","all","actions")
                cmd.feedback("enable","all","results")
                cmd.hide('everything', copycartoon)
                copycartoonsele = '%s and ( bychain ( (%s or %s) around 10) ) ' % (copycartoon, ligs1, ligs2)
                cmd.show('cartoon', copycartoonsele )
                cmd.hide('cartoon', pdbid)
                cmd.set('stick_transparency', 1, copycartoon)
                cmd.show('sticks', 'byresidue (%s and ( %s around 3))' % (copycartoon, allbsresi) )
                cmd.set('sphere_scale', 0.2, 'resn HOH')  # Needs to be done here because of the copy made
                cmd.set('sphere_transparency', 0.4, '!resn HOH')
                if 'Centroids*' in cmd.get_names("selections"):
                    cmd.color('grey80', 'Centroids*')
                cmd.hide('spheres', '%sCartoon' % pdbid)
                cmd.hide('cartoon', '%sCartoon and resn DA+DG+DC+DU+DT+A+G+C+U+T' % pdbid)  # Hide DNA/RNA Cartoon

                ##############################
                # Organization of selections #
                ##############################

                # Group non-empty selections
                cmd.group('Interactions%i' % i, 'Hydrophobic_%i HBonds_%i HalogenBonds_%i WaterBridges_%i PiCation_%i'
                                                ' PiStackingP_%i PiStackingT_%i Saltbridges_%i MetalComplexes_%i'
                          % (i, i, i, i, i, i, i, i, i))

                cmd.group('Atoms.Protein%i' % i, 'Hydrophobic-P_%i HBondAccept-P_%i HBondDonor-P_%i HalogenAccept_%i '
                                                 'Centroids-P_%i PiCatRing-P_%i StackRings-P_%i PosCharge-P_%i '
                                                 'NegCharge-P_%i AllBSRes_%i Chargecenter-P_%i  Metal-P_%i AllBSResExtra_%i'
                          % (i, i, i, i, i, i, i, i, i, i, i, i, i))
                cmd.group('Atoms.Ligand%i' % i, 'Hydrophobic-L_%i HBondAccept-L_%i HBondDonor-L_%i HalogenDonor_%i '
                                                'Centroids-L_%i NegCharge-L_%i PosCharge-L_%i NegCharge-L_%i '
                                                'ChargeCenter-L_%i StackRings-L_%i PiCatRing-L_%i Metal-L_%i Metal-M_%i '
                                                'Unpaired-HBA_%i Unpaired-HBD_%i Unpaired-HAL_%i Unpaired-RINGS_%i'
                          % (i, i, i, i, i, i, i, i, i, i, i, i, i, i, i, i, i))
                cmd.group('Atoms.Other%i' % i, 'Water_%i Metal-W_%i' % (i, i))
                cmd.group('Atoms%i' % i, 'Atoms.Protein%i Atoms.Ligand%i Atoms.Other%i' % (i, i, i))
                cmd.order('*', 'y')

                cmd.group('PLIP_%s' % pdbid, 'Interactions%i Atoms%i' % (i, i))



                cmd.delete('tmps_bs or tmp_lig')
                cmd.hide('labels', 'Interactions%i' % i)

            #Restrict the selection of extra residues to the non-interacting
            reallyallbsresi = " or ".join([ 'AllBSRes_%i' % (j+1) for j in range(i) ])
            for j in range(i):
                allbsresiextra = 'AllBSResExtra_%i' % (j+1)
                cmd.select(allbsresiextra, "%s and !(%s or *Cartoon)" % (allbsresiextra, reallyallbsresi) )
                cmd.show('lines', allbsresiextra)

            # Delete all empty and temporary selections
            selections = cmd.get_names("selections")
            for selection in selections:
                if len(cmd.get_model(selection).atom) == 0:
                    cmd.delete(selection)
            cmd.deselect()
            cmd.delete('tmp*')
            cmd.delete('ps-*')

    if not plip:  # Standard visualization by Joachim
        #color ligands
        cmd.color('mylightblue',"%s and symbol c" % ligs1)

        cmd.color('mylightgreen',"%s and symbol c" % ligs2)

        #remove chains distant from the ligands
        cmd.remove("not (bychain (((%s | %s) around 12) | (%s | %s)))" % (ligs1,ligs2,ligs1,ligs2))


        cmd.show('sticks',ligs1)
        cmd.show('sticks',ligs2)
        cmd.show('nb_spheres',ligs1)
        cmd.show('nb_spheres',ligs2)

        #show residues around
        don = 'don'
        acc = 'acc'
        cmd.h_add(pdb1)
        cmd.h_add(pdb2)
        cmd.select(don, "(elem n,o and (neighbor hydro))")
        cmd.select(acc, "(elem o or (elem n and not (neighbor hydro)))")
        cmd.remove("(%s) and hydro" %pdb1)
        cmd.remove("(%s) and hydro" %pdb2)

        tmpres1 = 'tmpres1'
        tmpres2 = 'tmpres2'
        cmd.select(tmpres1, "(byobj %s) and (%s or %s) and ((%s and (%s or %s)) around 3.5) and (not %s)" % (ligs1,don,acc,ligs1,acc,don,ligs1) )
        cmd.select(tmpres2, "(byobj %s) and (%s or %s) and ((%s and (%s or %s)) around 3.5) and (not %s)" % (ligs2,don,acc,ligs2,acc,don,ligs2) )
        cmd.select(tmpres1, "%s or (((%s and (resn O or resn HOH or resn OH)) around 3.5) and (%s or %s) and (byobj %s) and not %s)" % (tmpres1,tmpres1,acc,don,ligs1,ligs1) )
        cmd.select(tmpres2, "%s or (((%s and (resn O or resn HOH or resn OH)) around 3.5) and (%s or %s) and (byobj %s) and not %s)" % (tmpres2,tmpres2,acc,don,ligs2,ligs2) )
        cmd.select(tmpres1, "%s or (((%s and (resn O or resn HOH or resn OH)) around 3.5) and (%s or %s) and (byobj %s) and not %s)" % (tmpres1,tmpres1,acc,don,ligs1,ligs1) )
        cmd.select(tmpres2, "%s or (((%s and (resn O or resn HOH or resn OH)) around 3.5) and (%s or %s) and (byobj %s) and not %s)" % (tmpres2,tmpres2,acc,don,ligs2,ligs2) )

        #vizualize polar contacts
        cmd.dist("%s_contacts" % pdb1,"(byobj %s) and not %s" % (ligs1,ligs1),"%s or (%s and (resn O or resn HOH or resn OH))" % (ligs1,tmpres1),quiet=1,mode=2,label=0,reset=1)
        cmd.dist("%s_contacts" % pdb2,"(byobj %s) and not %s" % (ligs2,ligs2),"%s or (%s and (resn O or resn HOH or resn OH))" % (ligs2,tmpres2),quiet=1,mode=2,label=0,reset=1)


        res1 = "res_%s" % ligs1
        res2 = "res_%s" % ligs2

        cmd.create(res1,"byresi %s and not hydro" % tmpres1,zoom=0) #object with residues
        cmd.create(res2,"byresi %s and not hydro" % tmpres2,zoom=0) #object with residues
        cmd.color('myblue',"%s and symbol c" % res1)
        cmd.color('mygreen',"%s and symbol c" % res2)
        cmd.hide('everything',res1)
        cmd.hide('everything',res2)
        cmd.show('sticks',res1)
        cmd.show('sticks',res2)
        cmd.show('nb_spheres',res1)
        cmd.show('nb_spheres',res2)
        cmd.show('cartoon')
        #cmd.delete(tmpres1)
        #cmd.delete(tmpres2)
        #cmd.delete(acc)
        #cmd.delete(don)


    # Set view
    cmd.zoom("%s around 10" % (ligs1))
    cmd.clip('slab', 1000)
    sys.stdout.write("...done\n")
    sys.stdout.flush()


def object_exists(object_name):
    """Checks if an object exists in the open PyMOL session."""
    return object_name in cmd.get_names("objects")

def probis_load_example(pdb1, pdb2, het1, het2, deleteall=True):
    """
    DESCRIPTION

        Loads a ProBiS aligend pair from the local files and
        decorates the ligand.

    USAGE

        probis_load_example PDB1, PDB2, HET1, HET2 [,False]

    ARGUMENTS

        PDB1    -- Query
        PDB2    -- Target
        HET1    -- Hetero ID for Query
        HET2    -- Hetero ID for Target
        [False] -- don't delete all objects before starting
    """

    if deleteall:
        cmd.delete("all")

    #init PDB stuff
    pdb1 = pdb1.upper()
    pdb2 = pdb2.upper()
    ids = pdb.IDs() #PDB ID management constructor

    #find the alignment file
    #

    def _dist(obj, sele1, sele2):
        #        return cmd.distance(obj, sele1, sele2)
        stored.pos = []
        cmd.iterate_state(1, sele1,'stored.pos.append((x,y,z))')
        coords1 = stored.pos
        stored.pos = []
        cmd.iterate_state(1, sele2,'stored.pos.append((x,y,z))')
        coords2 = stored.pos
        l = [ sum( (a-b)**2 for a, b in zip(coord1, coord2) ) for coord1 in coords1 for coord2 in coords2 ]
        l.sort()
        l = l[:int(len(l)/3)]
        l = [math.sqrt(tmp) for tmp in l]
        return sum(l)/len(l)

    def open_files(pdb1,pdb2):
        #open the transformed query and the target pdb file
        #if they don't exist return an error
        f1 = ids.getPDBfile(pdb.getPDBid(pdb1))
        f2 = ids.getPDBfile(pdb.getPDBid(pdb2))
        return [f1,f2]

    #     rev = pdb1 > pdb2 # to reverse or not reverse...
    #     pdbs = [pdb1,pdb2]
    #     if rev:
    #         pdbs.reverse()
    #     pdbs = tuple(pdbs)
    #     pdb1, pdb2 = pdbs
    files = open_files(pdb1, pdb2)
    #
    #     if rev:
    #         files.reverse()
    f1,f2 = files

    #load files

    pdb1 = pdb1.lower().strip()
    pdb2 = pdb2.lower().strip()
    #     print pdb1,pdb2


    #fresh pdb files
    cmd.load(f1,pdb1)
    cmd.load(f2,pdb2)

    #transform the fresh to the aligned

    #     f = open('/home/bioinf/joachimh/Documents/interfacesim/benchmark/probis/evaluation/stats/aligned_ligs/examples/transformations.csv','r')
    #     f = open('/home/bioinf/joachimh/Documents/interfacesim/benchmark/probis/alltoall/examples/transformations.csv')
    f = open('/home/bioinf/joachimh/Documents/interfacesim/benchmark/probis/alltoall/examples/viagra/transformations.csv')
    f.next()
    transi = None
    transj = None
    for line in f:
        line = line.split('\t')
        i = line[0].split(':')[0]
        j = line[1].split(':')[0]
        #         print i,j
        if i.lower() == pdb1 and j.lower() == pdb2:
            transi = line[2]
            transj = line[3].strip()
            break
        elif j.lower() == pdb1 and i.lower() == pdb2:
            transj = line[2]
            transi = line[3].strip()
            break
    f.close()
    if transi == None:
        print 'Transformation not found.'
        sys.exit()
    transi, roti = json.loads(transi)
    transj, rotj = json.loads(transj)
    roti = flatten(roti)
    rotj = flatten(rotj)
    pdb1_tmp = "%s_tmp" % pdb1
    pdb2_tmp = "%s_tmp" % pdb2
    cmd.create(pdb1_tmp, pdb1)
    cmd.create(pdb2_tmp, pdb2)

    cmd.transform_selection(pdb1,
       [roti[0], roti[1], roti[2], transi[0],
        roti[3], roti[4], roti[5], transi[1],
        roti[6], roti[7], roti[8], transi[2],
        0, 0, 0, 1], homogenous=1)
    cmd.transform_selection(pdb2_tmp,
       [roti[0], roti[1], roti[2], transi[0],
        roti[3], roti[4], roti[5], transi[1],
        roti[6], roti[7], roti[8], transi[2],
        0, 0, 0, 1], homogenous=1)
    cmd.transform_selection(pdb2,
       [rotj[0], rotj[1], rotj[2], transj[0],
        rotj[3], rotj[4], rotj[5], transj[1],
        rotj[6], rotj[7], rotj[8], transj[2],
        0, 0, 0, 1], homogenous=1)
    cmd.transform_selection(pdb1_tmp,
       [rotj[0], rotj[1], rotj[2], transj[0],
        rotj[3], rotj[4], rotj[5], transj[1],
        rotj[6], rotj[7], rotj[8], transj[2],
        0, 0, 0, 1], homogenous=1)

    cmd.hide('everything')

    def _select_transform(pdb1, pdb2, het1, het2):
        #get the HET IDs and identify the ligands
        ligs1 = "ligand_%s" % pdb1
        cmd.select(ligs1,"none")
        ligs2 = "ligand_%s" % pdb2
        cmd.select(ligs2,"none")

    #     print ligs1, ligs2, [het1,het2]
        for het in [het1,het2]:
            cmd.select(ligs1,"%s | (%s and resn %s)" % (ligs1, pdb1, het))
            cmd.select(ligs2,"%s | (%s and resn %s)" % (ligs2, pdb2, het))

        #get the two ligands, which are closest together
        ligs1resi = []
        myspace = {'ligs1resi': ligs1resi}
        cmd.iterate(ligs1,"ligs1resi.append((resi,chain))", space=myspace)
        ligs1resi = set(ligs1resi)
        ligs2resi = []
        myspace = {'ligs2resi': ligs2resi}
        cmd.iterate(ligs2,"ligs2resi.append((resi,chain))", space=myspace)
        ligs2resi = set(ligs2resi)

        dists = []
        tmp = list(ligs1resi)
        while len(tmp) > 0:
            resi1,chain1 = tmp.pop()
            for resi2,chain2 in ligs2resi:
                dist = _dist('tmp', '%s and resi %s and chain %s' % (ligs1,resi1,chain1), '%s and resi %s and chain %s' % (ligs2,resi2,chain2))
                dists.append( (dist,resi1,resi2,chain1,chain2) )
        cmd.delete('tmp')
        dists.sort(key = lambda x:x[0])
        dist, resi1, resi2, chain1, chain2 = dists[0]

        return dist, ligs1, ligs2, resi1, resi2, chain1, chain2

    first_transform = _select_transform(pdb1, pdb2, het1, het2)
    second_transform = _select_transform(pdb1_tmp, pdb2_tmp, het1, het2)

    if first_transform[0] > second_transform[0]:
        #second transformation is better
        dist, ligs1, ligs2, resi1, resi2, chain1, chain2 = second_transform
        cmd.delete(pdb1)
        cmd.delete(pdb2)
        cmd.set_name(pdb1_tmp, pdb1)
        cmd.set_name(pdb2_tmp, pdb2)
        print "Used inverse rotation scheme. Diff in dist is %.2f" % first_transform[0] - second_transform[0]
    else:
        dist, ligs1, ligs2, resi1, resi2, chain1, chain2 = first_transform
        cmd.delete(pdb1_tmp)
        cmd.delete(pdb2_tmp)

    #color C_alphas
    cmd.color('myblue', "%s and symbol c" % pdb1)
    cmd.color('mygreen', "%s and symbol c" % pdb2)

    #color ligands
    cmd.color('mylightblue',"%s and symbol c" % ligs1)
    cmd.color('mylightgreen',"%s and symbol c" % ligs2)

    cmd.select(ligs1,"%s and resi %s and chain %s" % (ligs1, resi1, chain1))
    cmd.select(ligs2,"%s and resi %s and chain %s" % (ligs2, resi2, chain2))

    #remove chains distant from the ligands
    cmd.remove("not (bychain (((%s | %s) around 12) | (%s | %s)))" % (ligs1,ligs2,ligs1,ligs2))


    cmd.show('sticks',ligs1)
    cmd.show('sticks',ligs2)
    cmd.show('nb_spheres',ligs1)
    cmd.show('nb_spheres',ligs2)

    #show residues around
    don = 'don'
    acc = 'acc'
    cmd.h_add(pdb1)
    cmd.h_add(pdb2)
    cmd.select(don, "(elem n,o and (neighbor hydro))")
    cmd.select(acc, "(elem o or (elem n and not (neighbor hydro)))")
    cmd.remove("(%s) and hydro" %pdb1)
    cmd.remove("(%s) and hydro" %pdb2)

    tmpres1 = 'tmpres1'
    tmpres2 = 'tmpres2'
    cmd.select(tmpres1, "(byobj %s) and (%s or %s) and ((%s and (%s or %s)) around 3.5) and (not %s)" % (ligs1,don,acc,ligs1,acc,don,ligs1) )
    cmd.select(tmpres2, "(byobj %s) and (%s or %s) and ((%s and (%s or %s)) around 3.5) and (not %s)" % (ligs2,don,acc,ligs2,acc,don,ligs2) )
    cmd.select(tmpres1, "%s or (((%s and (resn O or resn HOH or resn OH)) around 3.5) and (%s or %s) and (byobj %s) and not %s)" % (tmpres1,tmpres1,acc,don,ligs1,ligs1) )
    cmd.select(tmpres2, "%s or (((%s and (resn O or resn HOH or resn OH)) around 3.5) and (%s or %s) and (byobj %s) and not %s)" % (tmpres2,tmpres2,acc,don,ligs2,ligs2) )
    cmd.select(tmpres1, "%s or (((%s and (resn O or resn HOH or resn OH)) around 3.5) and (%s or %s) and (byobj %s) and not %s)" % (tmpres1,tmpres1,acc,don,ligs1,ligs1) )
    cmd.select(tmpres2, "%s or (((%s and (resn O or resn HOH or resn OH)) around 3.5) and (%s or %s) and (byobj %s) and not %s)" % (tmpres2,tmpres2,acc,don,ligs2,ligs2) )

    #vizualize polar contacts
    cmd.dist("%s_contacts" % pdb1,"(byobj %s) and not %s" % (ligs1,ligs1),"%s or (%s and (resn O or resn HOH or resn OH))" % (ligs1,tmpres1),quiet=1,mode=2,label=0,reset=1)
    cmd.dist("%s_contacts" % pdb2,"(byobj %s) and not %s" % (ligs2,ligs2),"%s or (%s and (resn O or resn HOH or resn OH))" % (ligs2,tmpres2),quiet=1,mode=2,label=0,reset=1)


    res1 = "res_%s" % ligs1
    res2 = "res_%s" % ligs2

    cmd.create(res1,"byresi %s and not hydro" % tmpres1,zoom=0) #object with residues
    cmd.create(res2,"byresi %s and not hydro" % tmpres2,zoom=0) #object with residues
    cmd.color('myblue',"%s and symbol c" % res1)
    cmd.color('mygreen',"%s and symbol c" % res2)
    cmd.hide('everything',res1)
    cmd.hide('everything',res2)
    cmd.show('sticks',res1)
    cmd.show('sticks',res2)
    cmd.show('nb_spheres',res1)
    cmd.show('nb_spheres',res2)
    #cmd.delete(tmpres1)
    #cmd.delete(tmpres2)
    #cmd.delete(acc)
    #cmd.delete(don)




    cmd.zoom("%s around 10" % (ligs1))
    cmd.clip('slab', 1000)

cmd.extend('probis_load_example', probis_load_example)

global cluster
cluster = None
scops = None

def clusterloaderligandpair(ligidtuple):
    return clusterloaderid(ligidtuple, find_clust_rep=False)


def clusterloaderid(ligidtuple, find_clust_rep=True, plip=True, split=False):
    #p = '[(A-Z0-9]+_([a-zA-Z0-9]{4}):([a-zA-Z0-9]{1,2})_[0-9]+[, ]+[(A-Z0-9]+_([a-zA-Z0-9]{4}):([a-zA-Z0-9]{1,2})_[0-9]+'
    p = '[(]{0,1}([A-Z0-9]+)_([a-zA-Z0-9]{4}):([a-zA-Z0-9]{1,2})_[0-9]+[, ]+([A-Z0-9]+)_([a-zA-Z0-9]{4}):([a-zA-Z0-9]{1,2})_[0-9]+'
    het1, pdb1, chain1, het2, pdb2, chain2 = re.findall(p,ligidtuple)[0]
    return clusterloader(pdb1, chain1, pdb2, chain2, het1=het1, het2=het2,find_clust_rep=find_clust_rep, plip=plip)


def clusterloader(pdb1, chain1, pdb2, chain2, het1=None, het2=None, find_clust_rep=True, plip=True, split=False):
    global cluster
    cluster = ClusterLoader(pdb1, chain1, pdb2, chain2, het1=het1, het2=het2, find_clust_rep=find_clust_rep, plip=plip, split=split)

def clusternext():
    global cluster
    cluster.next()

def clusterget(nr):
    global cluster
    cluster.get(nr)

def clustersave():
    global cluster
    cluster.save()

def scopset(scop1, scop2):
    global scops
    if scops is None:
        scops = SCOPloader()
    return scops.set(scop1, scop2)

def scopnext():
    global scops
    return scops.next()


cmd.extend('clusterloader', clusterloader)
cmd.extend('clusterloaderid', clusterloaderid)
cmd.extend('clusterloaderligandpair', clusterloaderligandpair)
cmd.extend('next', clusternext)
cmd.extend('get', clusterget)
cmd.extend('save',clustersave)

cmd.extend('setscops', scopset)
cmd.extend('nextscops', scopnext)

if __name__ == '__main__':
    pass
