# The MIT License (MIT)
#
# Copyright (c) 2016 Joachim Haupt (TU Dresden / Germany)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from __future__ import division

import math
import pybel
import os, sys
import numpy
import json
import urllib, urllib2
import redis
import random
import cPickle as pickle
import rdkit.Chem
import rdkit.Chem.rdMolAlign

from chemspipy import ChemSpider
from kendrew.toolchain.files import *
from kendrew.toolchain.basic import RedirectStdNull
from kendrew.toolchain.Types import NormTuple, CountedDict, RedisStore
from kendrew.toolchain.errors import getTraceback
from kendrew.toolchain.checktypes import forcemoliterable
from kendrew.toolchain.errors import LigandRMSDError
from kendrew.REST.Molecules import LigandRMSDjson as LigandRMSDjsonREST
from kendrew.toolchain.mp import parallel_fn

from shutil import rmtree

from time import sleep

#import rdkit.RDLogger
#logger = rdkit.RDLogger.logger()
#logger.setLevel(99)

def center(a,b):
    """Returns the center of two points a and b."""
    return  tuple([min((fa,fb)) + math.fabs(fa-fb)/2 for fa,fb in zip(a,b)])


def dist(a,b):
    """Returns the Euclidean distance of two points a and b."""
    return math.sqrt(sum( (ta-tb)**2 for ta, tb in zip(a, b) ))

def centroid(mol):
    """Computes the simplified centroid of a molecule. A tuple of
    (centroid point, distance to the centroid, centroid Pybel mol) is returned."""
    coords = [ (atom.GetX(), atom.GetY(), atom.GetZ()) for atom in pybel.ob.OBMolAtomIter(mol.OBMol) ]
    x = [tmp[0] for tmp in coords]
    y = [tmp[1] for tmp in coords]
    z = [tmp[2] for tmp in coords]
    del coords
    a = (min(x), min(y), min(z))
    h = (max(x), max(y), max(z))
    cent = center(a,h)
    tmpmol = pybel.readstring('smi', "C")
    atom = pybel.ob.OBMolAtomIter(tmpmol.OBMol).next()
    x,y,z = cent
    atom.SetVector(x,y,z)
    return (cent, dist(a,h)/2, tmpmol)

def rmsd(coordpairs):
    """Compute the RMSD for a list of pairs of 3-tuples."""
    def squared_distance(coordsA, coordsB):
        sqrdist = sum( (a-b)**2 for a, b in zip(coordsA, coordsB) )
        return sqrdist
    deviation = sum(squared_distance(atomA, atomB) for
                    (atomA, atomB) in coordpairs)

    return math.sqrt(deviation / float(len(coordpairs)))

def transform(tmpmol,translation, rotation):
    ilig = copymol(tmpmol) #copy
    for atom in pybel.ob.OBMolAtomIter(ilig.OBMol):
        vec = transform_coordinate([ atom.GetX(), atom.GetY(), atom.GetZ() ], translation, rotation)
        x,y,z = [round(i,4) for i in vec]
        atom.SetVector(x,y,z)
    return ilig

def transform_json(tmpmol, trans_json):
    """Transform the given molecule using a JSON formatted string containing
       a translation vector and rotation matrix."""
    translation,rotation = json.loads(trans_json)
    translation = numpy.array(translation)
    rotation = numpy.matrix(rotation)
    rotation.shape = (3,3)
    return transform(tmpmol, translation, rotation)


def transform_coordinate(xyz_vec, translation, rotation, flat=False):
    """Transform the coordinates of a single point with a given translation and rotation."""
    vec = numpy.array(xyz_vec)
    vec = numpy.dot(rotation, vec.reshape(3,1)) + translation.reshape(3,1)
    vec = numpy.array(vec)
    if not flat:
        return vec
    else:
        return tuple(map(float, vec)) # Return coordinates as flat tuple


def transform_coordinate_json(xyz_vec, trans_json, flat=False):
    """Transform the coordinates of a single point with a given translation/rotation in json format."""
    translation,rotation = json.loads(trans_json)
    return transform_coordinate(xyz_vec, translation, rotation, flat)

def mindist(mol1, mol2):
    """Returns the min distance between any two atoms from mol1 and mol2."""
    coords1 = (atom1.coords for atom1 in mol1 if not atom1.OBAtom.IsHydrogen())
    coords2 = (atom2.coords for atom2 in mol2 if not atom2.OBAtom.IsHydrogen())
    l = [ sum( (a-b)**2 for a, b in zip(coord1, coord2) ) for coord1 in coords1 for coord2 in coords2 ]
    l.append(float('inf'))
    return math.sqrt(min(l))

def _computeForIsoStore(StdMolStr1, StdMolStr2, verbose=False):
    mol1, mol2 = [\
        standardizedMol(mol, fromCANstring=True)\
        for mol in [StdMolStr1, StdMolStr2]\
        ]
    iso = isomorphism(mol1, mol2, verbose=verbose)
    success = iso.findIso()
    if not success:
        success = iso.findMCS() #do substructure search if there is not isomorphism
    return success, set( tmpiso for tmpiso in iso.isomorphs )

def _loadFromIsoStore(isostore, key, mol1, mol2, verbose=False):
    iso_isomorphs = set()
    try:
        tmpmols = isostore.getMols(key)
    except KeyError, e:
        if verbose:
            print '  failed to load molecules from store.'
        return False, iso_isomorphs

    #get the mapping from the first molecule to
    iso1 = isomorphism(mol1, tmpmols[0])
    storediso = iso1.findIso(findall=False)
    iso2 = isomorphism(tmpmols[1],mol2)
    storediso = storediso and iso2.findIso(findall=False)
    if storediso:
        try:
            mapping1 = dict(iter(iso1.isomorphs).next())
        except StopIteration, e:
            print 'query mol:         ', mol1.write('can').strip()
            print 'query mol in store:', tmpmols[0].write('can').strip()
            print 'store key', key
            print 'target mol:         ', mol2.write('can').strip()
            print 'target mol in store:', tmpmols[1].write('can').strip()
            print 'iso:', iso1.isomorphs
            raise e
        try:
            mapping2 = dict(iter(iso2.isomorphs).next())
        except StopIteration, e:
            print 'iso:', iso2.isomorphs
            print 'target mol:         ', mol2.write('can').strip()
            print 'target mol in store:', tmpmols[1].write('can').strip()
            print 'store key', key
            print 'query mol:         ', mol1.write('can').strip()
            print 'query mol in store:', tmpmols[0].write('can').strip()
            raise e
        try:
            isopairs = isostore[(mol1, mol2)]
            iso_isomorphs = set(
                tuple([(mapping1[x],mapping2[y]) for x,y in pairs]) for pairs in isopairs
                )
        except KeyError:
            if verbose:
                print '  Not in isostore: %s' % str(key)
            storediso = False

    if verbose and not storediso:
        print "  Warning: loading isomorphism for %s and %s not successful!" % (mol1.write('can').strip(), mol2.write('can').strip())

    return storediso, iso_isomorphs

def ligandRMSDstring( molstr1_molstr2_tuple, mol1format='smi', mol2format='smi', isostore=None, usePersistentStore=False, getMols=False, getMatchMol=False, ResultMolFormat='mol2', storefilesinwd=False, verbose=False):
    mol1, mol2 = molstr1_molstr2_tuple
    mol1 = pybel.readstring(str(mol1format), str(mol1))
    mol2 = pybel.readstring(str(mol2format), str(mol2))
    return ligandRMSD(mol1, mol2, isostore=isostore, usePersistentStore=usePersistentStore, getMols=getMols, getMatchMol=getMatchMol, ResultMolFormat=ResultMolFormat, storefilesinwd=storefilesinwd, verbose=verbose)

def ligandRMSD(mol1, mol2, isostore=None, usePersistentStore=False, getMols=False, getMatchMol=False, ResultMolFormat=None, storefilesinwd=False, verbose=False):
    """Returns the LigandRMSD of two molecules. The MCS is used if the mols are not identical."""
    inf = float('inf')
    storediso = False
    if storefilesinwd:
        iso = isomorphism(mol1,mol2,outdir=os.getcwd())
    else:
        iso = isomorphism(mol1,mol2)

    # only use the stored isomorphism if the molecules are non-identical
    useIsoStore = cansmi(mol1, stripH = True, ignoreStereo = True) != cansmi(mol2, stripH = True, ignoreStereo = True)
    # create persistant IsoStore if needed
    if useIsoStore and usePersistentStore and isostore is None:
        isostore = IsoStorePersistent(verbose=verbose)

    if useIsoStore and isostore is not None:
        #check whether this mapping was already stored
        key = isostore.getKey((mol1,mol2))

        storediso = key in isostore
        if not storediso: # create new mapping
            success = iso.findIso()
            if not success:
                success = iso.findMCS() #do substructure search if there is no isomorphism
            storediso = success
            tmpisomorphs = set( tmpiso for tmpiso in iso.isomorphs )

            if storediso:
                isostore[(mol1, mol2)] = tmpisomorphs
        elif storediso: # load mapping
            storediso, iso.isomorphs = _loadFromIsoStore(isostore, key, mol1, mol2, verbose=verbose)

    if not storediso:
        success = iso.findIso()
        if not success:
            success = iso.findMCS() #do substructure search if there is no isomorphism
        if success and useIsoStore and isostore is not None:
            # the specific molecule pair is not in the store
            tmpisomorphs = set( tmpiso for tmpiso in iso.isomorphs )
            isostore[(mol1, mol2)] = tmpisomorphs

    results = iso.align_iso() #rmsd without transformation
    rmsd = results['rmsd']
    ratio = results['matchedAtoms']/min(results['qAtoms'], results['tAtoms'])
    if not rmsd == inf:
        diff = max(0,rmsd - iso.align_mobile()['rmsd']) #rmsd difference (static RMSD - RMSD with transformation (min RMSD of the two mols))
    else:
        diff = inf
        mobile_align = {}
    tmp = {'LigandRMSD':diff, 'LigandRMSD_raw':rmsd, 'LigandRMSDratio':ratio, 'matchedAtoms':results['matchedAtoms']}
    if ratio < 1:
        mol = iso.getMatchMol()
        if mol is not None:
            tmp['LigandRMSDmatch'] = cansmi(mol).split()[0]

    def _convertOutMol(mol):
        if ResultMolFormat is None:
            return mol
        else:
            return mol.write(str(ResultMolFormat))
    if getMols:
        tmp['qmol_opt'], tmp['tmol_opt'] = _convertOutMol(iso.getTransformedStructures())
        tmp['qmol_match'], tmp['tmol_match'] = _convertOutMol(iso.getMatchMols())
    elif getMatchMol:
        tmp['qmol_match'] = _convertOutMol(iso.getMatchMol(tmol=True))
    return tmp

def stripmol(pybelmolecule, stripSalts=False, stripFormalCharges=False):
    """Removes stereo information, hydrogens and converts all bonds to single
       bonds. A stripped copy of the input molecule is returned."""
    mol = copymol(pybelmolecule)
    if stripFormalCharges:
        for atom in pybel.ob.OBMolAtomIter(mol):
            atom.SetFormalCharge(0)
            if atom.GetType() == 'Fe':  # remove irons
                mol.DeleteAtom(atom)
    if stripSalts:
        mol.OBMol.StripSalts()
    mol.OBMol.DeleteHydrogens()
    mol.OBMol.DeleteData(pybel.ob.StereoData)
    for bond in pybel.ob.OBMolBondIter(mol.OBMol):
        if bond.GetBondOrder() != 1:
            bond.SetBondOrder(1)
#        bond.UnsetHash()
#        bond.UnsetWedge()
#        bond.UnsetUp()
#        bond.UnsetDown()
#        bond.UnsetAromatic()
#        bond.UnsetKekule()
#        for data in bond.GetData():
#            bond.DeleteData(data)
    #mol = pybel.Molecule(pybel.ob.OBMol(mol))
    return mol

def obconvert(pybelmolecule, outputformat='can', ignoreStereo = True, stripH = False):
    """Returns the canonical SMILES of a molecule without stereo info. Hydrogens are stripped upon request."""
    obc = pybel.ob.OBConversion()
    obc.SetOutFormat(outputformat)
    if ignoreStereo:
        obc.AddOption('i') #ignore stereo info
    if stripH:
        obc.AddOption('DeletePolarH')
        obc.AddOption('DeleteNonPolarH')
    obc.AddOption('n') #don't write the name to the output SMILES
    return obc.WriteString(pybelmolecule.OBMol).strip()

def cansmi(pybelmolecule, ignoreStereo = True, stripH = False):
    """Returns the canonical SMILES of a molecule without stereo info. Hydrogens are stripped upon request."""
    return obconvert(pybelmolecule, outputformat='can', ignoreStereo = ignoreStereo, stripH = stripH)

def inchikey(pybelmolecule, ignoreStereo = True, stripH = False):
    """Returns the InChI key of a molecule without stereo info. Hydrogens are stripped upon request."""
    obc = pybel.ob.OBConversion()
    obc.SetOutFormat("inchikey")
    if ignoreStereo:
        obc.AddOption('i') #ignore stereo info
    if stripH:
        obc.AddOption('DeletePolarH')
        obc.AddOption('DeleteNonPolarH')
    obc.AddOption('n') #don't write the name to the output SMILES
    return obc.WriteString(pybelmolecule.OBMol).strip()


def roundmol(pybelmolecule, places = 4):
    """Rounds the coordinates of a given Pybel molecule to the number of given
       decimal places."""
    for atom in pybelmolecule:
        x,y,z = [round(tmp,places) for tmp in atom.coords]
        atom.OBAtom.SetVector(x,y,z)
    return pybelmolecule

def copymol(pybelmolecule):
    return pybel.Molecule(pybel.ob.OBMol(pybelmolecule.OBMol))

def countAtoms(pybelmolecule):
    """Counts all non-hydrogen atoms."""
    return sum(1 for atom in pybelmolecule if not atom.OBAtom.IsHydrogen())

def readmol(format='mol', path=None, options=()):
    """Reads the given molecule file and returns the corresponding Pybel molecule.
    In contrast to the standard Pybel implementation, the file is closed properly."""
    obc = pybel.ob.OBConversion()
    obc.SetInFormat(format)
    for option in options:
        obc.AddOption(option)
    mol = pybel.ob.OBMol()
    f = read(path)
    obc.ReadString(mol, str(f.read()))
    f.close()
    return pybel.Molecule(mol)

def standardizedMol(pybelmolecule, getMolString=False, fromCANstring=False):
    """Returns a standardized molecule with same connectivity and bond order. Coordinates and Conformation might be lost. If getMolString is set to True, a string will be returned. If fromCANstring is True, a string produced by this function is expected as input."""
    if fromCANstring:
        mol = pybelmolecule
    else:
        mol = obconvert(pybelmolecule, outputformat='mol2', stripH=False, ignoreStereo=False)
    if not getMolString:
        mol = pybel.readstring('mol2', mol)
    return mol

def getIUPACname(smiles):
    """Returns the IUPAC name of a given SMILES string using the CACTUS web service."""
    url = "http://cactus.nci.nih.gov/chemical/structure/%s/iupac_name" % (urllib.quote_plus(smiles))
    req = urllib2.Request(url)
    try:
        res = urllib2.urlopen(req)
    except urllib2.HTTPError:
        # try ChemSpider
        cs = ChemSpider("9b5af2e7-2feb-4d5f-92d0-5de053f13c73")
        try:
            return iter(cs.search(smiles)).next().common_name
        except StopIteration:
            return None
    return res.read().strip()

class isomorphism(object):
    """Computes all possible isomorphisms of two molecules among each other. Hydrogens are ignored."""

    def __init__(self, querymol, targetmol, outdir=None, verbose=False):

        self.verbose = verbose

        self.outdir = outdir

        if not self.verbose:
            pybel.ob.obErrorLog.SetOutputLevel(-1)
            pybel.ob.obErrorLog.StopLogging()

        #sort according to number of non hydrogen atoms
        #mols.sort(key = lambda mol: sum(1 for atom in mol if not atom.OBAtom.IsHydrogen()) )

        self.origqmol = querymol
        self.origtmol = targetmol

        self.qmol = stripmol(self.origqmol, stripSalts=True) #stripped copy of the query
        self.tmol = stripmol(self.origtmol, stripSalts=True)

        # preprocess the molecules
        self.qmolcoords = [atom.coords for atom in self.qmol]
        self.lookupqmol = range(len(self.qmolcoords))
        self.tmolcoords = [atom.coords for atom in self.tmol]
        self.lookuptmol = range(len(self.tmolcoords))

        self.isomorphs = set() #all isomorphisms found
        self.mcsmol = None #Maximum common subgraph of the two mols as mol
        self.mcsmols = None

        self.mcsatoms = None
        self.qatoms =  countAtoms(self.origqmol)
        self.tatoms = countAtoms(self.origtmol)

        self.isomatch = None #isomorphism returning the best match

        self.mcssmi = None
        self.rotMat = None #rotation matrix for the query mol
        self.translation = None

        self.transqmol = None



    def findIso(self, findall=True):
        """Finds all isomorphisms between the target and the query mol. Returns
           true iff successful. By default, all isomorphisms are returned."""

        query = pybel.ob.CompileMoleculeQuery(self.qmol.OBMol)
        mapper = pybel.ob.OBIsomorphismMapper.GetInstance(query)
        if findall:
            # get all isomorphisms between the molecules
            isomorphs = pybel.ob.vvpairUIntUInt()
            mapper.MapAll(self.tmol.OBMol, isomorphs)
        else:
            # only returns one isomorphism
            isomorphs = pybel.ob.vpairUIntUInt()
            mapper.MapFirst(self.tmol.OBMol, isomorphs)
            isomorphs = [isomorphs]

        # store isomorphisms
        tmpisos = [tuple(sorted(isomorph)) for isomorph in isomorphs]
        self.isomorphs.update(tmpisos)
        self.mcsatoms = self.qatoms

        return (len(isomorphs) > 0)


    def findMCS(self):
        """Compute the maximum common subgraph of the two molecules and find substructure isomorphisms."""

        sg = MCS(self.qmol, self.tmol, outdir=self.outdir, verbose=self.verbose) #compute mcs
        success = sg.find()
        self.mcsmols = sg.getMols() # get all max substructures
        self.isomorphs.update(sg.getMappings())
        if self.verbose:
            print 'Number of all found isomorphisms:', len(self.isomorphs)

        # remove non maximum substructures
        maxlen = 0
        for iso in self.isomorphs:
            maxlen = max(maxlen, len(iso))
        tmpi = set(self.isomorphs)
        for iso in tmpi:
            if len(iso) < maxlen:
                self.isomorphs.remove(iso)
        if self.verbose:
            print 'Number of maximal isomorphisms:', len(self.isomorphs)

        success = len(sg.getMappings()) > 0

        if success:
            self.mcsatoms = sum(1 for atom in self.mcsmols[0])
        else:
            self.mcsatoms = 0
        if self.verbose:
            print "Atom counts query/mcs/target:", self.qatoms, self.mcsatoms, self.tatoms

        return success


    def align_iso(self):
        """Compute the RMSD between two molecules. The minimum RMSD of all longest mappings is returned."""
        #print self.qmol.title, self.tmol.title, len(self.isomorphs)
        #compute the rmsd
        minrmsd = float('inf')
        isomatch = None

        rmsds = []
        #matoms = [0]
        matoms = []
        matom = 0
        isomatchs = []

        for isomorph in self.isomorphs:
            #isomorph_coords = [None] * len(isomorph)
            coord_pairs = []
            for i in range(len(isomorph)):
                x, y = isomorph[i]
                coord_pairs.append( (self.qmolcoords[self.lookupqmol.index(x)], self.tmolcoords[self.lookuptmol.index(y)]) )
                #isomorph_coords[self.lookupqmol.index(x)] = self.tmolcoords[self.lookuptmol.index(y)]
            mapping_rmsd = rmsd(coord_pairs)

            rmsds.append(mapping_rmsd)
            matoms.append(len(coord_pairs))
            isomatchs.append(isomorph)

        #determine the longest match with min rmsd
        #matom = max(matoms)
        for i, tmprmsd in enumerate(rmsds):
            #print " ", tmprmsd, tmprmsd < minrmsd, matom, matoms[i+1]
            if tmprmsd < minrmsd:# and matom == matoms[i+1]:
                minrmsd = tmprmsd
                isomatch = isomatchs[i]
                matom = matoms[i]

        self.isomatch = isomatch
        #print '  returns', minrmsd, matom
        return {'rmsd':minrmsd, 'matchedAtoms':matom, 'qAtoms':self.qatoms, 'tAtoms':self.tatoms}

    def align_mobile(self):
        """Aligns the query to the target by performing rotations and translations, such that the RMSD is minimized. Only maximum atom matches are considered."""
        rmsds = []
        rotMats = []
        matoms = []
        isos = []

        if self.isomatch is not None: # if there was already a rigid alignment, use the best rigid for the mobile align
            isosearchspace = [self.isomatch]
        else:
            isosearchspace = self.isomorphs

        for iso in isosearchspace:
            # reduce both molecules to the common substructure
            qmol = copymol(self.qmol)
            tmol = copymol(self.tmol)
            qkeys = [q for q,t in iso]
            tkeys = [t for q,t in iso]

            # Checker for Redundant keys:
            # ===========================
            if len(qkeys) != len(set(qkeys)) or len(tkeys) != len(set(tkeys)):
                print "Error in redundant Key Check for query '%s' and target '%s'!" % (self.qmol.title, self.tmol.title)
                print '    Query Keys: %s' % qkeys
                print '    Target Keys: %s' % tkeys

            # ===========================

            qkeys = set(qkeys)
            tkeys = set(tkeys)
            dels = 0
            tmplist = [atom for atom in qmol]
            for i,atom in enumerate(tmplist):
                if i not in qkeys:
                    qmol.OBMol.DeleteAtom(atom.OBAtom)
                    dels+=1
            qatms = i+1 #number of atoms in query
            if self.verbose:
                print 'Deleted atoms in query:', dels
            dels = 0
            tmplist = [atom for atom in tmol]
            for i,atom in enumerate(tmplist):
                if i not in tkeys:
                    tmol.OBMol.DeleteAtom(atom.OBAtom)
                    dels+=1
            tatms = i+1
            if self.verbose:
                print 'Deleted atoms in target:', dels
            align = pybel.ob.OBAlign()
            align.SetRefMol(qmol.OBMol) # set the query molecule as ref
            align.SetTargetMol(tmol.OBMol) # tmol is aligned to the reference
            success = align.Align()
            if success:
                rmsds.append(align.GetRMSD())
                if self.verbose:
                    print 'ob ', align.GetRMSD()
                #save the rotation matrix
                tmpm = align.GetRotMatrix()
                numm = numpy.matrix(numpy.zeros((3,3),'d'))
                for i in range(3):
                    for j in range(3):
                        numm[i,j] = tmpm.Get(i,j)
                rotMats.append(numm)
                matoms.append(len(qkeys))
                isos.append(iso)

            #repeat the alignment with RDKit
            with RedirectStdNull():
                qmol = rdkit.Chem.MolFromMolBlock(qmol.write('mol'))
                tmol = rdkit.Chem.MolFromMolBlock(tmol.write('mol'))
            try:
                rmsd, mtrans = rdkit.Chem.rdMolAlign.GetAlignmentTransform(tmol,qmol)
                #rmsd = AllChem.GetBestRMS(qmol, tmol)
            except Exception,e:
                #print 'Alignment for %s and %s failed!' %(molname,amolname)
                #print e
                try:
                    rmsd, mtrans = rdkit.Chem.rdMolAlign.GetAlignmentTransform(qmol,tmol)
                except Exception,e:
                    #print 'Alignment for %s and %s failed!' %(molname,amolname)
                    #print e
                    if self.verbose:
                        print "Error: Alignment with RDkit failed."
                #print "%s-%s:%.3f" % (molname,amolname,rmsd)
                else:
                    rmsds.append(rmsd)
                    rotMats.append(numpy.matrix(mtrans[:3,:3]).transpose())
                    matoms.append(len(qkeys))
                    isos.append(iso)
                    if self.verbose:
                        print 'snd',rmsd
            #print "%s-%s:%.3f" % (molname,amolname,rmsd)
            else:
                rmsds.append(rmsd)
                rotMats.append(numpy.matrix(mtrans[:3,:3]))
                matoms.append(len(qkeys))
                isos.append(iso)
                if self.verbose:
                    print 'fst',rmsd

        minrmsd = float('inf')
        pos = None
        for i,rmsd in enumerate(rmsds):
            if minrmsd > rmsd:
                minrmsd = rmsd
                pos = i
        if pos is None:
            return {'rmsd':minrmsd, 'matchedAtoms':None, 'qAtoms':None, 'tAtoms':None}
        else:
            self.rotMat = rotMats[pos]
            if self.isomatch is None:
                self.isomatch = isos[pos]
            return { 'rmsd': rmsds[pos], 'matchedAtoms':matoms[pos], 'qAtoms':self.qatoms, 'tAtoms':self.tatoms }

    def getMatchMol(self, tmol=False):
        """Returns the matching submolecule as Pybel molecule."""

        if self.isomatch is None:
            return None
        if not tmol:
            mol = self.qmol
        else:
            mol = self.tmol
        mol = copymol(mol)
        if not tmol:
            keepatoms = set([x for x, y in self.isomatch]) # query atoms
        else:
            keepatoms = set([y for x, y in self.isomatch]) # target atoms
        atoms = [atom for atom in mol]

        for i,atom in enumerate(atoms):
            if i not in keepatoms:
                mol.OBMol.DeleteAtom(atom.OBAtom)
        return mol

    def getMatchMols(self):
        """Returns the matching submolecules of the query and target mol as Pybel molecule."""
        return (self.getMatchMol(), self.getMatchMol(tmol=True))

    def transformStructures(self):
        """Translates and rotates the query mol, corresponding to the min RMSD."""

        qmol = copymol(self.origqmol)
        #qmol = pybel.Molecule(pybel.ob.OBMol(self.qmol.OBMol))
        #tmol = pybel.Molecule(pybel.ob.OBMol(self.tmol.OBMol))

        # rotate the query mol
        for atom in pybel.ob.OBMolAtomIter(qmol.OBMol):
            #get atom coordinates
            vec = numpy.array([ atom.GetX(), atom.GetY(), atom.GetZ() ])
            vec = numpy.dot(vec,self.rotMat)
            vec = numpy.array(vec)[0]
            atom.SetVector(float(vec[0]), float(vec[1]), float(vec[2]))

        # get the translation vector
        qmolcoords = [atom.coords for atom in qmol]#retrieve the new coordinates of qmol
        q,t = self.isomatch[0] #a matching pair of atoms
        #translation vector
        self.translation = numpy.array(self.tmolcoords[t]) - numpy.array(qmolcoords[q])

        # translate the query molecule
        for atom in pybel.ob.OBMolAtomIter(qmol.OBMol):
            #get atom coordinates
            vec = numpy.array([ atom.GetX(), atom.GetY(), atom.GetZ() ])
            vec = vec + self.translation
            #round to 4 digets
            x,y,z = [round(i,4) for i in vec]
            atom.SetVector(x,y,z)

        self.transqmol = qmol

    def getTransformedStructures(self):
        """Returns the translated/rotated query mol, corresponding to the min RMSD."""
        if self.transqmol is None:
            self.transformStructures()
        return (self.transqmol, copymol(self.origtmol))

    def getTransformedQuery(self):
        return self.transqmol

class MCS(object):
    """Computes the maximum common subgraph of two molecules. Molecules have to be provided as Pybel Molecules."""

    def __init__(self, mol_1, mol_2, stripmols=True, debug=False, keeptemp=False, outdir=None, verbose=False):

        self.maxmem = 8589934592 # 8GB
        self.debug = debug #perform extra checks
        self.verbose = self.debug or verbose
        self.stripmols = stripmols

        if outdir is not None:
            self.dir = outdir
            self.keeptemp = True
        else:
            self.dir = tmpdir()
            self.keeptemp = keeptemp

        if self.keeptemp:
            self.verboseopts = "-g -d 2000x2000" #write a png of the mapping
        else:
            self.verboseopts = ''

        self.cwd = None

        self.subgmols = [] #all mc substructures
        self.mappings = set() #all found mappings

        self.origqmol = copymol(mol_1)
        self.origtmol = copymol(mol_2)

        if self.stripmols:
            # copy mols and strip them
            self.qmol = stripmol(mol_1)
            self.tmol = stripmol(mol_2)
        else:
            self.qmol = self.origqmol
            self.tmol = self.origtmol

        # round the coordinates
        roundmol(self.qmol,4)
        roundmol(self.tmol,4)
        roundmol(self.origqmol, 4)
        roundmol(self.origtmol, 4)


    def find(self):
        """Computes the MCS returning true if it was successful."""
        self.cwd = os.getcwd()
        os.chdir(self.dir)

        def cleanup():
            os.chdir(self.cwd)
            self.cwd = None

        # write the mols to MDL Molfiles
        self.qmol.write('mol','q.mol',overwrite=True)
        self.tmol.write('mol','t.mol',overwrite=True)

        # find the MCS
        out1 = osrun("SMSD %s -m -Q MOL -q q.mol -T MOL -t t.mol -O MOL -o out.mol" % (self.verboseopts), maxmem=self.maxmem).strip()
        if self.debug:
            print "Output of SMSD:>>>"
            print out1 + '<<<\n'
        reverse = False

        try:
            mol1 = readmol('mol', 'out.mol')
        except IOError:
            out1 = osrun("SMSD -m -Q MOL -q t.mol -T MOL -t q.mol -O MOL -o out.mol", maxmem=self.maxmem).strip()
            if self.debug:
                print "Output of SMSD:"
                print out1+'\n'
            try:
                mol1 = readmol('mol', 'out.mol')
            except IOError:
                #MCS search failed
                print "  Error: No common substructure for '%s' and '%s' found." % (self.qmol.title, self.tmol.title)
                cleanup()
                return False
            else:
                reverse = True

        mol1.removeh()
        mol1.OBMol.DeleteData(pybel.ob.StereoData)


        def readmapping(reverse=False):
            # read the mapping from file
            # enumerate the query atoms
            qcoords = []
            f = open('q.mol')
            lines = f.readlines()
            f.close()
            coordlines = int(lines[3][:3])
            for line in lines[4:4+coordlines]: #only lines with coordinates
                coords = line.split()[0:3]
                tmp = []
                for i in coords:
                    tmp.append(float(i))
                coords = tuple(tmp)
                qcoords.append(coords) #add the current atom to the list
            # enumerate the target atoms
            tcoords = []
            f = open('t.mol')
            lines = f.readlines()
            f.close()
            coordlines = int(lines[3][:3])
            for line in lines[4:4+coordlines]: #only lines with coordinates
                coords = line.split()[0:3]
                tmp = []
                for i in coords:
                    tmp.append(float(i))
                coords = tuple(tmp)
                tcoords.append(coords) #add the current atom to the list

            #read the mappings
            mappings = [] # set of tuples of tuples (q,t)
            try:
                f = open('mcs.out')
            except IOError:
                return False

            for line in f:
                if line.startswith('Max atoms matched'):
                    matchedatoms = int(line.split()[-1])
                elif line.startswith('Solution='):
                    # a mapping is found
                    mapping = []
                    line = f.next()
                    while not line.startswith('//'):
                        line = line.split()
                        if len(line) == 2:
                            pair = [int(line[0])-1,int(line[1])-1]
                            if reverse:
                                pair.reverse()
                            mapping.append(tuple(pair))
                        elif self.debug and len(line) != 0:
                            print 'Parsing error in line:', line
                        line = f.next()
                    mapping.sort()
                    mappings.append(tuple(mapping))
            mappings = set(mappings)
            f.close()

            if self.verbose:
                sys.stdout.write('%d Mapping(s) ... ' % (len(mappings)))
                sys.stdout.flush()
            if self.debug:
                prnt = False
                # check that the atom numbering is consistant
                # ===========================================
                for atom in self.qmol:
                    if atom.coords not in qcoords:
                        prnt = True
                        print 'Not found in q:',atom.coords
                for atom in self.tmol:
                    if atom.coords not in tcoords:
                        prnt = True
                        print 'Not found in t:',atom.coords
                # ===========================================
                if prnt:
                    print "qmol coords:"
                    for coords in qcoords:
                        print " ", coords
                    print "tmol coords:"
                    for coords in tcoords:
                        print " ", coords

            # build a mapping to the original mols
            mapmcsq = dict()
            for i,atom in enumerate(self.origqmol):
                if atom.coords in qcoords:
                    # query atom ids : orig. query atom ids
                    mapmcsq[qcoords.index(atom.coords)] = i
            mapmcst = dict()
            for i,atom in enumerate(self.origtmol):
                if atom.coords in tcoords:
                    mapmcst[tcoords.index(atom.coords)] = i

            # store the mapping between original atom ids
            for mapping in mappings:
                try:
                    m = [(mapmcsq[x],mapmcst[y]) for x,y in mapping]
                except KeyError:
                    if self.debug:
                        print "  Error: Mapping incomplete for %s and %s in substructure search!" % (self.origqmol.title, self.origtmol.title)
                        print "    keys:", x,', ', y
                        print "    mapmcsq", mapmcsq
                        print "    mapmcst", mapmcst
                        print "    mappings", mappings
                        try:
                            print '    Atom Nr %d in the mcs query file, was mapped to %d in the orig. query molecule.' % (x, mapmcsq[x] )
                        except KeyError:
                            pass
                        try:
                            print '    Atom Nr %d in the mcs target file, was mapped to %d in the orig. target molecule.' % (y, mapmcst[y] )
                        except KeyError:
                            pass
                else:
                    self.mappings.add(tuple(sorted(m)))

        # read the mapping
        readmapping(reverse = reverse)
        success = True
        if self.debug:
            # check for inhomogenities
            # ========================
            smi2 = osrun("SMSD -m -Q MOL -q t.mol -T MOL -t q.mol -O SMI -o --",maxmem=self.maxmem).strip()

            # strip and canonize mol strings
            smi1 = mol1.write('can').strip()

            mol2 = pybel.readstring('smi', smi2)
            mol2.removeh()
            mol2.OBMol.DeleteData(pybel.ob.StereoData)
            smi2 = mol2.write('can').strip().split()[0].strip()

            success = True
            if smi1 != smi2:
                # checker to assess SMSD determinism
                #self.subgmols.append(mol2)
                #self.subgsmis.append(smi2)
                success = False
                print "Not matching SMILES!"
                print ' >%s<' % smi1
                print ' >%s<' % smi2
                # read the mapping
                readmapping(reverse = True)
            # ========================

        self.subgmols.append(mol1)

        #clean up
        cleanup()

        return success

    def __del__(self):
        # remove not needed files
        if self.keeptemp:
            print 'Temporary dir is: %s' % (self.dir)
        else:
            rmtree(self.dir)
        if self.cwd is not None:
            os.chdir(self.cwd)

    def getMols(self):
        return self.subgmols
    def getSMILES(self):
        return self.subgsmis
    def getMappings(self):
        return self.mappings

class IsoStore(object):
    """Stores isomorphisms between Pyble molecule pairs in a dynamically sized cache."""

    def __init__(self, maxStoreItems=512):
        self.cdict = CountedDict() # key: hash(tuple), value: IsoMapping
        self.cdict.setMaxStoreItems(maxStoreItems)
        self.currentmols = None
        self.currentkey = None

    def __setitem__(self, key, value):
        """Key is a tuple of Pyble molecules, value is a list of tuples with atom mappings."""
        forcemoliterable(key)
        skey = self.smileskey(key)
        normkey = NormTuple(skey)
        self.cdict[normkey] = {'origkey': skey, 'mols':key, 'mapping':value}

    def __contains__(self,key):
        return NormTuple(self.smileskey(key)) in self.cdict

    def __getitem__(self, key):
        key = self.smileskey(key)
        normkey = NormTuple(key)
        vals = self.cdict[normkey]
        if vals['origkey'] == key:
            return vals['mapping']
        else:
            return self.reversemapping(vals['mapping'])

    def __delitem__(self, key):
        skey = self.smileskey(key)
        normkey = NormTuple(skey)
        del self.cdict[normkey]
        self.currentmols = None
        self.currentkey = None

    def keys(self):
        return self.cdict.keys()
    def values(self):
        return self.cdict.values()

    def smileskey(self,key):
        """For a tuple of mols, return the smiles key."""
        if not isinstance(key[0], str):
            if key == self.currentmols:
                skey = self.currentkey
            else:
                skey = tuple(cansmi(stripmol(tmp)) for tmp in key)
                self.currentkey = skey
                self.currentmols = key
            return skey
        else:
            return key

    def getKey(self, pybelmolecules):
        return NormTuple(self.smileskey(pybelmolecules))

    def getMols(self, key):
        key = self.smileskey(key)
        normkey = NormTuple(key)
        vals = self.cdict[normkey]
        mols = vals['mols']
        if vals['origkey'] == key:
            return mols
        else:
            return (mols[1], mols[0])

    def reversemapping(self, mappings):
        if len(mappings) > 0:
            tmp = iter(mappings).next()
            types = [type(mappings), type(tmp), type(iter(tmp).next())] # should be [set, tuple, tuple], but let's be tolerant...
            revmap = types[0]([ types[1]([types[2]((pair[1],pair[0])) for pair in mapping]) for mapping in mappings])
            return revmap
        else:
            return mappings

class IsoStoreEntry(object):
    """Stores all data of a mapping between a pair of molecules."""
    def __init__(self):
        self.mol0 = None
        self.mol1 = None
        self.isos = None

class IsoStorePersistent(IsoStore):
    """Stores isomorphisms between Pyble molecule pairs in persistant store."""

    def __init__(self, configfile='~/.kendrew.conf', verbose=False):
        self.configfile = os.path.expanduser(configfile)
        self.store = RedisStore(self.configfile).get()
        self.verbose = verbose
        self._reinit()

    def _reinit(self):
        self.currentkey = None
        self.currentmols = None
        self.currentsmikey = None
        self.reversed = False

    def _storeset(self, key, value):
        self.store.set(pickle.dumps(key), pickle.dumps(value))

    def _storeget(self, key):
        try:
            value = self.store.get(pickle.dumps(key))
        except redis.exceptions.TimeoutError:
            if self.verbose:
                print '  Connection to Redis timed out. Retrying...'
            sleep(random.uniform(0.1, .5))
            try:
                value = self.store.get(pickle.dumps(key))
            except redis.exceptions.TimeoutError:
                if self.verbose:
                    print '  Finally faild to connect to Redis.'
                value = None
        if value is None:
            raise KeyError(key)
        return pickle.loads(value)

    def __setitem__(self, key, value):
        """Key is a tuple of Pyble molecules, value is a list of tuples with atom mappings."""
        forcemoliterable(key)
        ikey = self._inchikey(key)

        entry = IsoStoreEntry()
        entry.mol0, entry.mol1 = [ mol.write(format='mol') for mol in self.currentmols ]
        if self.reversed:
            value = self.reversemapping(value)
        entry.isos = [iso for iso in value]

        if not self._key_in_store(ikey): # is the InChIkey pair present in the store?
            entries = dict()
        else:
            entries = self._storeget(ikey)
        entries[self.currentsmikey] = entry

        self._storeset(ikey, entries)

    def _key_in_store(self,key):
        return self.store.exists(pickle.dumps(key))

    def __contains__(self,key):
        key = self._inchikey(key)
        success = False
        try:
            success = self.currentsmikey in self._storeget(key)
            if self.verbose and not success:
                print '  SMILES key not found in store entry:\n  %s \n  %s\n  %s' % (str(self.currentsmikey), str(self._storeget(key).keys()), str(key) )
        except KeyError:
            if self.verbose:
                print '  SMILES key not found in store: %s' % self.currentsmikey
            pass
        return success

    def __getitem__(self, key):
        key = self._inchikey(key)
        if not self.__contains__(key):
            if self.verbose:
                print '  Key was not found in store: %s' % str(key)
            raise KeyError(key)
        entries = self._storeget(key)
        if not self.currentsmikey in entries:
            if self.verbose:
                print '  SMILES key was not found in store: %s' % str(key)
            raise KeyError(key)
        vals = entries[self.currentsmikey]
        if not self.reversed:
            return vals.isos
        else:
            return self.reversemapping(vals.isos)

    def __delitem__(self, key):
        key = self._inchikey(key)
        entries = self._storeget(key)
        del entries[self.currentsmikey]
        if len(entries) == 0:
            self.store.delete(pickle.dumps(key))
        else:
            self._storeset(key, entries)
        self._reinit()

    def keys(self):
        return [pickle.loads(x) for x in self.store.keys()]

    def values(self):
        return [pickle.loads(self.store.get(key)) for key in self.keys()]

    def _inchikey(self,key):
        """For a tuple of mols, return the InChI key."""
        if not isinstance(key[0], str):
            if self.currentmols is not None and key == self.currentmols:
                normkey = self.currentkey
            else:
                ikey = tuple(inchikey(tmp, ignoreStereo=True, stripH=True) for tmp in key)
                normkey = tuple(NormTuple(ikey))
                self.reversed = ikey != normkey
                self.currentkey = normkey
                self.currentmols = tuple(mol for mol in key)
                if self.reversed:
                    self.currentmols = tuple( reversed(self.currentmols) )
                self.currentsmikey = tuple(cansmi(mol, stripH = False, ignoreStereo = False) for mol in self.currentmols)
            return normkey
        else:
            return key

    def getKey(self, pybelmolecules):
        return self._inchikey(pybelmolecules)

    def getMols(self, key):
        entries = self._storeget(self._inchikey(key))
        vals = entries[self.currentsmikey]
        mol0 = pybel.readstring('mol', vals.mol0)
        mol1 = pybel.readstring('mol', vals.mol1)
        if self.reversed:
            return (mol1, mol0)
        else:
            return (mol0, mol1)


class LigandRMSD(object):
    """Computes LigandRMSD for batch applications, i.e. multiple runs against a target."""

    def __init__(self, defaultmolformat='mol2', runParallel=True, threads=None, verbose=False):
        self.configfile = os.path.expanduser('~/.kendrew.conf')
        self.isostore = IsoStorePersistent(configfile=self.configfile) # store isomorphisms for faster access
        self.qmols = None
        self.tmol = None
        self.results = []
        self.defaultmolformat = defaultmolformat
        self.verbose = verbose
        self.parallel = runParallel
        self.molsAsStrings = True
        self.qmolformat = self.defaultmolformat
        self.tmolformat = self.defaultmolformat
        self.threads = threads
        if not self.verbose:
            pybel.ob.OBMessageHandler().SetOutputLevel(-1)
            pybel.ob.obErrorLog.SetOutputLevel(-1)
            pybel.ob.obErrorLog.StopLogging()

    def _create_mol_from_str(self, molformat, molstring):
        try:
            mol = pybel.readstring(str(molformat), str(molstring))
        except IOError:
            print getTraceback()
            mol = None
        return mol

    def setQueryMol(self, molstring, molformat='mol'):
        """Provide the molecule as string and set it as query"""
        self.setQueryMols([molstring], molformat)

    def setQueryMols(self, molstrings, molformat='mol'):
        """Provide the a list of molecules as strings and set it as queries"""
        if not self.molsAsStrings:
            self.qmols = [self._create_mol_from_str(molformat, molstring) for molstring in molstrings]
        else:
            self.qmols = molstrings
            self.qmolformat = molformat

    def setTargetMol(self, molstring, molformat='mol'):
        """Provide the molecule as string and set it as target"""
        if not self.molsAsStrings:
            self.tmol = self._create_mol_from_str(molformat, molstring)
        else:
            self.tmol = molstring
            self.tmolformat = molformat

    def getMatchMols(self):
        """Gets the matching part of the two molecules with min LigandRMSD as string in the defaultmolformat."""
        return [result['qmol_match'] for result in self.results]

    def compute(self, LigandRMSDonly=True):
        if self.qmols is None or self.tmol is None:
            raise LigandRMSDError('You need to provide two molecules!')
        if not self.molsAsStrings:
            molformat = 'mol2'
            qmolstrs = [ qmol.write(format=molformat) for qmol in self.qmols ]
            tmolstr = self.tmol.write(format=molformat)
            self.qmolformat = molformat
            self.tmolformat = molformat
        else:
            qmolstrs = self.qmols
            tmolstr = self.tmol
        if not self.molsAsStrings:
            for qmol in self.qmols:
                self.results.append(ligandRMSD(qmol, self.tmol, isostore=self.isostore, getMatchMol=True, verbose=self.verbose))
        else:
            if self.parallel:
                parligrmsd = parallel_fn(ligandRMSDstring)
                self.results = [ result for result in parligrmsd( [(qmol, tmolstr) for qmol in qmolstrs], mol1format=self.qmolformat, mol2format=self.tmolformat, isostore=None, usePersistentStore=True, getMatchMol=True, ResultMolFormat=self.defaultmolformat, processes=self.threads, verbose=self.verbose )]
            else:
                for qmol in qmolstrs:
                    self.results.append(ligandRMSDstring( (qmol, tmolstr), mol1format=self.qmolformat, mol2format=self.tmolformat, isostore=self.isostore, getMatchMol=True, ResultMolFormat=self.defaultmolformat, verbose=self.verbose))
        if LigandRMSDonly:
            retval = [result['LigandRMSD'] for result in self.results]
        else:
            #convert all molecule objects to defaultmolformat strings
            for i,result in enumerate(self.results):
                for key in result:
                    if type(self.results[i][key]) == pybel.Molecule:
                        self.results[i][key] = self.results[i][key].write(self.defaultmolformat)
            retval = self.results
        if len(retval) == 1:
            retval = retval[0]
        return retval


class LigandRMSDjson(LigandRMSDjsonREST):
    def build(self):
        """Returns a LigandRMSD object,"""
        l = LigandRMSD(defaultmolformat=self.defaultmolformat, runParallel=self.runParallel, threads=self.threads, verbose=self.verbose)
        l.setQueryMols(self.qmols, self.qmolformat)
        l.setTargetMol(self.tmol, self.tmolformat)
        return l
