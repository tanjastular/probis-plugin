# The MIT License (MIT)
#
# Copyright (c) 2016 Joachim Haupt (TU Dresden / Germany)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

'''
Created on Apr 23, 2013

@author: joachimh
'''
import unittest, random
from kendrew.Molecules import *
from kendrew.toolchain.basic import rounddict


################################
###\\\\\\\\          ////////###
###        UNIT TESTS        ###
###////////          \\\\\\\\###
################################

class TestBasicFunctions(unittest.TestCase):

    def setUp(self):
        self.x = (0,0,0)
        self.y = (2,2,2)
        self.rand = random.randint(0,10**5)/13.0
        self.randy = tuple([tmp*self.rand for tmp in self.y])

        #create a dummy molecule with bond length 1
        self.mol = pybel.readstring('smi', "C1CCC1")
        coords = [(0,0,0),(1,0,0),(1,1,0),(0,1,0)] # square sphaped mol
        for i,atom in enumerate(pybel.ob.OBMolAtomIter(self.mol.OBMol)):
            x,y,z = coords[i]
            atom.SetVector(x,y,z)
        del(coords)
        #self.mol.write('mol','/tmp/mol.mol')
        #mol = transform(self.mol,numpy.array([-1,0,0]), numpy.identity(3))
        #mol.write('mol','/tmp/mol2.mol')

    def test_stripmol(self):
        self.assertEqual(self.mol.write('can'), stripmol(self.mol).write('can'))

    def test_center(self):
        self.assertEqual(center(self.x,self.y),(1,1,1))
        self.assertEqual(center(self.x,self.randy),tuple([0.5*tmp*self.rand for tmp in self.y]))

    def test_dist(self):
        self.assertEqual(dist(self.x,self.y),math.sqrt(12))

    def test_centroid(self):
        self.assertEqual(centroid(self.mol)[0],(0.5,0.5,0))

    def test_rmsd(self):
        self.assertEqual(round(rmsd( [(self.x,self.randy), (self.randy,self.randy) ] ),8), round(dist( self.x,self.randy )/math.sqrt(2),8))

    def test_transform(self):

        def _getCoords(mol):
            return sorted([(atom.GetX(), atom.GetY(), atom.GetZ()) for atom in pybel.ob.OBMolAtomIter(mol.OBMol)])
        #test identity
        self.assertEqual(_getCoords(transform(self.mol,numpy.zeros(3), numpy.identity(3))), _getCoords(self.mol))
        #test transformation = 1
        self.assertEqual(_getCoords(transform(self.mol,numpy.ones(3), numpy.identity(3))), [tuple([tmpc+1 for tmpc in tmp]) for tmp in _getCoords(self.mol)])
        #test rotation by 90 deg counterclockwise around z, in this case equivalent to a move along the x axis by -1
        self.assertEqual(_getCoords(transform(self.mol,numpy.zeros(3), numpy.array([[0,-1,0],[1,0,0],[0,0,1]]))), _getCoords(transform(self.mol,numpy.array([-1,0,0]), numpy.identity(3))))

    def test_mindist(self):
        self.assertEqual( mindist(self.mol, transform(self.mol, numpy.array([0,self.rand,0]), numpy.identity(3)) ) , round(self.rand,4))
        self.assertEqual( mindist(self.mol, transform(self.mol, numpy.array([self.rand,0,0]), numpy.identity(3)) ) , round(self.rand,4))
        self.assertEqual( mindist(self.mol, transform(self.mol, numpy.array([0,0,self.rand]), numpy.identity(3)) ) , round(self.rand,4))


class TestIso(unittest.TestCase):
    def setUp(self):

        self.rands = [random.randint(-10**3,10**3)/13.0 for tmp in range(21)]
        def _random_generate():
            numbers = random.sample(self.rands,3)
            cos = [math.cos(tmp) for tmp in numbers]
            sin = [math.sin(tmp) for tmp in numbers]
            phi,theta,psi = [0,1,2]
            self.randrot = [ [cos[theta]*cos[psi], sin[phi]*sin[theta]*cos[psi] - cos[phi]*sin[psi], sin[phi]*sin[psi] +cos[phi]*sin[theta]*cos[psi]],
                             [cos[theta]*sin[psi], cos[phi]*cos[psi] + sin[phi]*sin[theta]*sin[psi], cos[phi]*sin[theta]*sin[psi] - sin[phi]*cos[psi]],
                             [-sin[theta], sin[phi]*cos[theta], cos[phi]*cos[theta]] ] #rotation matrix with three random angles
            self.randrot = numpy.array(self.randrot)
            self.randtrans = numpy.array(random.sample(self.rands, 3))

        def _prepare_mol(smiles):
            mol = pybel.readstring('smi', smiles)
            mol.localopt()
            mol.removeh()
            roundmol(mol)
            _random_generate()
            return transform(mol, self.randtrans, self.randrot)

        self.q = _prepare_mol("C1C(=C)C(=ON)C1")
        self.t = _prepare_mol("C1C(=C)C(=OC)C1")
        self.s = _prepare_mol("C1C(=C)CC1")
        self.u = _prepare_mol("C1C(=C)C(=O)C1")

        self.molpool = [
                        'c1ccc(cc1)[C@@H]1CCN(CC1)CCc1cc2c([nH]1)cccc2',
                        'C[Se]CC[C@@H](C=O)N',
                        'O[C@@H]1[C@H](O)[C@H](O[C@H]1n1cnc2c1ncnc2N)COP(=O)(O)O'
                        ]
        self.molpool = [pybel.readstring('smi',tmp) for tmp in self.molpool]
        self.randt, self.randq = random.sample(self.molpool,2)


        _random_generate()


    def getCoords(self,mol):
        return sorted([(atom.GetX(), atom.GetY(), atom.GetZ()) for atom in pybel.ob.OBMolAtomIter(mol.OBMol)])


class TestLigandRMSD(TestIso):

    def ligandRMSD(self, isostore=None):
        move = abs(random.choice(self.rands))
        vec = [0,0,move]
        random.shuffle(vec)

        #get a transformed substructure
        iso = isomorphism(self.u, self.q)
        iso.findIso()
        iso.align_mobile()
        iso.transformStructures()
        u = iso.getTransformedQuery()
        origresult = ligandRMSD(self.q,u, isostore=isostore)
        utrans = transform(u, numpy.array(vec), numpy.identity(3))

        result = ligandRMSD(self.q,utrans, isostore=isostore)
        self.assertGreaterEqual(result['LigandRMSD']+2, move)
        self.assertGreaterEqual(result['LigandRMSD_raw'], result['LigandRMSD'])
        self.assertLessEqual(result['LigandRMSD'],  move+2)
        self.assertEqual(result['LigandRMSDratio'], 1)
        self.assertEqual(result['matchedAtoms'], countAtoms(self.u))

        if isostore is not None:
            self.assertTrue(isostore.getKey((self.u,self.q)) in isostore)

    def test_std(self):
        self.ligandRMSD()

    def test_isostore(self):
        store = IsoStore()
        self.ligandRMSD(isostore=store) # store it
        self.ligandRMSD(isostore=store) # get it again

class TestIsomorphism(TestIso):

    def test_setUp(self):
        #ensure that the molecule positions are random and thus unequal
        self.assertNotEqual(0, round(mindist(self.q, self.t),1))

    def test_findIso(self):
        # identity check
        iso = isomorphism(self.q, transform(self.q, self.randtrans, self.randrot) )
        self.assertTrue(iso.findIso())
        self.assertEqual(len(iso.isomorphs), 1) # only one isomorphism should be found
        isomorphs = list(iso.isomorphs)
        self.assertEqual([q for q,t in isomorphs[0]], [t for q,t in isomorphs[0]]) # the mapping should be identical

        # check for no iso with dissimilar mols
        iso = isomorphism(self.q, self.t )
        success = iso.findIso()
        #self.assertFalse(success)
        self.assertLessEqual(len(iso.isomorphs), 1) # only one isomorphism should be found

        # test subgraph
        iso = isomorphism(self.s, self.t )
        self.assertTrue( iso.findIso())
        self.assertLessEqual(len(iso.isomorphs), 4) # only two isomorphism should be found

        iso = isomorphism(self.q, self.t )
        self.assertTrue( iso.findIso(findall=False))
        self.assertEqual(len(iso.isomorphs), 1) # only one isomorphism should be found now

    def findMCS(self, iso):
        self.assertTrue(iso.findMCS())
        self.assertEqual(len(iso.isomorphs), 1) # only one isomorphism should be found
        self.assertTrue(iso.align_mobile()['rmsd']<=0.5)
#    def findMCS_single(self, iso):
#        self.assertTrue(iso.findMCS(all=False))
#        self.assertEqual(len(iso.isomorphs), 1) # only one isomorphism should be found
#        self.assertTrue(iso.align_mobile()['rmsd']<=0.5)


    def test_findMCS_identity(self):
        # identity check
        iso = isomorphism(self.q, self.q )
        self.findMCS(iso)
        isomorphs = list(iso.isomorphs)
        self.assertEqual([q for q,t in isomorphs[0]], [t for q,t in isomorphs[0]]) # the mapping should be identical

    def test_findMCS_subg(self):
        # identity check
        iso = isomorphism(self.u, self.q )
        self.findMCS(iso)

    def test_findMCS_mcs(self):
        iso = isomorphism(self.s, self.q )
        self.assertTrue(iso.findMCS())
        self.assertEqual(len(iso.isomorphs), 2) # only one isomorphism should be found
        self.assertTrue(iso.align_mobile()['rmsd']<=0.5)

    def test_findMCS_equals_findIso(self):
        isom = isomorphism(self.t, self.t )
        isom.findMCS()

        isoi = isomorphism(self.t, self.t )
        isoi.findIso()

        self.assertItemsEqual(isoi.isomorphs, isom.isomorphs)


    def test_align_iso(self):
        q = transform(self.q, numpy.array(self.rands[:3]), self.randrot)
        iso = isomorphism(self.q, q )
        iso.findIso()
        tmp = len(self.q.atoms)
        iso.align_mobile()
        iso.transformStructures()
        q,t = iso.getTransformedStructures()
        iso1 = isomorphism(copymol(q),copymol(t))
        iso1.findIso()
        align_iso = rounddict(iso1.align_iso(),3)
        align_mobile = rounddict(iso1.align_mobile(),3)
        self.assertEqual(align_iso ,{'rmsd':0, 'matchedAtoms':tmp, 'qAtoms':tmp, 'tAtoms':tmp} )
        self.assertEqual(align_mobile, align_iso )


    def test_align_mobile(self):
        iso = isomorphism(self.q, transform(self.q, self.randtrans, self.randrot) )
        iso.findIso()
        tmp = len(self.q.atoms)
        self.assertEqual(rounddict(iso.align_mobile(),3),{'rmsd':0, 'matchedAtoms':tmp, 'qAtoms':tmp, 'tAtoms':tmp})

class TestMCSinit(TestIso):
    def test__init__(self):
        #identity, unchanged mols
        mcs = MCS(self.q,self.q, stripmols=False)
        ref = self.q.write('can')
        self.assertEqual(mcs.qmol.write('can'), ref, "Equality input query to instance query")
        self.assertEqual(mcs.tmol.write('can'), ref, "Equality input target to instance target")
        #identity, stripped mols
        mcs = MCS(self.q,self.q,stripmols=True)
        self.assertEqual(stripmol(self.q).write('can'), mcs.qmol.write('can'), "Equality input query to stripped instance query")
        self.assertEqual(stripmol(self.q).write('can'), mcs.tmol.write('can'), "Equality input target to stripped instance target")
        self.assertEqual(mcs.qmol.write('can'), mcs.tmol.write('can'), "Equality stripped query to stripped instance target")
        #identity, stripped mols
        mcs = MCS(self.q,self.t,stripmols=True)
        self.assertEqual(stripmol(self.q).write('can'), mcs.qmol.write('can'), "Equality input query to stripped instance query")
        self.assertEqual(stripmol(self.t).write('can'), mcs.tmol.write('can'), "Equality input target to stripped instance target")

class TestMCSbasic(TestIso):

    def find(self,reference_molecule):
        self.mcs.find()
        subgmol = self.mcs.subgmols[0]
        self.assertEqual(len(self.mcs.subgmols),1)
        subgcoords = self.getCoords(subgmol)
        coords = zip(subgcoords, self.getCoords(reference_molecule))
        self.assertLessEqual(rmsd(coords), 0.5 )
        self.assertEqual(sum([1 for atom in reference_molecule]), max([len(tmp) for tmp in self.mcs.getMappings()])) # mapping must have the len of the substructure

    def compare_iso(self, query, target):
        #check the mapping for idenity
        iso = isomorphism(query, target )
        iso.findIso()
        self.assertEqual(sorted(list(iso.isomorphs)[0]), sorted(list(self.mcs.getMappings())[0]))

class TestMCSfind(TestMCSbasic):

    def setUp(self):
        super(TestMCSfind,self).setUp()
        self.mcs = None

    def test_find_identity(self):
        self.mcs = MCS(self.q,self.q, debug=False) #identity
        self.find(self.q)
        self.compare_iso(self.q, self.q)

    def test_find_mcs(self):
        #compute the substructure
        self.mcs = MCS(self.q,self.t, debug=False)

        #transform the substructure, such that it aligns to the target
        iso = isomorphism(self.u, self.t)
        iso.findMCS()
        iso.align_mobile()
        iso.transformStructures()
        utrans = iso.getTransformedQuery()

        #utrans.write('mol', '/tmp/trans.mol',overwrite=True)
        #self.t.write('mol', '/tmp/t.mol',overwrite=True)
        self.find(utrans)

class TestIsoStoreGeneral(TestIso):

    def test__init__(self):
        pass

    def test_normtuple(self):
        store = self.isostorename()
        if stripmol(self.q).write('can').strip() > stripmol(self.s).write('can').strip():
            q = self.q
            t = self.s
        else:
            q = self.s
            t = self.q

        invkey = store.smileskey((q,t)) # inverted
        key = store.smileskey((t,q)) # not inverted
        # test inverted key
        store[(q,t)] = [[(1,2)]]
        self.assertTrue(store.cdict[key]['origkey'] == invkey)
        self.assertFalse(store.cdict[key]['origkey'] == key)
        self.assertEqual(q, store.getMols(invkey)[0])
        self.assertEqual(t, store.getMols(invkey)[1])

        #test non-inverted key
        self.assertEqual(t, store.getMols(key)[0])
        self.assertEqual(q, store.getMols(key)[1])

    def test_reversemapping(self):
        store = self.isostorename()
        mapping = set([ tuple([ (int(self.rands[i]*self.rands[j]),int(self.rands[i*2]*self.rands[j]/j)) for i in range(1,11)]) for j in range(1,11)  ])
        revmapping = store.reversemapping(mapping)
        self.assertEqual(mapping, store.reversemapping(revmapping))

    def test_get_set(self):
        iso = isomorphism(self.q, self.t)
        iso.findIso()

        store = self.isostorename()
        for key in [ #store.getKey((self.q,self.t)),
                    (self.q,self.t),
                    (pybel.readstring('can', self.q.write('can')), pybel.readstring('can', self.u.write('can')) ) ]:
            if key in store:
                del store[key]
            self.assertFalse(key in store)
            #print 'putting key', key, store.getKey(key)
            store[key] = iso.isomorphs
            #check for the presence of the key and it's reversed representation

            self.assertTrue(key in store)
            self.assertTrue((key[1], key[0]) in store)

            self.assertEqual(set(store[key]), set(iso.isomorphs))

        #use can smi as a key
        #keysmi = (stripmol(key[0]).write('can').split()[0].strip(), stripmol(key[1]).write('can').split()[0].strip())
        #self.assertTrue(keysmi in store)
        #self.assertTrue([keysmi[1],keysmi[0]] in store)

    def test_capacity(self):
        maxStoreItems=5
        store = self.isostorename(maxStoreItems=maxStoreItems)

        keys = [ ( pybel.readstring('smi', ''.join(random.sample("CNO"*2, 5))), pybel.readstring('smi', ''.join(random.sample("CNO"*2, 5)))  ) for i in range(20)]

        for key in keys:
            store[key] = None
        self.assertTrue(len(store.keys()) <= maxStoreItems)

        for key in keys[1:]:
            store[key] = None
        self.assertTrue(keys[0] not in store)

        for tmp in range(5):
            store[keys[0]] = None
        for key in keys:
            store[key] = None
        self.assertTrue(keys[0] in store)
        self.assertTrue(len(store.keys()) <= maxStoreItems)

    def test_key(self):
        store = self.isostorename()
        #mol1 = stripmol(self.randq).write('can').split()[0].strip()
        #mol2 = stripmol(self.randt).write('can').split()[0].strip()
        key = store.getKey((self.randq,self.randt))
        #self.assertEqual(sorted(key), sorted([mol1,mol2]))
        self.assertEqual(tuple(sorted(key)), key)
        #print key
        #self.assertTrue('@' not in ''.join(key))
        #self.assertTrue('H' not in ''.join(key))

class TestIsoStore(TestIsoStoreGeneral):

    def __init__(self, args):
        self.isostorename = IsoStore
        super(TestIsoStore,self).__init__(args)


class TestIsoStorePersistant(TestIsoStoreGeneral):
    def __init__(self, args):
        self.isostorename = IsoStorePersistent
        super(TestIsoStorePersistant,self).__init__(args)

    def test_capacity(self):
        pass

    def test_normtuple(self):
        pass


############################
### Define tests to call ###
############################

tests = [
           TestBasicFunctions,
           TestMCSinit,
           TestMCSfind,
           TestIsomorphism,
           TestIsoStore,
           TestIsoStorePersistant,
           TestLigandRMSD
         ]

loadtest = unittest.TestLoader().loadTestsFromTestCase
alltests = unittest.TestSuite([loadtest(tmp) for tmp in tests])
unittest.TextTestRunner(verbosity=2).run(alltests)
