# The MIT License (MIT)
#
# Copyright (c) 2016 Joachim Haupt (TU Dresden / Germany)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

"""Provides basic communication with web services."""

import sys
import os
from time import sleep
import urllib2
import ssl
from kendrew.toolchain.errors import getTraceback

def printerror(request):
    print "Error while retrieving data."
    print "  request was %s\n  Traceback is:" %(request)
    print "\n    ".join(getTraceback().split('\n'))
    sys.stdout.flush()

def query(url, data=None, max_repeats = 1, verbose=False):
    req = urllib2.Request(url)
    repeats = 0
    while repeats <= max_repeats:
        repeats += 1
        noContext = False
        try:
            context = ssl.create_default_context()
            context.load_verify_locations(capath=os.path.join(os.path.dirname(__file__),'certs'))
        except Exception:
            noContext = True
        try:
            if not noContext:
                try:
                    res = urllib2.urlopen(req, data, timeout=600, context=context)
                except urllib2.URLError: # certificate validation failed
                    noContext = True
            if noContext:
                res = urllib2.urlopen(req, data, timeout=600)

        except urllib2.HTTPError, e:
            if int(e.code) == 550:
                #HTTP Error 550: Malformed input
                res = None
                print '  Error: Malformed input detected.'
                if verbose: printerror(url)
                break
            elif int(e.code) == 503:
                #HTTP Error 503: Service Temporarily Unavailable
                sleep(5)
            elif int(e.code) == 404:
                #HTTP Error 404: NOT FOUND
                res = None
                break
            else:
                sleep(5)
            if repeats == max_repeats:
                if verbose: printerror(url)
                raise e
        except Exception, e:
            sleep(5)
            if verbose: printerror(url)
            res = None
        else:
            break
    if res is None:
        return res
    else:
        return res.read()
