# The MIT License (MIT)
#
# Copyright (c) 2016 Joachim Haupt (TU Dresden / Germany)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from cStringIO import StringIO
import traceback

class ArgumentError(Exception):
    def __init__(self, e):
        super(ArgumentError,self).__init__('Argument ' + e + ' was not provided!')

class PubChemError(Exception):
    def __init__(self, e):
        super(PubChemError,self).__init__('PubChem PUG Error: '+e)


class SubProcessError(Exception):
    def __init__(self, e, exitcode=1):
        self.exitcode = exitcode
        super(SubProcessError,self).__init__('SubProcessError: '+e)

class LigandRMSDError(Exception):
    def __init__(self, e, exitcode=1):
        self.exitcode = exitcode
        super(LigandRMSDError,self).__init__('LigandRMSD Error: '+e)    

def raiseargerr(s):
    raise ArgumentError(s)

def raisepcerr(s):
    raise PubChemError(s)

def getTraceback():
    """Returns the traceback of the current error."""
    strbuff = StringIO()
    traceback.print_exc(file=strbuff)
    errtb = strbuff.getvalue()
    del strbuff
    return "   %s" % ( errtb.replace('\n','\n   ') )
