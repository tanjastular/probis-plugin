# The MIT License (MIT)
#
# Copyright (c) 2016 Joachim Haupt (TU Dresden / Germany)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

#multiprocessing tools
from __future__ import division

import numpy
import kendrew.toolchain.matrix
import pybel

# the 'force'* functions raise an error if the argument is not of the given type

def forcetuple(t,length=2):
    if type(t) is not tuple:
        raise TypeError("\"%s\" is not of type tuple." % str(t))
    if len(t) != 2:
        raise IndexError("Tuple \"%s\" is not of length %i." % (str(t),length))

def forcedict(d):
    if type(d) is not dict:
        raise TypeError("\"%s\" is not of type dict." % str(d))

def forcelistlike(l):
    t = (list,set,tuple)
    if not isinstance(l,t):
        raise TypeError("\"%s\" is not of type %s." % (str(l),str(t)))

def forcelisttuple(l):
    t = (list,tuple)
    if not isinstance(l,t):
        raise TypeError("\"%s\" is not of type %s." % (str(l),str(t)))

def forcematrix(l):
    t = (numpy.matrix,toolchain.Matrix.matrix)
    if not isinstance(l,t):
        raise TypeError("\"%s\" is not of type %s." % (str(l),str(t)))


def forcestr(l):
    t = (str,unicode)
    if not isinstance(l,t):
        raise TypeError("\"%s\" is not of type %s." % (str(l),str(t)))

def forcemol(mol):
    if not isinstance(mol,pybel.Molecule):
        raise TypeError("\"%s\" is not of type %s." % (str(mol),str(pybel.Molecule)))

def forcemoliterable(l):
    for mol in l:
        forcemol(mol)
