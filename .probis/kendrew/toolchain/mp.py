# The MIT License (MIT)
#
# Copyright (c) 2016 Joachim Haupt (TU Dresden / Germany)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

#multiprocessing tools
from __future__ import division

import multiprocessing
import sys
import zlib
import itertools
import cPickle

from time import sleep
from random import shuffle
from kendrew.toolchain.errors import raiseargerr
from kendrew.toolchain.files import osrun
from numpy import asarray
from functools import partial



# parallel_fn provides a simple wrapper for easy parallelization similar to a parallel for loop

def universal_worker(input_pair):
    """This is a wrapper function expecting a tiplet of function, single
       argument, dict of keyword arguments. The provided function is called
       with the appropriate arguments."""
    function, arg, kwargs = input_pair
    return function(arg, **kwargs)

def pool_args(function, sequence, kwargs):
    """Return a single iterator of n elements of lists of length 3, given a sequence of len n."""
    return itertools.izip(itertools.repeat(function), sequence, itertools.repeat(kwargs) )

def parallel_fn(f):
    """Simple wrapper function, returning a parallel version of the given function f.
       The function f must have one argument and may have an arbitray number of
       keyword arguments. Additionally, f must be defined outside any other function."""

    def simple_parallel(f, sequence, **args):
        """ f takes an element of sequence as input and the keyword args in **args"""
        multiprocessing.freeze_support()
        if 'processes' in args and args.get('processes') is not None:
            processes = args.get('processes')
            del args['processes']
        else:
            processes = max(multiprocessing.cpu_count()-1, 1)

        #print "prcesses are set to:", processes
        pool = multiprocessing.Pool(processes=processes) # depends on available cores
        try:
            result = pool.map( universal_worker, pool_args(f, sequence, args) )
        except cPickle.PicklingError, e:
            raise type(e)(e.message + "\n    Did you make sure to define the function to parallelize outside any other function?")
        cleaned = [x for x in result if not x is None] # getting results
        cleaned = asarray(cleaned)
        pool.terminate()
        pool.join()
        return cleaned
    return partial(simple_parallel, f )




def _wrapfn(fn, args):
    """This function controls the worker behavior. The arguments for fn are stored in args.
    Alternative value for args is WAIT."""
    name = multiprocessing.current_process().name
    if args == 'WAIT':
        sleep(5)
        return (name, 'WAIT')
    return (name, fn(*args))

class Pool(object):
    """Creates a new Pool of Workers, accepting arbitrary data. Only for shared memory architectures."""
    def __init__(self, **args):

        print "\nSetting up multiprocessing..."
        print "-----------------------------\n"

        #iteratable object containing the data to be processed
        self.slices = args.get('slices') or None
        self.queues = args.get('queues') or None
        if (self.slices is None) == (self.queues is None):
            raiseargerr('a list of slices xor data queues')

        self.results = []

        #percentage of available PU to be used
        self.pu_ratio = args.get('pu_ratio') or (1)

        #the number of available PUs
        if args.get('ncpus') is None:
            self.ncpus = multiprocessing.cpu_count()
        else:
            self.ncpus = args.get('ncpus')
        smppus = osrun('echo $NSLOTS').strip()
        if smppus != '':
            self.ncpus = int(smppus)
            print '  Using only the number of PUs (%i) assigned by the queuing system!.' % self.ncpus

        #number of processes should be lower than the number of available cpus
        self.nproc = args.get('nproc') or int(round((self.ncpus)*self.pu_ratio,0)) #min(int(round((self.ncpus)*self.pu_ratio,0)), self.ncpus-1)

        #time to wait before checking again for results
        self.sleep_time = args.get('sleep_time') or 5

        #init function for the worker processes
        # def f_init(q1,q2):
            # q = q1
            # d = q2

        #create a pool of workers and let them do the job
        self.pool = multiprocessing.Pool(processes=self.nproc)

        print '  Detected', self.ncpus, 'CPUs. Ratio is', str(self.pu_ratio),'Thus, using', self.nproc, ' worker processes.'

        if self.slices is not None:
            print '  There are', len(self.slices), 'jobs to distribute...'
            self.slices.sort() #process slices with many members first
        sys.stdout.flush()

    def enqueue(self, fn=None, args=[]):
        """Enqueue and start the jobs by execution of fn(args), while args should be a list. The first argument of fn has to be data previously stored in the slices object (see __init__). But `args` given to `enqueue` should NOT contain this data, since it is dynamically added from the slices object."""
        if self.slices is not None:
            for sli in self.slices:
                #add the data to be processed to frontmost list position
                tmpargs = [sli]
                tmpargs.extend(args)
                tmpargs = [fn,tmpargs]
                self.results.append( self.pool.apply_async(_wrapfn, tmpargs) )
        elif self.queues is not None:
            for pu in range(0,self.nproc):
                self.results.append( self.pool.apply_async(_wrapfn, [fn].append(args)) )
        else:
            raiseargerr('Neither queue nor slices present!')

        print '  All jobs queued.'
        sys.stdout.flush()

    def print_results(self, quiet=False):
        """Waits for all processes to finish and prints the results of each process."""
        while len(self.results) > 0:
            remresults = []
            for result in self.results:
                if result.ready():
                    name,tmpresult = result.get()
                    if type(tmpresult) not in (bool, float, int):
                        try: #decompress compressed results
                            tmpresult = zlib.decompress(tmpresult)
                        except zlib.error: #print plain results
                            pass
                    if not result.successful() and not quiet:
                        print '  ERROR in the worker process:\n+++++++++++++++++++++++++++'
                    if result.get() == 0:
                        if not quiet:
                            print 'Process %s has cleanly quit.' % name
                    else:
                        if not quiet:
                            print 'Process %s returned:' % name
                        print tmpresult
                    sys.stdout.flush()
                    remresults.append(result)
            #remove finished results
            for result in remresults:
                self.results.remove(result)
            sleep(self.sleep_time)

class DistributedPool(Pool):
    pass

class distinctmatrix(object):

    def __init__(self, **args):
        """Transforms the given list to a n upper triangular matrix, whose submatrices can be processed seperately. It returns a suprocess queue, which returns a new chunk, if ready."""

        self.l = args.get('list') or raiseargerr('list')
        self.q = args.get('qtasks') or raiseargerr('qtasks')
        self.d = args.get('qdone') or raiseargerr('qdone')
        #self.o = args.get('qoutput') or raiseargerr('qoutput')
        self.l = list(self.l)
        #self.output = ''

        self.m = dict()
        cols = list(self.l)

        while len(cols) > 0:
            # build a triangular matrix
            self.m[cols[0]] = list(cols)
            #print cols[0], self.m[cols[0]]
            cols = cols[1:]

        # create set of locked ids
        self.locked = set()

        #self.q = Queue(maxsize=len(self.l)) # queue for jobs to process
        #self.d = Queue(maxsize=len(self.l)) # queue for processed jobs

    def getqueues(self):
        """Returns a tuple (q,d) of a queue of jobs q and a queue where to submit the done jobs d together with the results."""

        return (self.q, self.d)

    def process(self):
        """Adds the chunks to the queue and waits for all chunks to be processed. It returns a result matrix with zlib compressed values."""

        # create a list of tuples to process
        ptub = set()
        for i in self.m.keys():
            for j in self.m[i]:
                ptub.add((i,j))

        while len(ptub) > 0 or len(self.locked) > 0:
            #for all tuples to process
            remt = set() #tuples to remove from the set of tuples to process
            # shuffle the set to ensure, that jobs are picked randomly
            ptub = list(ptub)
            ptub = shuffle(ptub)
            for t in ptub:
                i,j = t
                if (i not in self.locked) and (j not in self.locked):
                    remt.add(t)
                    self.locked.update([i,j])
                    self.q.put( (i,j) ) # add the tuple to the queue
            ptub = set(ptub)
            ptub = ptub - remt
            if len(ptub) == 0:
                print 'Sending the STOP signal to the worker processes.\n  This will stop the calculation after all tasks are processed.'
                self.q.put( 'STOP' )
            if len(self.locked) > 0:
                # get results and mark processed ids as free
                t = self.d.get()
                i,j = t[0:2] #the two ids
                #get the output of the job, if present
                if len(t) == 3:
                    o = t[2]
                else:
                    o = None

                self.locked.remove(i) #remove the lock from i
                if i != j:
                    self.locked.remove(j)
                if o is not None:
                    print o
            sleep(5)
        print '  ...done'


# Test

def mult(x):
        print "Hi, I'm process %s and got %d. I'm napping for %.0f s." % (multiprocessing.current_process().name,x, x%3*4)
        sleep(x%3*4)
        return str(x*2)
if __name__ == "__main__":
    print 'OK'
    #p = Pool(slices=[1,2,3,4,5,6,7,8,9,10],sleep_time=0.01)
    p = Pool(slices=range(20),sleep_time=0.01)
    p.enqueue(fn=mult)
    p.print_results()
    print 'all done'
