# The MIT License (MIT)
#
# Copyright (c) 2016 Joachim Haupt (TU Dresden / Germany)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from __future__ import division
from math import sqrt
from copy import deepcopy
import numpy

class matrix(object):
    def __init__(self,elements,m,n):
        """Builds an m*n matrix of the given Elements."""
        if not isinstance(elements, list):
            data = list(elements)
        else:    
            data = elements
        self.m = m #rows
        self.n = n #columns
        tmp = self.n*self.m
        if tmp != len(data):
            raise ValueError('The dimensions provided (%d*%d) do not fit the number of elements (%d)' % (self.m, self.n, len(data)))
        # create rows and columns  
        self.rows = [data[(i*self.n):self.n+(i*self.n)] for i in range(self.m)]
        #self.cols = [[] for i in range(self.n)]
#        for row in self.rows:
#            for i,ele in enumerate(row):
#                self.cols[i].append(ele)
    
    def __str__(self):
        s = "[\n"
        for row in self.rows:
            s= "%s%s\n" % (s,', '.join([str(i) for i in row]))
        s += ']'
        return s
    
    def __len__(self):
        return len(self.rows)
    
    def submatrix(self,i,k,j,l):
        """Returns the submatrix starting in (i,k) --upper left-- to (j,l) --lower right."""
        tmp = []
        for ii in range(i,j+1): #rows
            r = self.rows[ii]
            if len(r) <= l-k+1:
                tmp.append(r)
            else:
                tmp.append(r[k:l+1])
        return tmp
     
    
    def diagonal(self):
        if self.m != self.n:
            raise(NameError, 'Rows and columns differ in length.')
        return [self.rows[i][i] for i in range(self.m)]
    
    def triu(self):
        """Returns the upper triangular matrix without the diagonal."""
        if self.m != self.n:
            raise(NameError, 'Rows and columns differ in length.')
        return [self.rows[i][i+1:] for i in range(self.m-1)]
    def shape(self):
        return (self.m,self.n)
            
class smatrix(matrix):
    """Builds a square matrix of the given Elements."""
    def __init__(self,elements):
        if isinstance(elements, numpy.matrix):
            elements = numpy.array(elements).reshape(-1,).tolist()
            
        m = int(sqrt(len(elements)))
        super(self.__class__,self).__init__(elements, m, m)
            
# Test

if __name__ == "__main__":
    m = smatrix(range(36))
    print m
    print m.diagonal()
    print m.triu()