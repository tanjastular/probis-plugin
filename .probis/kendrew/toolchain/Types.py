# The MIT License (MIT)
#
# Copyright (c) 2016 Joachim Haupt (TU Dresden / Germany)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

'''
Created on Mar 1, 2013

@author: joachimh
'''
import collections
import sys
import math
import os
import ConfigParser

try:
    import MySQLdb
except ImportError:
    pass
try:
    import redis
except ImportError:
    pass


class NormTuple(tuple):
    """A tuple with sorted elements."""
    def __new__ (cls, arg):
        return super(NormTuple, cls).__new__(cls, tuple(sorted(arg)))

class maxdict(dict):
    """Like dict, but replaces only iff value is bigger than before."""
    def __init__(self, *args, **kw):
        super(maxdict,self).__init__(*args, **kw)
    def __setitem__(self, key, value):
        if self.__contains__(key) and value <= self.__getitem__(key):
            pass
        else:
            super(maxdict,self).__setitem__(key, value)
    def update(self, E):
        if callable(getattr(E, "keys", None)):
            for k in E:
                self[k]=E[k]
        else:
            for k,v in E:
                self[k] = v


class CountedDict(collections.MutableMapping):
    """Extends the dict type by an access count of the keys."""

    def __init__(self, *args, **kwargs):
        self.counts = dict() #counts each access to each key
        self.queue = [] #order in which keys were added to the dict
        self.store = dict() #ds to store the data: dict
        self.countcutoff = 2 #to be balanced, the mean of the key count should be above this value
        self.removeperc = 0.5 # ratio of data to be removed on balancing
        self.memorymax = 2000 # max memory of the object
        self.maxstoreitems = 16

        self.update(dict(*args, **kwargs))

    def __getitem__(self, key):
        return self.store[self.__count__(key)]

    def __setitem__(self, key, value):
        if not self.__is_balanced__():
            self.balance()
        if key not in self.queue:
            self.queue.append(key)
        self.store[self.__count__(key)] = value

    def __delitem__(self, key):
        del self.store[key]
        self.queue.remove(key)

    def __iter__(self):
        return iter(self.store)

    def __len__(self):
        return len(self.store)

    def __count__(self, key):
        if key not in self.counts:
            self.counts[key] = 0
        self.counts[key] += 1
        return key

    def __is_balanced__(self):
        tmp = self.counts.values()
        mean = sum(tmp)/max(1,len(tmp))
        return len(self.store) < self.maxstoreitems and (len(self.store) < 2/3.0*self.maxstoreitems or mean > self.countcutoff) #or sys.getsizeof(self.store) < self.memorymax

    def __is_withinmemory__(self):
        return sys.getsizeof(self.store) < self.memorymax

    def __str__(self):
        return str(self.store)

    def balance(self):
        """Reduces the object size by deleting 50% of unused data."""
        age = dict([(key,tmpage) for tmpage,key in enumerate(self.queue)]) # lower numbers denote older entries
        delkeys = sorted(self.keys(), key=lambda x: (self.counts[x],age[x]))
        for key in delkeys[:int(math.ceil(len(delkeys)*self.removeperc))]:
            self.__delitem__(key)

    def setMaxStoreItems(self,maxstoreitems):
        self.maxstoreitems = maxstoreitems


class CountedFiles(CountedDict):
    """Extends the dict type by an access count of the keys."""

    def __init__(self, path=None, *args, **kwargs):
        self.path = path
        self.dontbalance = False
        super(CountedFiles,self).__init__(*args, **kwargs)

    def do_not_balance(self):
        self.dontbalance = True

    def do_balance(self):
        self.dontbalance = False
        if not self.__is_balanced__():
            self.balance()

    def balance(self):
        """Reduces the object size by deleting 50% of unused data."""
        if not self.dontbalance:
            age = dict([(key,tmpage) for tmpage,key in enumerate(self.queue)]) # lower numbers denote older entries
            delkeys = sorted(self.keys(), key=lambda x: (self.counts[x],age[x]))
            for key in delkeys[:int(math.ceil(len(delkeys)*self.removeperc))]:
                for f in self.store[key]:
                    try:
                        os.remove(os.path.join(self.path, f))
                    except OSError:
                        pass
                self.__delitem__(key)


class OrderedSet(collections.MutableSet):

    def __init__(self, iterable=None):
        self.end = end = []
        end += [None, end, end]         # sentinel node for doubly linked list
        self.map = {}                   # key --> [key, prev, next]
        if iterable is not None:
            self |= iterable

    def __len__(self):
        return len(self.map)

    def __contains__(self, key):
        return key in self.map

    def add(self, key):
        if key not in self.map:
            end = self.end
            curr = end[1]
            curr[2] = end[1] = self.map[key] = [key, curr, end]

    def discard(self, key):
        if key in self.map:
            key, prev, next = self.map.pop(key)
            prev[2] = next
            next[1] = prev

    def __iter__(self):
        end = self.end
        curr = end[2]
        while curr is not end:
            yield curr[0]
            curr = curr[2]

    def __reversed__(self):
        end = self.end
        curr = end[1]
        while curr is not end:
            yield curr[0]
            curr = curr[1]

    def pop(self, last=True):
        if not self:
            raise KeyError('set is empty')
        key = self.end[1][0] if last else self.end[2][0]
        self.discard(key)
        return key

    def __repr__(self):
        if not self:
            return '%s()' % (self.__class__.__name__,)
        return '%s(%r)' % (self.__class__.__name__, list(self))

    def __eq__(self, other):
        if isinstance(other, OrderedSet):
            return len(self) == len(other) and list(self) == list(other)
        return set(self) == set(other)

class DBconnection(object):
    def __init__(self, configfile):
        self.configfile = os.path.expanduser(configfile)
        self.config = ConfigParser.SafeConfigParser()
        if os.path.exists(self.configfile):
            self.config.read(self.configfile)

        # should be set in the inheriting class
        self.connection = None
        self.sectionName = None # name of the section in the config file
        self.connected = False

    def connect(self):
        return False

    def get(self):
        if not self.connected:
            self.connect(verbose=False)
        return self.connection

    def flush(self):
        """Deletes all config."""
        self.config = ConfigParser.SafeConfigParser()

    def write_config_file(self,
        configDict={
            'user': 'test',
            'password': 123
            },
        configfile=None):
        """Writes a default config file to store access parameters."""
        if configfile is not None:
            self.configfile = configfile
        try:
            self.config.add_section(self.sectionName)
        except ConfigParser.DuplicateSectionError:
            pass
        for key in configDict:
            self.config.set(self.sectionName, key, configDict[key])
        with open(self.configfile, 'wb') as f:
            self.config.write(f)

class RedisStore(DBconnection):

    def __init__(self, configfile, db=0):
        super(RedisStore,self).__init__(configfile)
        self.db = db

        self.connection = None
        self.sectionName = 'redis' # name of the section in the config file

    def connect(self, verbose=True):
        try:
            self.connection = redis.StrictRedis(
                host=self.config.get(self.sectionName, 'host'),
                port=int(self.config.get(self.sectionName, 'port')),
                db=self.db, #0
                password=self.config.get(self.sectionName, 'password'),
                socket_connect_timeout=5, # timout for socket.connect()
                socket_timeout=10 # timout for a query to redis
                )
        except Exception, e:
            if verbose:
                print "  Failed to establish the connection to Redis!"
                raise e
            self.connected = False
        else:
            self.connected = True
        return self.connected

    def write_config_file(self, configfile=None):
        """Writes a default config file to store access parameters."""
        super(RedisStore,self).write_config_file(
            configDict={
                'host': 'redis_host.my.domain',
                'port': '1234',
                'password': 'secretPassword'
                },
            configfile=configfile
            )

class comprot(DBconnection):

    def __init__(self, configfile, db='comprot'):
        super(comprot,self).__init__(configfile)

        self.db = db
        self.connection = None
        self.sectionName = 'comprot' # name of the section in the config file

    def connect(self, verbose=True):
        try:
            self.connection = MySQLdb.connect(
                host=self.config.get(self.sectionName, 'host'),
                user=self.config.get(self.sectionName, 'user'),
                passwd=self.config.get(self.sectionName, 'password'),
                db=self.db
                )
        except MySQLdb.Error, e:
            print "Error %d: %s" % (e.args[0], e.args[1])
            raise IOError(e)
        except NameError, e:
            print "Database connection could not be established! Using flat files instead..."
            self.connected = False
        else:
            self.connection = self.connection.cursor()
            self.connected = True
        return self.connected

    def write_config_file(self, configfile=None):
        """Writes a default config file to store access parameters."""
        super(comprot,self).write_config_file(
            configDict={
                'host': 'mysql_host.my.domain',
                'user': 'mysql_user_name',
                'password': 'secretPassword'
                },
            configfile=configfile
            )
