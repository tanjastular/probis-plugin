# The MIT License (MIT)
#
# Copyright (c) 2016 Joachim Haupt (TU Dresden / Germany)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import sys
import os
import numpy

def flatten(l, ltypes=(list, tuple)):
    """Transforms a nested list or tuple to a flat representation."""
    ltype = type(l)
    l = list(l)
    i = 0
    while i < len(l):
        while isinstance(l[i], ltypes):
            if not l[i]:
                l.pop(i)
                i -= 1
                break
            else:
                l[i:i + 1] = l[i]
        i += 1
    return ltype(l)

def flatten_listonly(l):
    return flatten(l,ltypes=(list))

def underline(text, symbol="="):
    """Returns an underlined version of the given string."""
    if symbol is None:
        symbol = "="
    return "\n%s\n%s\n" % (text, symbol*len(text))

def numpytolist(l):
    """Walks through an iterable, converting all numpy objects to lists."""
    return [tmp.tolist() if isinstance(tmp, numpy.object) else tmp for tmp in l]

def rounddict(dictionary, places=6):
    """Rounds the values in a dictionary to the given number of decimal places."""
    return dict(zip(dictionary.keys(),[round(tmpx,places) for tmpx in dictionary.values()]))

class Progress(object):
    def __init__(self,total=False, title="PROGRESS"):
        self.total = total
        self.title = title
        self.tail = 0
        self.current = 0
        self.first = True
        
    def __del__(self):
        sys.stdout.write('\n')
        
    def prnt(self):
        if self.first:
            sys.stdout.write("%s: " % self.title)
            self.first = False
        if self.total:
            tail = "%d / %d" % (self.current,self.total)
        else:
            tail = "%d" % (self.current)
            
        sys.stdout.write('\b'*self.tail + tail)
        sys.stdout.flush()
        self.tail = len(tail)
        
    def setcurrent(self, current):
        self.current = current
        
    def update(self, current):
        self.current = current
        self.prnt()
        
    def close(self,current):
        self.update(current)
        sys.stdout.write('\n')
        
    def full(self,current):
        sys.stdout.write("%s: " % self.title)
        self.close(current)
        

class RedirectStdStreams(object):
    def __init__(self, stdout=None, stderr=None, merge=False):
        self._stdout = stdout or sys.stdout
        if merge:
            self._stderr = self._stdout
        else:
            self._stderr = stderr or sys.stderr
    def __enter__(self):
        self.old_stdout, self.old_stderr = sys.stdout, sys.stderr
        self.old_stdout.flush(); self.old_stderr.flush()
        sys.stdout, sys.stderr = self._stdout, self._stderr
    def __exit__(self, exc_type, exc_value, traceback):
        self._stdout.flush(); self._stderr.flush()
        sys.stdout = self.old_stdout
        sys.stderr = self.old_stderr

class RedirectStdNull(RedirectStdStreams):
    def __init__(self):
        devnull = open(os.devnull, 'w')
        super(RedirectStdNull,self).__init__(stdout=devnull, stderr=devnull)
        
class ProgressMsg(object):
    def __init__(self, showmsg=True):
        self.msg=None
        self.ok=False
        self.showmsg=showmsg
    def set(self,msg):
        self.msg = msg.strip()
        if self.showmsg:
            sys.stdout.write(str("%s..." % self.msg))
            sys.stdout.flush()
    def dot(self, cha="."):
        if self.showmsg:
            sys.stdout.write(cha)
            sys.stdout.flush()
    def done(self,endmsg='OK', ok=True,extra=None):
        self.ok=ok
        if self.showmsg:
            if self.ok:
                sys.stdout.write(str("%s\n" % endmsg))
            else:
                sys.stdout.write(str("Failed\n"))
            if extra is not None:
                print extra
            sys.stdout.flush()


