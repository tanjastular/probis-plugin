# The MIT License (MIT)
#
# Copyright (c) 2016 Joachim Haupt (TU Dresden / Germany)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
#@author: V. Joachim Haupt
#@contact: joachimh@biotec.tu-dresden.de
#@organization: BIOTEC/TU Dresden
#@version: 0.9
#@note: methods to access the comprot database


from __future__ import division
from kendrew.toolchain.files import read
from kendrew.toolchain.Types import comprot as ComprotDB
import os
import re

try:
    import MySQLdb
except ImportError:
    pass


class Comprot(object):

    def __init__(self, **args):
        """Establishes a DB connection to comprot."""
        self.cursor = None
        self.verbose = args.get('verbose') or False
        #path for flat files, if DB connection fails
        self.path = os.path.expanduser('~/lib/pdbsuppl')
        if args.get('noDB') is True:
            if self.verbose:
                print "Database connection will not be used as requested! Using flat files instead..."
            self.db = False
        else:
            try:
                self.conn = ComprotDB(configfile='~/.kendrew.conf')
                self.conn.connect(verbose=True)
            except MySQLdb.Error, e:
                print "Error %d: %s" % (e.args[0], e.args[1])
                raise IOError(e)
            except NameError, e:
                print "Database connection could not be established! Using flat files instead..."
                self.db = False
                #raise NameError(e)
            else:
                self.db = True
                self.cursor = self.conn.get()

    def get_cursor(self):
        return self.cursor

    def BS_sim_probis(self, pdbid1, pdbid2, pdbchain1, pdbchain2, ligrmsdthreshold=3, get_members=False):

        self.cursor.execute("desc SIM_PDB_BINDING_SITE_PROBIS")
        columns = [cols[0] for cols in self.cursor.fetchall()]
        columns.sort(key=lambda column: ([float(nr) for nr in re.findall('[0-9]' ,column)],column) ) # columns with a 1 come first
        query1 = """
        select %s
        from
          SIM_PDB_BINDING_SITE_PROBIS
        where
          PDB_ID1$="%s"
          and
          PDB_CHAIN1$="%s"
          and
          PDB_ID2$="%s"
          and
          PDB_CHAIN2$="%s"
          and
          LIG_RMSD<=%.2f
        """ % (", ".join(columns), pdbid1, pdbchain1, pdbid2, pdbchain2, ligrmsdthreshold)
        columns2 = sorted(columns, key=lambda column: ([1/(1+float(nr)) for nr in re.findall('[0-9]+' ,column)],column) ) # columns with a 1 come first
        query2 = """
        select %s
        from
          SIM_PDB_BINDING_SITE_PROBIS
        where
          PDB_ID2$="%s"
          and
          PDB_CHAIN2$="%s"
          and
          PDB_ID1$="%s"
          and
          PDB_CHAIN1$="%s"
          and
          LIG_RMSD<=%.2f
        """ % (", ".join(columns2), pdbid1, pdbchain1, pdbid2, pdbchain2, ligrmsdthreshold)
        query = "%s union %s" % (query1, query2)
        if get_members:
            query = query.replace('$', "_ORIG")
        else:
            query = query.replace('$', '')
        self.cursor.execute(query)
        data = self.cursor.fetchall()

        results = [dict(zip(columns, d)) for d in data]
        return results

    def BS_sim_pdb(self):
        """Returns a comprehensive BS sim table of SMAP.
        0           1           2               3               4
        PDB_ID1 | PDB_CHAIN1 | PDB_HET_ID1 | PDB_ORIG_ID1 | PDB_ORIG_CHAIN1 |
        5           6           7               8           9
        PDB_ID2 | PDB_CHAIN2 | PDB_HET_ID2 | PDB_ORIG_ID2 | PDB_ORIG_CHAIN2 |
        10            11        12
        P_VALUE | LIG_RMSD | LIG_RMSD_DIFF"""
        if self.db:
            self.cursor.execute("""
               select
                  a.PDB_ID1,
                  a.PDB_CHAIN1,
                  a.PDB_HET_ID1,
                  b.PDB_ORIG_ID1,
                  b.PDB_ORIG_CHAIN1,
                  a.PDB_ID2,
                  a.PDB_CHAIN2,
                  a.PDB_HET_ID2,
                  b.PDB_ORIG_ID2,
                  b.PDB_ORIG_CHAIN2,
                  b.P_VALUE,
                  b.LIG_RMSD,
                  b.LIG_RMSD_DIFF
              from
                  (
                  -- get the min Lig RMSD Diff for each cluster
                  -- the lexicographically smallest PDB id is used
                  select
                    PDB_ID1,
                    PDB_CHAIN1,
                    PDB_HET_ID1,
                    PDB_ID2,
                    PDB_CHAIN2,
                    PDB_HET_ID2,
                    min(LIG_RMSD_DIFF) as LIG_RMSD_DIFF
                  from
                    (
                    select
                      IF(STRCMP(d1.PDB_ID,d2.PDB_ID)=1,d2.PDB_ID,d1.PDB_ID) as PDB_ID1,
                      IF(STRCMP(d1.PDB_ID,d2.PDB_ID)=1,d2.PDB_CHAIN,d1.PDB_CHAIN) as PDB_CHAIN1,
                      IF(STRCMP(d1.PDB_ID,d2.PDB_ID)=1,s.PDB_HET_ID2,s.PDB_HET_ID1) as PDB_HET_ID1,
                      IF(STRCMP(d1.PDB_ID,d2.PDB_ID)=1,d1.PDB_ID,d2.PDB_ID) as PDB_ID2,
                      IF(STRCMP(d1.PDB_ID,d2.PDB_ID)=1,d1.PDB_CHAIN,d2.PDB_CHAIN) as PDB_CHAIN2,
                      IF(STRCMP(d1.PDB_ID,d2.PDB_ID)=1,s.PDB_HET_ID1,s.PDB_HET_ID2) as PDB_HET_ID2,
                      s.LIG_RMSD_DIFF
                    from
                      SIM_PDB_BINDING_SITE_SMAP s,
                      PDB_CLUSTER95 c1,
                      VIEW_PDB_REPRESENTATIVE_CLUST d1,
                      PDB_CLUSTER95 c2,
                      VIEW_PDB_REPRESENTATIVE_CLUST d2
                    where
                      s.PDB_ID1 = c1.PDB_ID and
                      s.PDB_CHAIN1 = c1.PDB_CHAIN and
                      c1.CLUSTER_ID = d1.CLUSTER_ID and
                      s.PDB_ID2 = c2.PDB_ID and
                      s.PDB_CHAIN2 = c2.PDB_CHAIN and
                      c2.CLUSTER_ID = d2.CLUSTER_ID
                    ) x
                  group by
                    PDB_ID1,
                    PDB_CHAIN1,
                    PDB_HET_ID1,
                    PDB_ID2,
                    PDB_CHAIN2,
                    PDB_HET_ID2
                  ) a
                JOIN
                  (
                  -- get the mapping pdb cluster to original
                  select
                    IF(STRCMP(d1.PDB_ID,d2.PDB_ID)=1,d2.PDB_ID,d1.PDB_ID) as PDB_ID1,
                    IF(STRCMP(d1.PDB_ID,d2.PDB_ID)=1,d2.PDB_CHAIN,d1.PDB_CHAIN) as PDB_CHAIN1,
                    IF(STRCMP(d1.PDB_ID,d2.PDB_ID)=1,s.PDB_HET_ID2,s.PDB_HET_ID1) as PDB_HET_ID1,
                    IF(STRCMP(d1.PDB_ID,d2.PDB_ID)=1,d1.PDB_ID,d2.PDB_ID) as PDB_ID2,
                    IF(STRCMP(d1.PDB_ID,d2.PDB_ID)=1,d1.PDB_CHAIN,d2.PDB_CHAIN) as PDB_CHAIN2,
                    IF(STRCMP(d1.PDB_ID,d2.PDB_ID)=1,s.PDB_HET_ID1,s.PDB_HET_ID2) as PDB_HET_ID2,
                    s.P_VALUE,
                    s.LIG_RMSD,
                    s.LIG_RMSD_DIFF,
                    IF(STRCMP(d1.PDB_ID,d2.PDB_ID)=1,s.PDB_ID2,s.PDB_ID1) as PDB_ORIG_ID1,
                    IF(STRCMP(d1.PDB_ID,d2.PDB_ID)=1,s.PDB_CHAIN2,s.PDB_CHAIN1) as PDB_ORIG_CHAIN1,
                    IF(STRCMP(d1.PDB_ID,d2.PDB_ID)=1,s.PDB_ID1,s.PDB_ID2) as PDB_ORIG_ID2,
                    IF(STRCMP(d1.PDB_ID,d2.PDB_ID)=1,s.PDB_CHAIN1,s.PDB_CHAIN2) as PDB_ORIG_CHAIN2
                  from
                    SIM_PDB_BINDING_SITE_SMAP s,
                    PDB_CLUSTER95 c1,
                    VIEW_PDB_REPRESENTATIVE_CLUST d1,
                    PDB_CLUSTER95 c2,
                    VIEW_PDB_REPRESENTATIVE_CLUST d2
                  where
                    s.PDB_ID1 = c1.PDB_ID and
                    s.PDB_CHAIN1 = c1.PDB_CHAIN and
                    c1.CLUSTER_ID = d1.CLUSTER_ID and
                    s.PDB_ID2 = c2.PDB_ID and
                    s.PDB_CHAIN2 = c2.PDB_CHAIN and
                    c2.CLUSTER_ID = d2.CLUSTER_ID
                  ) b
                using
                  (
                  PDB_ID1,
                  PDB_CHAIN1,
                  PDB_HET_ID1,
                  PDB_ID2,
                  PDB_CHAIN2,
                  PDB_HET_ID2,
                  LIG_RMSD_DIFF
                  )
              """)
            return self.cursor.fetchall()
        else: #DB connection is not established
            return None

    def PDBclust95getRepresentative(self, pdbid, chain):

        self.cursor.execute("select CLUSTER_ID from PDB_CLUSTER95 where PDB_ID='%s' and PDB_CHAIN='%s'" % (pdbid, chain))
        clusterid = self.cursor.fetchall()[0][0]

        self.cursor.execute("select PDB_ID, PDB_CHAIN from PDB_CLUSTER95 where CLUSTER_ID=%d and MEMBER_ID=1" % (clusterid))

        return self.cursor.fetchall()[0]

    def PDBchainName(self, pdbid, chain):
        self.cursor.execute("select PDB_CHAIN_NAME from PDB_STRUCTURE_TYPE where PDB_PDB_ID='%s' and PDB_CHAIN='%s';" % (pdbid, chain))
        return self.cursor.fetchall()[0][0]

    def HETtoCID(self, hetid):
        """Returns a tuple of CID and Name."""
        self.cursor.execute(\
        """
            select
              p.CID,
              p.NAME
            from
              MAP_PDB_COMPOUNDS_PUBCHEM m,
              PUBCHEM_COMPOUND p
            where
              m.PDB_HET_ID = '%s' and
              m.PUBCHEM_CID = p.CID
        """  % (hetid))
        return self.cursor.fetchall()[0]

    def HETname(self, hetid):
        return self.HETtoCID(hetid)[1]



    def PDBclust95mapping(self):
        """Return a mapping cluster Rep. -- Cluster Member.
            ReprPDB_ID | ReprChain | PDB_ID | PDB_CHAIN"""
        if self.db:
            self.cursor.execute("""
            select
               c.PDB_ID as ReprPDB_ID,      -- PDB ID representative
               c.PDB_CHAIN as ReprChain,    -- PDB representative CHAIN
               cc.PDB_ID,
               cc.PDB_CHAIN
            from
              PDB_CLUSTER95 c,
              PDB_CLUSTER95 cc

            where
              c.MEMBER_ID=1 and
              c.CLUSTER_ID = cc.CLUSTER_ID

            ;
            """)
            return self.cursor.fetchall()
        else: #DB connection is not established
            f = read(os.path.expanduser('~/lib/pdbsuppl/PDBclust95mapping_comprot.csv'))
            pdbclust = []
            if not f.next().startswith('ReprPDB_ID'):
                f.seek(0)
            for line in f:
                pdbclust.append(line.strip().split('\t'))
            return pdbclust

    def PDBclust95(self):
        """Return the PDB_CLUSTER95 table."""
        if self.db:
            self.cursor.execute("select * from PDB_CLUSTER95")
            return self.cursor.fetchall()
        else: #DB connection is not established
            clustfile = os.path.join(self.path,'PDBclust95comprot.csv')
            if self.verbose:
                print "Checking for PDB cluster file in %s..." % clustfile
            pdbclust = []
            f = open(clustfile,'r')
            #skip Heading if present in the file!
            if not f.next().startswith('CLUSTER_ID'):
                f.seek(0)
            for line in f:
                pdbclust.append(line.strip().split('\t'))
            return pdbclust

    def CIDbindsPDB(self):
        """Returns the table of PDB Chains mapped to CIDs."""
        if self.db:
            self.cursor.execute("select distinct c.PUBCHEM_CID, p.PDB_ID, p.PDB_CHAIN from BINDS_PDB_COMPOUND p, MAP_PDB_COMPOUNDS_PUBCHEM c where p.PDB_HET_ID=c.PDB_HET_ID")
            tmp = self.cursor.fetchall()
        else:
            clustfile = os.path.join(self.path,'pdbchainscid.csv')
            if self.verbose:
                print "Checking for PDB-CID mapping in %s..." % clustfile
            tmp = []
            f = open(clustfile,'r')
            #skip Heading if present in the file!
            if not f.next().startswith('PUBCHEM_CID'):
                f.seek(0)
            for line in f:
                tmp.append(line.strip().split('\t'))
        return tmp

    def PDBstructureTypes(self):
        """Returns the RT_PDB_STRUCTURE_TYPE table as a dictionary mapping a 'structure type' to its id."""
        if self.db:
            self.cursor.execute("select * from RT_PDB_STRUCTURE_TYPE")
            tmp = self.cursor.fetchall()
        else:
            clustfile = os.path.join(self.path,'RTPDBstrucutretypeComprot.csv')
            if self.verbose:
                print "Checking for PDB structure type references file in %s..." % clustfile
            tmp = []
            f = open(clustfile,'r')
            #skip Heading if present in the file!
            if not f.next().startswith('ID'):
                f.seek(0)
            for line in f:
                tmp.append(line.strip().split('\t'))
        d = dict()
        for line in tmp:
            #e.g. d['prot'] = 3
            d[line[1].strip()] = int(line[0])
        return d

    def PDBnonProteins(self):
        """Returns a set of all NON-PROTEIN tuples of (pdb id, chain id). This function gets its data from the table PDB_STRUCTURE_TYPE. E.g.:
        +------------+-----------+-----------------------+
        | PDB_PDB_ID | PDB_CHAIN | PDB_STRUCTURE_TYPE_ID |
        +------------+-----------+-----------------------+
        | 100D       | A         |                     2 |
        +------------+-----------+-----------------------+
        """
        if self.db:
            self.cursor.execute("select * from PDB_STRUCTURE_TYPE")
            tmp = self.cursor.fetchall()
        else:
            clustfile = os.path.join(self.path,'PDBstrucutretypeComprot.csv')
            if self.verbose:
                print "Checking for PDB structure type file in %s..." % clustfile
            tmp = []
            f = open(clustfile,'r')
            #skip Heading if present in the file!
            if not f.next().startswith('PDB_PDB_ID'):
                f.seek(0)
            for line in f:
                tmp.append(line.strip().split('\t'))
        s = set()
        typesd = self.PDBstructureTypes()
        allowedids = [int(typesd['prot']),int(typesd['prot-nuc'])]
        for line in tmp:
            if int(line[2]) not in allowedids:
                s.add( (str(line[0]).strip(),str(line[1]).strip()) )
        return s

    def CIDtoHET(self):
        """Returns a dict of all PDB HET IDs, wich are represented by a CID.
        """
        if self.db:
            self.cursor.execute("""
                select distinct PUBCHEM_CID as CID, PDB_HET_ID as HETID from VIEW_BINDS_PDB_COMPOUNDS
                    UNION
                select distinct PUBCHEM_CID, PDB_HET_ID from MAP_PDB_COMPOUNDS_PUBCHEM
            """)
            tmp = self.cursor.fetchall()
        else:
            return False
        d = dict()

        for line in tmp:
            cid, het = line
            if cid not in d:
                d[cid] = [het]
            else:
                d[cid].append(het)
        return d
