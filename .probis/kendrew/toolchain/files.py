# The MIT License (MIT)
#
# Copyright (c) 2016 Joachim Haupt (TU Dresden / Germany)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import gzip
import zipfile
import os
import tempfile
import codecs
import socket
import csv

from kendrew.toolchain.errors import SubProcessError
from subprocess import Popen, PIPE, STDOUT


def read(fil):
    """Returns a file handler and detects gzipped files."""
    if fil is None:
        raise IOError("Provided 'None' as file name!")
    if os.path.splitext(fil)[-1] == '.gz':
        return gzip.open(fil, 'rb')
    elif os.path.splitext(fil)[-1] == '.zip':
        zf = zipfile.ZipFile(fil,'r')
        return zf.open(zf.infolist()[0].filename)
    else:
        try:
            codecs.open(fil, 'r','utf-8').read()
            return codecs.open(fil, 'r','utf-8')
        except UnicodeDecodeError:
            return open(fil, 'r')

def osrun(cmd,maxmem=None):
    """Runs a shell command returning all the output. The maximum memory in bytes
    is denoted by maxmem. E.g., 8589934592 B is 8 GB."""

    if maxmem is not None:
        cmd = "ulimit -v %d; %s" % (maxmem,cmd)
    p = Popen(cmd, shell=True, stdin=None, stdout=PIPE, stderr=STDOUT, close_fds=True)
    output = p.stdout.read()
    returncode = p.wait()
    if returncode > 0:
        error = "Subprocess exited with exit code %d (output follows).\n%s\n(end of output)" % (returncode,output.strip())
        raise SubProcessError(error,exitcode=returncode)

    return output

def tmpdir(dir='/dev/shm'):
    if socket.gethostname() == 'bioinfws1':
        dir='/ssd/joachimh/tmp'
    """Returns the path to a newly created temporary directory."""
    return tempfile.mkdtemp(dir=dir)

def tmpfile(dir='/tmp'):
    if socket.gethostname() == 'bioinfws1':
        dir='/ssd/joachimh/tmp'
    """Returns the path to a newly created temporary file."""
    return tempfile.mktemp(dir=dir)

def dictread(fname, delimiter='\t'):

    f = read(fname)
    csv.register_dialect('mysql', delimiter=delimiter, quoting=csv.QUOTE_NONE)
    return f, csv.DictReader(f, dialect='mysql')

def fileprefix(fname):
    return os.path.splitext(os.path.basename(fname))[0]
