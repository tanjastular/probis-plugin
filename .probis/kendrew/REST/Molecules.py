# The MIT License (MIT)
#
# Copyright (c) 2016 Joachim Haupt (TU Dresden / Germany)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

"""Implements functionality from kendrew/Molecules over a JSON REST interface without any fancy import."""

from kendrew.toolchain.webservice import query as wsquery
from kendrew.toolchain.errors import LigandRMSDError
import json
import getopt
import sys
import os
import uuid

class LigandRMSDjson(object):

    def __init__(self, defaultmolformat='mol2', runParallel=True, threads=None, verbose=False ):
        self.qmols = [None]
        self.qmolformat = None
        self.tmol = None
        self.tmolformat = None
        self.defaultmolformat = defaultmolformat
        self.runParallel = runParallel
        self.threads = threads
        self.verbose = verbose
        self._computeSessionID()

    def _computeSessionID(self):
        self.sessionID = str(uuid.uuid1(clock_seq=hash((tuple(self.qmols),self.tmol))))

    def setQueryMol(self, molstring, molformat='mol'):
        self.setQueryMols([molstring], molformat)

    def setQueryMols(self, molstrings, molformat='mol'):
        self.qmols = molstrings
        self.qmolformat = molformat
        self._computeSessionID()

    def setTargetMol(self, molstring, molformat='mol'):
        self.tmol = molstring
        self.tmolformat = molformat
        self._computeSessionID()

    def loads(self,jsonstring):
        d = json.loads(jsonstring)
        self.tmol = d['tmol']
        self.tmolformat = d['tmolformat']
        self.qmols = d['qmol']
        if type(self.qmols) == str:
            self.qmols = [self.qmols]
        self.qmolformat = d['qmolformat']
        if 'sessionID' in d:
            self.sessionID = d['sessionID']
        else:
            self._computeSessionID()
        if 'defaultmolformat' in d:
            self.defaultmolformat = d['defaultmolformat']
        if 'runParallel' in d:
            self.runParallel &= d['runParallel']
        if 'threads' in d:
            self.threads = d['threads']

    def dumps(self):
        return json.dumps(self.get_dict())

    def get_dict(self):
        d = dict()
        d['tmol'] = self.tmol
        d['tmolformat'] = self.tmolformat
        d['qmol'] = self.qmols
        d['qmolformat'] = self.qmolformat
        d['defaultmolformat'] = self.defaultmolformat
        d['runParallel'] = self.runParallel
        d['sessionID'] = self.sessionID
        if self.threads is not None:
            d['threads'] = self.threads
        for key in d:
            if d[key] is None:
                raise LigandRMSDError("NoneType is not allowed as a value for ''%s'." % key)
        return d

    def compute(self, alwaysReturnAlist=False):
        if self.qmols is None or self.tmol is None:
            raise LigandRMSDError('You need to provide two molecules!')
        self.results = wsquery(self.url, data=self.dumps())
        # f = open('/tmp/err.json','w')
        # f.write(self.results)
        # f.close()
        self.results = json.loads(self.results)
        if type(self.results) == dict:
            result = self.results['LigandRMSD']
            if alwaysReturnAlist:
                result = [result]
            return result
        else:
            return [result['LigandRMSD'] for result in self.results]

    def getSessionID(self):
        return self.sessionID

    def killjob(self):
        self.results = wsquery(self.url, data=json.dumps({'kill':self.sessionID}))


def main(verbose=False, query=None, target=None):

    j = LigandRMSDjson()
    j.setQueryMol(open(query).read(), os.path.splitext(query)[1][1:])
    j.setTargetMol(open(target).read(), os.path.splitext(target)[1][1:])
    #j.setQueryMol("CCC", 'smi')
    #j.setTargetMol("c1CCC=OCc1", 'smi')

    #generating the query`
    jstring = j.dumps()



    #this should run on the server
    from kendrew.Molecules import LigandRMSDjson
    ligrmsd = LigandRMSDjson()
    ligrmsd.loads(jstring)
    ligrmsd = ligrmsd.build()

    print ligrmsd.compute()






def usage():
    """Prints the help message for the script."""
    print """USAGE:

    NAME
       Molecules.py - kendrew molecule tools over REST.

    SYNOPSIS
        Molecules.py -q somemol.ext -t anothermol.fxt

    OPTIONS

    PARAMETERS
       -q   query molecule, ext is used to guess the format
       -t   target
    """

# Parse command line options
if __name__ == '__main__':
    try:
        opts, args = getopt.getopt(sys.argv[1:], "h?q:t:")
    except getopt.GetoptError:
        usage()
        sys.exit(2)
    verbose = False
    q = None
    t = None

    for opt, arg in opts:
        if opt == "-h" or opt == "-?":
            usage()
            sys.exit(1)
        elif opt == "-v":
            verbose = True
        elif opt == "-q":
            q = arg
        elif opt == "-t":
            t = arg

    main(verbose=verbose, query=q, target=t)
#end
