# The MIT License (MIT)
#
# Copyright (c) 2016 Joachim Haupt (TU Dresden / Germany)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

"""Computes the LigandRMSD from a given JSON file"""

import sys
import os
import json
from kendrew.toolchain.Types import RedisStore

from kendrew.Molecules import LigandRMSDjson

ligrmsd = LigandRMSDjson(runParallel=True, threads=6)
f = open(sys.argv[1])
ligrmsd.loads(f.read())
f.close()

#save the curren PID as a vaule of sessionID
sessionID = ligrmsd.getSessionID()
r = RedisStore(os.path.expanduser('~/.kendrew.conf'), db=1).get()
r.set(sessionID, os.getpid())

ligrmsd = ligrmsd.build()
results = ligrmsd.compute(LigandRMSDonly=False)

r.delete(sessionID)

print json.dumps(results)
