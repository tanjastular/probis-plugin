# The MIT License (MIT)
#
# Copyright (c) 2016 Joachim Haupt (TU Dresden / Germany)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

"""Generates sample input for the JSON REST interface."""

import json
import sys
import os
from kendrew.REST.Molecules import LigandRMSDjson
import random


path = sys.argv[1]
molfiles = os.listdir(path)

target = molfiles[-1]
for i, query in enumerate(molfiles[:-1]):
    j = LigandRMSDjson()
    if bool(random.getrandbits(1)):
        j.setQueryMol(open(os.path.join(path,query)).read(), os.path.splitext(query)[1][1:])
    else:
        j.setQueryMols([open(os.path.join(path,query)).read(), open(os.path.join(path,target)).read()], os.path.splitext(query)[1][1:])
    j.setTargetMol(open(os.path.join(path,target)).read(), os.path.splitext(target)[1][1:])
    f = open('test%d.json' % i, 'w')
    json.dump(j.get_dict(), f)
    f.close()
