import chimera.extension

class FindHBondEMO(chimera.extension.EMO):
	def name(self):
		return 'ProBiS'
	def description(self):
		return 'protein binding site detection'
	def categories(self):
		return ['Surface/Binding Analysis']
	def icon(self):
		return self.path('klika.ico')
	def activate(self):
		self.module('probis').main()
		return None

chimera.extension.manager.registerExtension(FindHBondEMO(__file__))
